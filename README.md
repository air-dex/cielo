Cielo

Datas en provenance de https://datahub.io/collections/football

Dépendances :
**chrono** pour la gestion des dates
**serde** pour la serialisation/déserialisation (JSON, YAML)
**getopts** pour parser les arguments dans le main

Passer les tests : cargo test --lib -- --nocapture --test-threads=1 --color=always
