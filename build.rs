use std::env;
use std::path::PathBuf;
use fs_extra::dir;

fn main() {
    // Copying data files near the executable.

    let mut root_data = PathBuf::from(match env::var("CARGO_MANIFEST_DIR") {
        Ok(cwd) => cwd,
        Err(e) => panic!("Error while fetching current dir: {}", e.to_string())
    });
    root_data.push("data");

    println!("cargo:rerun-if-changed={}", root_data.to_str().expect("There should be a root data path."));

    // The build exe is at <execdir>/build/cielo-*/*.exe
    // Also, $env:OUT_DIR is <execdir>/build/cielo-*/out
    // Also, <execdir> == $env:CARGO_TARGET_DIR/$env:PROFILE
    // NOTE: change it when there will be a proper OUT_DIR. (or if it is possible to retrieve the --out-dir arg of cargo build

    let mut execdir_data = PathBuf::from(env::var("OUT_DIR")
        .expect("Cannot access OUT_DIR."))
        .parent().expect("OUT_DIR should have a parent")
        .parent().expect("OUT_DIR should have a grand-parent")
        .parent().expect("OUT_DIR should have a great-grandparent")
        .to_path_buf();

    execdir_data.push("data");

    if let Err(e) = dir::remove(&execdir_data) {
        eprintln!("Error while deleting data files in target: {}", e.to_string());
    }

    let c_opt = dir::CopyOptions {
        overwrite: true,
        copy_inside: true,
        skip_exist: false,
        depth: 3,
        buffer_size: 1_000_000
    };

    match dir::copy(&root_data, &execdir_data, &c_opt) {
        Ok(_u) => println!(
            "{} copied at {}",
            root_data.to_str().expect("Cannot display root data path."),
            execdir_data.to_str().expect("Cannot display execdir data path.")
        ),
        Err(e) =>  {
            eprintln!("Error while copying data files in target: {}", e.to_string());
            eprintln!("Deleting copied files");

            match dir::remove(&execdir_data) {
                Ok(_rr) => eprintln!("Rollback completed!"),
                Err(er) => eprintln!("Error during rollback: {}", er.to_string())
            }
        }
    }
}
