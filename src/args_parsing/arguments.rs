use crate::common::{Country, Season, CieloResult};
use crate::data::file_formats::FileFormat;
use crate::data::file_formats::json::JsonParser;
use path_absolutize::Absolutize;
use std::convert::TryFrom;
use std::ops::Deref;
use std::path::PathBuf;

pub struct ArgInfo<'a> {
    short_name: &'a str,
    long_name: &'a str,
    description: &'a str,
    hint: &'a str,
    usage: &'a str,
}

impl<'a> ArgInfo<'a> {
    pub fn short_name(&self) -> &'a str {
        &self.short_name
    }

    pub fn long_name(&self) -> &'a str {
        &self.long_name
    }

    pub fn description(&self) -> &'a str {
        &self.description
    }

    pub fn hint(&self) -> &'a str {
        &self.hint
    }

    pub fn usage(&self) -> &'a str {
        &self.usage
    }
}

pub struct ArgHandler<'a, T: Clone> {
    arg_info: ArgInfo<'a>,
    parse_fn: fn (&String) -> CieloResult<T>,
    default_arg: T,
}

impl<'a, T: Clone> ArgHandler<'a, T> {
    pub fn parse_arg(&self, arg: &String) -> CieloResult<T> {
        match (self.parse_fn)(arg) {
            Ok(res) => Ok(res),
            Err(e) => Err(format!("Wrong {} argument: {}. Usage: {}", self.long_name(), e, self.usage()))
        }
    }

    pub fn default(&self) -> T {
        self.default_arg.clone()
    }
}

// Dereferencing on ArgInfo.
impl<'a, T: Clone> Deref for ArgHandler<'a, T> {
    type Target = ArgInfo<'a>;

    fn deref(&self) -> &Self::Target {
        &self.arg_info
    }
}

pub static COUNTRY_ARG: ArgHandler<Country> = ArgHandler {
    arg_info: ArgInfo {
        short_name: "c",
        long_name: "country",
        description: "Championship country. Values : en|es|de|it|fr. Default is fr (Ligue 1)",
        hint: "-c fr",
        usage: "-c en|es|de|it|fr or --country en|es|de|it|fr",
    },
    parse_fn: |arg| {
        Country::try_from(arg)
    },
    default_arg: Country::France
};

pub static SEASON_ARG: ArgHandler<Season> = ArgHandler {
    arg_info: ArgInfo {
        short_name: "s",
        long_name: "season",
        description: "Championship season. Default is lastest season (18-19)",
        hint: "-s 09-10",
        usage: "-s <year on 2 digits>-<next year on 2 digits> or --season <year on 2 digits>-<next year on 2 digits>",
    },
    parse_fn: |arg| {
        Season::try_from(arg)
    },
    default_arg: Season::const_new(18, 19),
};

pub static INPUT_FORMAT_ARG: ArgHandler<FileFormat> = ArgHandler {
    arg_info: ArgInfo {
        short_name: "",
        long_name: "input-format",
        description: "Input format. Values : csv|json. Default is JSON",
        hint: "--input-format json",
        usage: "--input-format csv|json",
    },
    parse_fn: |arg| {
        let i_fmt = FileFormat::try_from(arg)?;

        match i_fmt {
            FileFormat::Csv(_) | FileFormat::Json(_) => Ok(i_fmt),
            _ => Err(format!("{} is not accepted as an input format, unlike CSV and JSON.", arg))
        }
    },
    default_arg: FileFormat::Json(JsonParser {}),
};

pub static OUTPUT_FORMAT_ARG: ArgHandler<FileFormat> = ArgHandler {
    arg_info: ArgInfo {
        short_name: "",
        long_name: "output-format",
        description: "Output format. Values : human|xml|json|yaml|sqlite|log. Default is human",
        hint: "--output-format json",
        usage: "--output-format human|xml|json|yaml|sqlite|log",
    },
    parse_fn: |arg| {
        let o_fmt = FileFormat::try_from(arg)?;

        match o_fmt {
            FileFormat::Csv (_)=> Err(format!(
                "{} is not accepted as an output format, unlike XML, YAML, JSON, SQLite and (humanly readable) logs.", arg
            )),
            _ => Ok(o_fmt)
        }
    },
    default_arg: FileFormat::Human,
};

pub static STDOUT_ARG: ArgInfo = ArgInfo {
    short_name: "",
    long_name: "stdout",
    description: "Display through standard output?",
    hint: "",
    usage: "--stdout",
};

// Have to split the ArgHandler because of "const fn" fucking issues.
pub static OUTPUT_FILE_ARG: ArgInfo = ArgInfo {
    short_name: "o",
    long_name: "output-file",
    description: "Output file path. Default is \"data\". Ouput file format will be added automatically later.",
    hint: "",
    usage: "-o <path> or --output-file <path>",
};

pub fn string_to_pathbuf(arg: &String) -> CieloResult<PathBuf> {
    let pathbuf: PathBuf = PathBuf::from(std::ffi::OsString::from(arg));

    match pathbuf.absolutize() {
        Ok(p) => Ok(p),
        Err(e) => Err(format!("Error while parsing output file path: {}", e.to_string()))
    }
}

pub static DEFAULT_OUTPUT_NAME: &'static str = "data";

#[cfg(test)]
mod tests {
    use super::*;
    use std::fmt::Debug;
    use std::str::FromStr;
    use path_absolutize::CWD;
    use crate::data::file_formats::csv::CsvParser;

    fn test_a_handler<T: Clone + PartialEq + Eq + Debug>(arg_handler: &ArgHandler<T>, s: &str, expected: T) {
        let s = s.to_string();
        let s_res = arg_handler.parse_arg(&s);
        assert!(s_res.is_ok(), format!("{} should be a valid input for {}", s, arg_handler.long_name()));
        assert_eq!(s_res.unwrap(), expected);
    }

    fn fail_a_handler<T: Clone + PartialEq + Eq + Debug>(arg_handler: &ArgHandler<T>, s: &str, reason: &str) {
        let s = s.to_string();
        let s_res = arg_handler.parse_arg(&s);
        assert!(s_res.is_err());
        assert_eq!(s_res.unwrap_err(), format!("Wrong {} argument: {}. Usage: {}", arg_handler.long_name(), reason, arg_handler.usage()));
    }

    fn test_a_season_parse(season: &str, expected_begin_year: u8, expected_end_year: u8) {
        test_a_handler(&SEASON_ARG, season, Season::const_new(expected_begin_year, expected_end_year));
    }

    fn test_a_pathbuf(path: &str, expected_pathbuf: &PathBuf) {
        let s = path.to_string();
        let s_res = string_to_pathbuf(&s);
        assert!(s_res.is_ok());
        assert_eq!(s_res.unwrap(), *expected_pathbuf);
    }

    #[test]
    fn country_arg() {
        // Valid countries
        println!("\n\tValid countries");

        test_a_handler(&COUNTRY_ARG, "en", Country::England);
        test_a_handler(&COUNTRY_ARG, "es", Country::Spain);
        test_a_handler(&COUNTRY_ARG, "de", Country::Germany);
        test_a_handler(&COUNTRY_ARG, "it", Country::Italy);
        test_a_handler(&COUNTRY_ARG, "fr", Country::France);

        // Invalid country
        println!("\tInvalid country");

        fail_a_handler(&COUNTRY_ARG, "foobar", "foobar is not a supported country");
    }

    #[test]
    fn season_arg() {
        // Valid season
        println!("\n\tValid season");

        println!("\t\tNominal case");
        test_a_season_parse("18-19", 18, 19);

        println!("\t\tYears beginning with a zero");
        test_a_season_parse("09-10", 9, 10);
        test_a_season_parse("04-05", 4, 5);
        test_a_season_parse("00-01", 0, 1);

        // Invalid seasons
        println!("\tInvalid seasons");

        // Years beginning with a zero
        println!("\t\tYears beginning with a zero");

        fail_a_handler(
            &SEASON_ARG,
            "9-10",
            "The season has to be written with the first year on two digits, a caret, then the following year on 2 digits."
        );
        fail_a_handler(
            &SEASON_ARG,
            "08-9",
            "The season has to be written with the first year on two digits, a caret, then the following year on 2 digits."
        );
        fail_a_handler(
            &SEASON_ARG,
            "8-9",
            "The season has to be written with the first year on two digits, a caret, then the following year on 2 digits."
        );

        // Years with 4 digits
        println!("\t\tYears with 4 digits");

        fail_a_handler(
            &SEASON_ARG,
            "2018-2019",
            "The season has to be written with the first year on two digits, a caret, then the following year on 2 digits."
        );
        fail_a_handler(
            &SEASON_ARG,
            "18-2019",
            "The season has to be written with the first year on two digits, a caret, then the following year on 2 digits."
        );
        fail_a_handler(
            &SEASON_ARG,
            "2018-19",
            "The season has to be written with the first year on two digits, a caret, then the following year on 2 digits."
        );

        // Not following year
        println!("\t\tNot following year");

        fail_a_handler(
            &SEASON_ARG,
            "10-12",
            "The ending year should follow the beginning year."
        );
        fail_a_handler(
            &SEASON_ARG,
            "13-12",
            "The ending year should follow the beginning year."
        );
        fail_a_handler(
            &SEASON_ARG,
            "14-12",
            "The ending year should follow the beginning year."
        );

        // Missing years
        println!("\t\tMissing years");

        fail_a_handler(
            &SEASON_ARG,
            "-",
            "The season has to be written with the first year on two digits, a caret, then the following year on 2 digits."
        );
        fail_a_handler(
            &SEASON_ARG,
            "18-",
            "The season has to be written with the first year on two digits, a caret, then the following year on 2 digits."
        );
        fail_a_handler(
            &SEASON_ARG,
            "-19",
            "The season has to be written with the first year on two digits, a caret, then the following year on 2 digits."
        );

        // Negative years
        println!("\t\tNegative years");

        fail_a_handler(
            &SEASON_ARG,
            "-18-19",
            "The season has to be written with the first year on two digits, a caret, then the following year on 2 digits."
        );
        fail_a_handler(
            &SEASON_ARG,
            "-18--19",
            "The season has to be written with the first year on two digits, a caret, then the following year on 2 digits."
        );
        fail_a_handler(
            &SEASON_ARG,
            "18--19",
            "The season has to be written with the first year on two digits, a caret, then the following year on 2 digits."
        );

        // Other values
        println!("\t\tOther values");

        fail_a_handler(
            &SEASON_ARG,
            "hotot",
            "The season has to be written with the first year on two digits, a caret, then the following year on 2 digits."
        );
    }

    #[test]
    fn input_format_arg() {
        // Valid input format
        println!("\n\tValid input format");
        test_a_handler(&INPUT_FORMAT_ARG, "csv", FileFormat::Csv(CsvParser {}));
        test_a_handler(&INPUT_FORMAT_ARG, "json", FileFormat::Json(JsonParser {}));

        // Invalid input format
        println!("\tInvalid input format");
        ["xml", "yaml", "sqlite", "human", "log"].into_iter().for_each(|ff| {
            fail_a_handler(
                &INPUT_FORMAT_ARG,
                ff,
                format!("{} is not accepted as an input format, unlike CSV and JSON.", ff).as_str()
            );
        });

        // Unknown input format
        println!("\tUnknown input format");
        fail_a_handler(&INPUT_FORMAT_ARG, "foobar", "Unknown foobar file format");
    }

    #[test]
    fn output_format_arg() {
        // Valid output formats
        println!("\n\tValid output formats");
        test_a_handler(&OUTPUT_FORMAT_ARG, "json", FileFormat::Json(JsonParser {}));
        test_a_handler(&OUTPUT_FORMAT_ARG, "xml", FileFormat::Xml);
        test_a_handler(&OUTPUT_FORMAT_ARG, "yaml", FileFormat::Yaml);
        test_a_handler(&OUTPUT_FORMAT_ARG, "human", FileFormat::Human);
        test_a_handler(&OUTPUT_FORMAT_ARG, "log", FileFormat::Human);
        test_a_handler(&OUTPUT_FORMAT_ARG, "sqlite", FileFormat::Sqlite);

        // Invalid output format
        println!("\tInvalid output format");
        fail_a_handler(
            &OUTPUT_FORMAT_ARG,
            "csv",
            "csv is not accepted as an output format, unlike XML, YAML, JSON, SQLite and (humanly readable) logs."
        );

        // Unknown output format
        println!("\tUnknown output format");
        fail_a_handler(&OUTPUT_FORMAT_ARG, "foobar", "Unknown foobar file format");
    }

    #[inline]
    fn string_to_pathbuf_test(root: &str, rel_path: &str, c_prefix: &str, _inv_char: &str) {
        // Absolute path
        println!("\n\tAbsolute path");
        let expected_pathbuf = PathBuf::from_str(root).unwrap();
        test_a_pathbuf(root, &expected_pathbuf);

        // Relative path
        println!("\tRelative path");

        println!("\t\t.");

        let mut expected_pathbuf: PathBuf = CWD.clone();
        expected_pathbuf.push(rel_path);
        test_a_pathbuf(rel_path, &expected_pathbuf);

        let mut expected_pathbuf: PathBuf = CWD.clone();
        expected_pathbuf.push(rel_path);
        let path = format!("{}{}", c_prefix, rel_path); // ./data
        test_a_pathbuf(path.as_str(), &expected_pathbuf);

        println!("\t\t..");
        if let Some(p) = CWD.clone().parent() {
            let mut expected_pathbuf: PathBuf = p.to_path_buf();
            expected_pathbuf.push("data");
            let path = format!(".{}{}", c_prefix, rel_path); // ../data
            test_a_pathbuf(path.as_str(), &expected_pathbuf);
        }
        else {
            println!("\t\t\tYou are likely testing at root. Ignore!")
        }

        // Path with invalid chars
        println!("\tPath with forbidden chars");
        println!("\t\tLooks like Rust do not give a f*ck to forbidden characters in paths. Ignore!");
        /*
        let path = inv_char.to_string();
        let stp_res = string_to_pathbuf(&path);
        assert!(
            stp_res.is_err(),
            format!("{} should not be valid since it contains a forbidden character.", stp_res.unwrap().to_str().unwrap())
        );
        assert!(stp_res.unwrap_err().starts_with("Error while parsing output file path: "));
        */
    }

    #[test]
    #[cfg(windows)]
    fn string_to_pathbuf_windows() {
        string_to_pathbuf_test(r"C:\toto", "data", r".\", r"C:\to<to");
    }

    #[test]
    #[cfg(not(windows))]
    fn string_to_pathbuf_unix() {
        string_to_pathbuf_test("/toto", "data", r"./", "/to<to");
    }
}
