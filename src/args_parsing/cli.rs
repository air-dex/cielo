use crate::args_parsing::arguments::*;
use crate::common::{Country, Season, CieloResult};
use crate::data::file_formats::FileFormat;
use getopts::{Options, Matches, ParsingStyle};
use std::convert::TryFrom;
use std::path::PathBuf;

// TODO: add a half-time argument (ELO at half time or 2nd half time

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct CieloArgs {
    country: Country,
    season: Season,
    input_format: FileFormat,
    output_format: FileFormat,
    output_file: PathBuf,
    stdout: bool
}

impl CieloArgs {
    pub fn from_cli(args: &Vec<String>) -> CieloResult<CieloArgs> {
        match parse_cli(args) {
            Ok(m) => parse_matches(m),
            Err(f) => Err(format!("Error while parsing args: {}", f.to_string()))
        }
    }

    // Getters

    pub fn country(&self) -> Country {
        self.country
    }

    pub fn season(&self) -> Season {
        self.season
    }

    pub fn input_format(&self) -> FileFormat {
        self.input_format
    }

    pub fn output_format(&self) -> FileFormat {
        self.output_format
    }

    pub fn output_file(&self) -> &PathBuf {
        &self.output_file
    }

    pub fn stdout(&self) -> bool {
        self.stdout
    }
}

impl TryFrom<&Vec<String>> for CieloArgs {
    type Error = String;

    fn try_from(args: &Vec<String>) -> Result<Self, Self::Error> {
        CieloArgs::from_cli(args)
    }
}

fn parse_cli(args: &Vec<String>) -> getopts::Result {
    let mut opts: Options = Options::new();
    opts.optopt(COUNTRY_ARG.short_name(), COUNTRY_ARG.long_name(), COUNTRY_ARG.description(), COUNTRY_ARG.hint());
    opts.optopt(SEASON_ARG.short_name(), SEASON_ARG.long_name(), SEASON_ARG.description(), SEASON_ARG.hint());
    opts.optopt(INPUT_FORMAT_ARG.short_name(), INPUT_FORMAT_ARG.long_name(), INPUT_FORMAT_ARG.description(), INPUT_FORMAT_ARG.hint());
    opts.optopt(OUTPUT_FORMAT_ARG.short_name(), OUTPUT_FORMAT_ARG.long_name(), OUTPUT_FORMAT_ARG.description(), OUTPUT_FORMAT_ARG.hint());
    opts.optopt(OUTPUT_FILE_ARG.short_name(), OUTPUT_FILE_ARG.long_name(), OUTPUT_FILE_ARG.description(), OUTPUT_FILE_ARG.hint());
    opts.optflagopt(STDOUT_ARG.short_name(), STDOUT_ARG.long_name(), STDOUT_ARG.description(), STDOUT_ARG.hint());

    opts.parsing_style(ParsingStyle::StopAtFirstFree);
    opts.parse(&args[1..])
}

fn parse_matches(matches: Matches) -> CieloResult<CieloArgs> {
    let mut err_msgs: Vec<String> = Vec::with_capacity(5);

    let country: Country = parse_a_match(&matches, &COUNTRY_ARG, &mut err_msgs);
    let season: Season = parse_a_match(&matches, &SEASON_ARG, &mut err_msgs);
    let input_format: FileFormat = parse_a_match(&matches, &INPUT_FORMAT_ARG, &mut err_msgs);
    let output_format: FileFormat = parse_a_match(&matches, &OUTPUT_FORMAT_ARG, &mut err_msgs);

    let mut output_file: PathBuf = PathBuf::new();

    if matches.opt_present(OUTPUT_FILE_ARG.long_name()) {
        match matches.opt_str(OUTPUT_FILE_ARG.long_name()) {
            Some(o) => match string_to_pathbuf(&o){
                Ok(o2) => output_file = o2,
                Err(e) => err_msgs.push(e)
            },
            None => err_msgs.push("The output-file argument should be a string.".to_string())
        }
    }
    else {
        let s = DEFAULT_OUTPUT_NAME.to_string();
        match string_to_pathbuf(&s) {
            Ok(p) => output_file = p,
            Err(e) => err_msgs.push(format!("Cannot parse default output path: {}", e.to_string()))
        }
    }

    output_file.set_extension(output_format.to_string());

    let stdout = matches.opt_present(STDOUT_ARG.long_name());

    if err_msgs.is_empty() {
        Ok(CieloArgs {country, season, input_format, output_format, output_file, stdout})
    }
    else {
        Err(err_msgs.join("\n"))
    }
}

#[inline]
fn parse_a_match<T: Clone>(matches: &Matches, a: &ArgHandler<T>, err_msgs: &mut Vec<String>) -> T {
    let mut res: T = a.default();

    if matches.opt_present(a.long_name()) {
        match matches.opt_str(a.long_name()) {
            Some(s) => match a.parse_arg(&s){
                Ok(t) => res = t,
                Err(e) => err_msgs.push(e)
            },
            None => err_msgs.push(format!("The {} argument should be a string.", a.long_name()))
        }
    }

    res
}

#[cfg(test)]
mod tests {
    use super::*;
    use path_absolutize::CWD;
    use std::str::FromStr;
    use crate::data::file_formats::csv::CsvParser;
    use crate::data::file_formats::json::JsonParser;

    #[inline]
    fn to_vecargs(args: Vec<&str>) -> Vec<String> {
        args.iter().map(|x| {x.to_string()}).collect()
    }

    #[inline]
    // e_* values are ignored if their corresponding has_* values are setting to false.
    fn test_a_parse_cli(args: Vec<&str>,
                        has_country: bool, e_country: &str,
                        has_season: bool,  e_season: &str,
                        has_ifmt: bool, e_ifmt: &str,
                        has_ofmt: bool, e_ofmt: &str,
                        has_stdout: bool,
                        has_ofile: bool, e_ofile: &str)
    {
        let args= to_vecargs(args);
        let c_res = parse_cli(&args);
        assert!(c_res.is_ok());

        let matches = c_res.unwrap();

        let expectations = [
            ("country", has_country, e_country),
            ("season", has_season,  e_season),
            ("input-format", has_ifmt, e_ifmt),
            ("output-format", has_ofmt, e_ofmt),
            ("output-file", has_ofile, e_ofile)
        ];

        expectations.iter().for_each(|&(arg_name, has_arg, exp_val)| {
            let arg_present = matches.opt_present(arg_name);
            if has_arg {
                assert!(arg_present, format!("A {} argument should be present.", arg_name));
                assert_eq!(matches.opt_count(arg_name), 1);
                match matches.opt_str(arg_name) {
                    Some(current) => assert_eq!(current, exp_val.to_string(), "The {} argument should be equal to '{}'.", arg_name, exp_val),
                    None => panic!("There should be a string value for the {} argument.", arg_name)
                }
            }
            else {
                assert!(!arg_present, format!("A {} argument should not be present.", arg_name))
            }
        });

        // Stdout argument is a bool so it is a little bit different for it.
        let arg_name = "stdout";
        let arg_present = matches.opt_present(arg_name);
        if has_stdout {
            assert!(arg_present, format!("A {} argument should be present.", arg_name));
        }
        else {
            assert!(!arg_present, format!("A {} argument should not be present.", arg_name))
        }
    }

    #[inline]
    fn fail_a_parse_cli(args: Vec<&str>) {
        let args= to_vecargs(args);
        let c_res = parse_cli(&args);
        assert!(c_res.is_err());
    }

    #[test]
    fn parse_cli_test() {
        // Nominal case
        println!("\n\tNominal case");

        // cielo.exe --stdout --input-format csv --season 17-18 --output-format sqlite --country de --output-file bundes
        test_a_parse_cli(
            vec![
                "cielo.exe",
                "--stdout",
                "--input-format", "csv",
                "--season", "17-18",
                "--output-format", "sqlite",
                "--country", "de",
                "--output-file", "bundes"
            ],
            true, "de",
            true, "17-18",
            true, "csv",
            true, "sqlite",
            true,
            true, "bundes"
        );

        // Cases with short name args
        println!("\tCases with short name args");

        // cielo.exe --stdout --input-format csv -s 17-18 --output-format sqlite -c de -o bundes
        test_a_parse_cli(
            vec![
                "cielo.exe",
                "--stdout",
                "--input-format", "csv",
                "-s", "17-18",
                "--output-format", "sqlite",
                "-c", "de",
                "-o", "bundes"
            ],
            true, "de",
            true, "17-18",
            true, "csv",
            true, "sqlite",
            true,
            true, "bundes"
        );

        // Cases with arguments missing
        println!("\tCases with arguments missing");

        // cielo.exe --input-format csv --season 17-18 --output-format sqlite --country de --output-file bundes
        test_a_parse_cli(
            vec![
                "cielo.exe",
                "--input-format", "csv",
                "--season", "17-18",
                "--output-format", "sqlite",
                "--country", "de",
                "--output-file", "bundes"
            ],
            true, "de",
            true, "17-18",
            true, "csv",
            true, "sqlite",
            false,
            true, "bundes"
        );

        // cielo.exe --stdout --season 17-18 --output-format sqlite --country de --output-file bundes
        test_a_parse_cli(
            vec![
                "cielo.exe",
                "--stdout",
                "--season", "17-18",
                "--output-format", "sqlite",
                "--country", "de",
                "--output-file", "bundes"
            ],
            true, "de",
            true, "17-18",
            false, "csv",
            true, "sqlite",
            true,
            true, "bundes"
        );

        // cielo.exe --stdout --input-format csv --output-format sqlite --country de --output-file bundes
        test_a_parse_cli(
            vec![
                "cielo.exe",
                "--stdout",
                "--input-format", "csv",
                "--output-format", "sqlite",
                "--country", "de",
                "--output-file", "bundes"
            ],
            true, "de",
            false, "17-18",
            true, "csv",
            true, "sqlite",
            true,
            true, "bundes"
        );

        // cielo.exe --stdout --input-format csv --season 17-18 --country de --output-file bundes
        test_a_parse_cli(
            vec![
                "cielo.exe",
                "--stdout",
                "--input-format", "csv",
                "--season", "17-18",
                "--country", "de",
                "--output-file", "bundes"
            ],
            true, "de",
            true, "17-18",
            true, "csv",
            false, "sqlite",
            true,
            true, "bundes"
        );

        // cielo.exe --stdout --input-format csv --season 17-18 --output-format sqlite --output-file bundes
        test_a_parse_cli(
            vec![
                "cielo.exe",
                "--stdout",
                "--input-format", "csv",
                "--season", "17-18",
                "--output-format", "sqlite",
                "--output-file", "bundes"
            ],
            false, "de",
            true, "17-18",
            true, "csv",
            true, "sqlite",
            true,
            true, "bundes"
        );

        // cielo.exe --stdout --input-format csv --season 17-18 --output-format sqlite --country de
        test_a_parse_cli(
            vec![
                "cielo.exe",
                "--stdout",
                "--input-format", "csv",
                "--season", "17-18",
                "--output-format", "sqlite",
                "--country", "de",
            ],
            true, "de",
            true, "17-18",
            true, "csv",
            true, "sqlite",
            true,
            false, "bundes"
        );

        // cielo.exe
        test_a_parse_cli(
            vec![ "cielo.exe", ],
            false, "de",
            false, "17-18",
            false, "csv",
            false, "sqlite",
            false,
            false, "bundes"
        );

        // Case with a typo in an arg name.
        println!("\tCase with a typo in an arg name");

        // cielo.exe --stdout --inut-format csv --season 17-18 --output-format sqlite --country de --output-file bundes
        fail_a_parse_cli(
            vec![
                "--stdout",
                "--inut-format", "csv",
                "--season", "17-18",
                "--output-format", "sqlite",
                "--country", "de",
                "--output-file", "bundes"
            ]
        );

        // With an additional arg
        println!("\tWith an additional arg");

        // cielo.exe --stdout --input-format csv --season 17-18 --output-format sqlite --country de  --output-file bundes --foo bar
        fail_a_parse_cli(
            vec![
                "cielo.exe",
                "--stdout",
                "--input-format", "csv",
                "--season", "17-18",
                "--output-format", "sqlite",
                "--country", "de",
                "--output-file", "bundes",
                "--foo", "bar"
            ]
        );

        // Multi arguments fail
        println!("\tMulti arguments fail");

        // cielo.exe --stdout --input-format csv --input-format csv
        fail_a_parse_cli(
            vec![
                "cielo.exe",
                "--stdout",
                "--input-format", "csv",
                "--input-format", "csv",
            ]
        );

        // cielo.exe --stdout --input-format csv --input-format json
        fail_a_parse_cli(
            vec![
                "cielo.exe",
                "--stdout",
                "--input-format", "csv",
                "--input-format", "json",
            ]
        );

        // Case with an argument without value
        println!("\tCase with an argument without value");

        // cielo.exe --stdout --input-format csv --season --output-format sqlite --country de  --output-file bundes
        // Parsing starts failing after incomplete --season
        test_a_parse_cli(
            vec![
                "cielo.exe",
                "--stdout",
                "--input-format", "csv",
                "--season",
                "--output-format", "sqlite",
                "--country", "de",
                "--output-file", "bundes"
            ],
            false, "de",
            true, "--output-format",    // It bugs there.
            true, "csv",
            false, "sqlite",
            true,
            false, "bundes"
        );
        // cielo.exe --stdout --input-format csv --season --output-format sqlite --country de  --output-file bundes should be OK.
        test_a_parse_cli(
            vec![
                "cielo.exe",
                "--stdout",
                "--input-format", "csv",
                "--season",
                "--output-format", "--output-format", "sqlite",
                "--country", "de",
                "--output-file", "bundes"
            ],
            true, "de",
            true, "--output-format",    // Because why not.
            true, "csv",
            true, "sqlite",
            true,
            true, "bundes"
        );

        // No cielo.exe
        println!("\tNo cielo.exe");

        // --stdout --input-format csv --season 17-18 --output-format sqlite --country de  --output-file bundes
        // --stdout is the executable name for CLI there.
        test_a_parse_cli(
            vec![
                "--stdout",
                "--input-format", "csv",
                "--season", "17-18",
                "--output-format", "sqlite",
                "--country", "de",
                "--output-file", "bundes"
            ],
            true, "de",
            true, "17-18",
            true, "csv",
            true, "sqlite",
            false,
            true, "bundes"
        );

        // --input-format csv --season 17-18 --output-format sqlite --country de  --output-file bundes
        test_a_parse_cli(
            vec![
                "--input-format", "csv",
                "--season", "17-18",
                "--output-format", "sqlite",
                "--country", "de",
                "--output-file", "bundes"
            ],
            false, "de",
            false, "17-18",
            false, "csv",
            false, "sqlite",
            false,
            false, "bundes"
        );

        // Special values
        println!("\tSpecial values");

        // cielo.exe --season 09-10
        test_a_parse_cli(
            vec![ "cielo.exe", "--season", "09-10", ],
            false, "de",
            true, "09-10",
            false, "csv",
            false, "sqlite",
            false,
            false, "bundes"
        );

        // cielo.exe --season 04-05
        test_a_parse_cli(
            vec![ "cielo.exe", "--season", "04-05", ],
            false, "de",
            true, "04-05",
            false, "csv",
            false, "sqlite",
            false,
            false, "bundes"
        );

        // cielo.exe --season 00-01
        test_a_parse_cli(
            vec![ "cielo.exe", "--season", "00-01", ],
            false, "de",
            true, "00-01",
            false, "csv",
            false, "sqlite",
            false,
            false, "bundes"
        );

        if cfg!(windows) {
            // cielo.exe -o C:\toto (Windows only)
            test_a_parse_cli(
                vec![ "cielo.exe", "-o", r"C:\toto" ],
                false, "us",
                false, "9-10",
                false, "csv",
                false, "sqlite",
                false,
                true, r"C:\toto"
            );
        }
        else if cfg!(not(windows)) {
            // cielo.exe -o /toto (Unix-style only)
            test_a_parse_cli(
                vec![ "cielo.exe", "-o", "/toto" ],
                false, "us",
                false, "9-10",
                false, "csv",
                false, "sqlite",
                false,
                true, "/toto"
            );
        }

        // Mismatched args
        println!("\tMismatched args");

        // cielo.exe --country us
        test_a_parse_cli(
            vec![ "cielo.exe", "--country", "us", ],
            true, "us",
            false, "9-10",
            false, "csv",
            false, "sqlite",
            false,
            false, "bundes"
        );

        // cielo.exe --season 9-10
        test_a_parse_cli(
            vec![ "cielo.exe", "--season", "9-10", ],
            false, "de",
            true, "9-10",
            false, "csv",
            false, "sqlite",
            false,
            false, "bundes"
        );

        // cielo.exe --season 08-9
        test_a_parse_cli(
            vec![ "cielo.exe", "--season", "08-9", ],
            false, "de",
            true, "08-9",
            false, "csv",
            false, "sqlite",
            false,
            false, "bundes"
        );

        // cielo.exe --season toto
        test_a_parse_cli(
            vec![ "cielo.exe", "--season", "toto", ],
            false, "de",
            true, "toto",
            false, "csv",
            false, "sqlite",
            false,
            false, "bundes"
        );

        // cielo.exe --season 8-9
        test_a_parse_cli(
            vec![ "cielo.exe", "--season", "8-9", ],
            false, "de",
            true, "8-9",
            false, "csv",
            false, "sqlite",
            false,
            false, "bundes"
        );

        // cielo.exe --season 2018-2019
        test_a_parse_cli(
            vec![ "cielo.exe", "--season", "2018-2019", ],
            false, "de",
            true, "2018-2019",
            false, "csv",
            false, "sqlite",
            false,
            false, "bundes"
        );

        // cielo.exe --season 18-2019
        test_a_parse_cli(
            vec![ "cielo.exe", "--season", "18-2019", ],
            false, "de",
            true, "18-2019",
            false, "csv",
            false, "sqlite",
            false,
            false, "bundes"
        );

        // cielo.exe --season 2018-19
        test_a_parse_cli(
            vec![ "cielo.exe", "--season", "2018-19", ],
            false, "de",
            true, "2018-19",
            false, "csv",
            false, "sqlite",
            false,
            false, "bundes"
        );

        // cielo.exe --season 10-12
        test_a_parse_cli(
            vec![ "cielo.exe", "--season", "10-12", ],
            false, "de",
            true, "10-12",
            false, "csv",
            false, "sqlite",
            false,
            false, "bundes"
        );

        // cielo.exe --season 13-12
        test_a_parse_cli(
            vec![ "cielo.exe", "--season", "13-12", ],
            false, "de",
            true, "13-12",
            false, "csv",
            false, "sqlite",
            false,
            false, "bundes"
        );

        // cielo.exe --season 14-12
        test_a_parse_cli(
            vec![ "cielo.exe", "--season", "14-12", ],
            false, "de",
            true, "14-12",
            false, "csv",
            false, "sqlite",
            false,
            false, "bundes"
        );

        // cielo.exe --season -
        test_a_parse_cli(
            vec![ "cielo.exe", "--season", "-", ],
            false, "de",
            true, "-",
            false, "csv",
            false, "sqlite",
            false,
            false, "bundes"
        );

        // cielo.exe --season 18-
        test_a_parse_cli(
            vec![ "cielo.exe", "--season", "18-", ],
            false, "de",
            true, "18-",
            false, "csv",
            false, "sqlite",
            false,
            false, "bundes"
        );

        // cielo.exe --season -19
        test_a_parse_cli(
            vec![ "cielo.exe", "--season", "-19", ],
            false, "de",
            true, "-19",
            false, "csv",
            false, "sqlite",
            false,
            false, "bundes"
        );

        // cielo.exe --season -18-19
        test_a_parse_cli(
            vec![ "cielo.exe", "--season", "-18-19", ],
            false, "de",
            true, "-18-19",
            false, "csv",
            false, "sqlite",
            false,
            false, "bundes"
        );

        // cielo.exe --season -18--19
        test_a_parse_cli(
            vec![ "cielo.exe", "--season", "-18--19", ],
            false, "de",
            true, "-18--19",
            false, "csv",
            false, "sqlite",
            false,
            false, "bundes"
        );

        // cielo.exe --season 18--19
        test_a_parse_cli(
            vec![ "cielo.exe", "--season", "18--19", ],
            false, "de",
            true, "18--19",
            false, "csv",
            false, "sqlite",
            false,
            false, "bundes"
        );

        // cielo.exe --input-format xml
        test_a_parse_cli(
            vec![ "cielo.exe", "--input-format", "xml", ],
            false, "de",
            false, "17-18",
            true, "xml",
            false, "sqlite",
            false,
            false, "bundes"
        );

        // cielo.exe --input-format yamelle
        test_a_parse_cli(
            vec![ "cielo.exe", "--input-format", "yamelle", ],
            false, "de",
            false, "17-18",
            true, "yamelle",
            false, "sqlite",
            false,
            false, "bundes"
        );

        // cielo.exe --output-format csv
        test_a_parse_cli(
            vec![ "cielo.exe", "--output-format", "csv", ],
            false, "de",
            false, "17-18",
            false, "cssv",
            true, "csv",
            false,
            false, "bundes"
        );

        // cielo.exe --output-format yamelle
        test_a_parse_cli(
            vec![ "cielo.exe", "--output-format", "yamelle", ],
            false, "de",
            false, "17-18",
            false, "csssev",
            true, "yamelle",
            false,
            false, "bundes"
        );
    }

    #[inline]
    fn test_a_from_cli(args: Vec<&str>,
                       e_country: Country,
                       e_byear: u8, e_eyear: u8,
                       e_ifmt: FileFormat,
                       e_ofmt: FileFormat,
                       e_stdout: bool,
                       e_ofile: PathBuf)
    {
        let args= to_vecargs(args);
        let c_res = CieloArgs::from_cli(&args);
        assert!(c_res.is_ok());
        assert_eq!(
            c_res.unwrap(),
            CieloArgs {
                country: e_country,
                season: Season::const_new(e_byear, e_eyear),
                input_format: e_ifmt,
                output_format: e_ofmt,
                stdout: e_stdout,
                output_file: e_ofile
            }
        );
    }

    #[inline]
    fn fail_a_from_cli(cli: Vec<&str>, e_err_msgs : Vec<&str>) {
        let cli_str = cli.join(" ");
        let args= to_vecargs(cli);
        let c_res = CieloArgs::from_cli(&args);
        assert!(c_res.is_err());
        let c_err = c_res.unwrap_err();
        e_err_msgs.iter().for_each(|e| {
            assert!(
                c_err.contains(e),
                format!("'{}' error message '{}' should contain '{}'", cli_str, c_err, e)
            );
        })
    }

    #[inline]
    fn format_a_parse_errmsg(argname :&str, errmsg: &str) -> String {
        format!("Wrong {} argument: {}. Usage: ", argname, errmsg)
    }

    #[test]
    fn try_from_cli() {
        // Nominal case
        println!("\n\tNominal case");

        // cielo.exe --stdout --input-format csv --season 17-18 --output-format sqlite --country de --output-file bundes
        test_a_from_cli(
            vec![
                "cielo.exe",
                "--stdout",
                "--input-format", "csv",
                "--season", "17-18",
                "--output-format", "sqlite",
                "--country", "de",
                "--output-file", "bundes"
            ],
            Country::Germany,
            17, 18,
            FileFormat::Csv(CsvParser {}),
            FileFormat::Sqlite,
            true,
            {
                let mut of: PathBuf = CWD.clone();
                of.push("bundes.sqlite");
                of
            }
        );

        // Cases with short name args
        println!("\tCases with short name args");

        // cielo.exe --stdout --input-format csv -s 17-18 --output-format sqlite -c de -o bundes
        test_a_from_cli(
            vec![
                "cielo.exe",
                "--stdout",
                "--input-format", "csv",
                "-s", "17-18",
                "--output-format", "sqlite",
                "-c", "de",
                "-o", "bundes"
            ],
            Country::Germany,
            17, 18,
            FileFormat::Csv(CsvParser {}),
            FileFormat::Sqlite,
            true,
            {
                let mut of: PathBuf = CWD.clone();
                of.push("bundes.sqlite");
                of
            }
        );

        // Cases with arguments missing
        println!("\tCases with arguments missing");

        // cielo.exe --input-format csv --season 17-18 --output-format sqlite --country de --output-file bundes
        test_a_from_cli(
            vec![
                "cielo.exe",
                "--input-format", "csv",
                "--season", "17-18",
                "--output-format", "sqlite",
                "--country", "de",
                "--output-file", "bundes"
            ],
            Country::Germany,
            17, 18,
            FileFormat::Csv(CsvParser {}),
            FileFormat::Sqlite,
            false,
            {
                let mut of: PathBuf = CWD.clone();
                of.push("bundes.sqlite");
                of
            }
        );

        // cielo.exe --stdout --season 17-18 --output-format sqlite --country de --output-file bundes
        test_a_from_cli(
            vec![
                "cielo.exe",
                "--stdout",
                "--season", "17-18",
                "--output-format", "sqlite",
                "--country", "de",
                "--output-file", "bundes"
            ],
            Country::Germany,
            17, 18,
            FileFormat::Json(JsonParser {}),
            FileFormat::Sqlite,
            true,
            {
                let mut of: PathBuf = CWD.clone();
                of.push("bundes.sqlite");
                of
            }
        );

        // cielo.exe --stdout --input-format csv --output-format sqlite --country de --output-file bundes
        test_a_from_cli(
            vec![
                "cielo.exe",
                "--stdout",
                "--input-format", "csv",
                "--output-format", "sqlite",
                "--country", "de",
                "--output-file", "bundes"
            ],
            Country::Germany,
            18, 19,
            FileFormat::Csv(CsvParser {}),
            FileFormat::Sqlite,
            true,
            {
                let mut of: PathBuf = CWD.clone();
                of.push("bundes.sqlite");
                of
            }
        );

        // cielo.exe --stdout --input-format csv --season 17-18 --country de --output-file bundes
        test_a_from_cli(
            vec![
                "cielo.exe",
                "--stdout",
                "--input-format", "csv",
                "--season", "17-18",
                "--country", "de",
                "--output-file", "bundes"
            ],
            Country::Germany,
            17, 18,
            FileFormat::Csv(CsvParser {}),
            FileFormat::Human,
            true,
            {
                let mut of: PathBuf = CWD.clone();
                of.push("bundes.log");
                of
            }
        );

        // cielo.exe --stdout --input-format csv --season 17-18 --output-format sqlite --output-file bundes
        test_a_from_cli(
            vec![
                "cielo.exe",
                "--stdout",
                "--input-format", "csv",
                "--season", "17-18",
                "--output-format", "sqlite",
                "--output-file", "bundes"
            ],
            Country::France,
            17, 18,
            FileFormat::Csv(CsvParser {}),
            FileFormat::Sqlite,
            true,
            {
                let mut of: PathBuf = CWD.clone();
                of.push("bundes.sqlite");
                of
            }
        );

        // cielo.exe --stdout --input-format csv --season 17-18 --output-format sqlite --country de
        test_a_from_cli(
            vec![
                "cielo.exe",
                "--stdout",
                "--input-format", "csv",
                "--season", "17-18",
                "--output-format", "sqlite",
                "--country", "de",
            ],
            Country::Germany,
            17, 18,
            FileFormat::Csv(CsvParser {}),
            FileFormat::Sqlite,
            true,
            {
                let mut of: PathBuf = CWD.clone();
                of.push("data.sqlite");
                of
            }
        );

        // cielo.exe
        test_a_from_cli(
            vec![ "cielo.exe" ],
            Country::France,
            18, 19,
            FileFormat::Json(JsonParser {}),
            FileFormat::Human,
            false,
            {
                let mut of: PathBuf = CWD.clone();
                of.push("data.log");
                of
            }
        );

        // Case with a typo in an arg name.
        println!("\tCase with a typo in an arg name");

        // cielo.exe --stdout --inut-format csv --season 17-18 --output-format sqlite --country de --output-file bundes
        fail_a_from_cli(
            vec![
                "--stdout",
                "--inut-format", "csv",
                "--season", "17-18",
                "--output-format", "sqlite",
                "--country", "de",
                "--output-file", "bundes"
            ],
            vec![
                "Error while parsing args: "
            ]
        );

        // With an additional arg
        println!("\tWith an additional arg");

        // cielo.exe --stdout --input-format csv --season 17-18 --output-format sqlite --country de  --output-file bundes --foo bar
        fail_a_from_cli(
            vec![
                "cielo.exe",
                "--stdout",
                "--input-format", "csv",
                "--season", "17-18",
                "--output-format", "sqlite",
                "--country", "de",
                "--output-file", "bundes",
                "--foo", "bar"
            ],
            vec![
                "Error while parsing args: "
            ]
        );

        // Case with an argument without value
        println!("\tCase with an argument without value");

        // cielo.exe --stdout --input-format csv --season --output-format sqlite --country de  --output-file bundes
        fail_a_from_cli(
            vec![
                "cielo.exe",
                "--stdout",
                "--input-format", "csv",
                "--season",
                "--output-format", "sqlite",
                "--country", "de",
                "--output-file", "bundes"
            ],
            vec![
                format_a_parse_errmsg(
                    "season",
                    "The season has to be written with the first year on two digits, a caret, then the following year on 2 digits."
                ).as_str()
            ]
        );

        // No cielo.exe
        println!("\tNo cielo.exe (should not happen IRL)");

        // --stdout --input-format csv --season 17-18 --output-format sqlite --country de  --output-file bundes
        // --stdout is the executable name for CLI there.
        test_a_from_cli(
            vec![
                "--stdout",
                "--input-format", "csv",
                "--season", "17-18",
                "--output-format", "sqlite",
                "--country", "de",
                "--output-file", "bundes"
            ],
            Country::Germany,
            17, 18,
            FileFormat::Csv(CsvParser {}),
            FileFormat::Sqlite,
            false,
            {
                let mut of: PathBuf = CWD.clone();
                of.push("bundes.sqlite");
                of
            }
        );

        // --input-format csv --season 17-18 --output-format sqlite --country de  --output-file bundes
        // No values for CLI parser => default values!
        test_a_from_cli(
            vec![
                "--input-format", "csv",
                "--season", "17-18",
                "--output-format", "sqlite",
                "--country", "de",
                "--output-file", "bundes"
            ],
            Country::France,
            18, 19,
            FileFormat::Json(JsonParser {}),
            FileFormat::Human,
            false,
            {
                let mut of: PathBuf = CWD.clone();
                of.push("data.log");
                of
            }
        );

        // Special values
        println!("\tSpecial values");

        // cielo.exe --season 09-10
        test_a_from_cli(
            vec![ "cielo.exe", "--season", "09-10", ],
            Country::France,
            9, 10,
            FileFormat::Json(JsonParser {}),
            FileFormat::Human,
            false,
            {
                let mut of: PathBuf = CWD.clone();
                of.push("data.log");
                of
            }
        );

        // cielo.exe --season 04-05
        test_a_from_cli(
            vec![ "cielo.exe", "--season", "04-05", ],
            Country::France,
            4, 5,
            FileFormat::Json(JsonParser {}),
            FileFormat::Human,
            false,
            {
                let mut of: PathBuf = CWD.clone();
                of.push("data.log");
                of
            }
        );

        // cielo.exe --season 00-01
        test_a_from_cli(
            vec![ "cielo.exe", "--season", "00-01" ],
            Country::France,
            0, 1,
            FileFormat::Json(JsonParser {}),
            FileFormat::Human,
            false,
            {
                let mut of: PathBuf = CWD.clone();
                of.push("data.log");
                of
            }
        );

        if cfg!(windows) {
            // cielo.exe -o C:\toto (Windows only)
            test_a_from_cli(
                vec![ "cielo.exe", "-o", r"C:\toto" ],
                Country::France,
                18, 19,
                FileFormat::Json(JsonParser {}),
                FileFormat::Human,
                false,
                PathBuf::from_str(r"C:\toto.log").unwrap()
            );
        }
        else if cfg!(not(windows)) {
            // cielo.exe -o /toto (Unix-style only)
            test_a_from_cli(
                vec![ "cielo.exe", "-o", "/toto" ],
                Country::France,
                18, 19,
                FileFormat::Json(JsonParser {}),
                FileFormat::Human,
                false,
                PathBuf::from_str("/toto.log").unwrap()
            );
        }

        // Multi-arg fail
        println!("\tMulti-arg fail");

        // cielo.exe --stdout --input-format csv --input-format csv
        fail_a_from_cli(
            vec![
                "--stdout",
                "--inut-format", "csv",
                "--inut-format", "csv",
            ],
            vec![
                "Error while parsing args: "
            ]
        );

        // cielo.exe --stdout --input-format csv --input-format json
        fail_a_from_cli(
            vec![
                "--stdout",
                "--inut-format", "csv",
                "--inut-format", "json",
            ],
            vec![
                "Error while parsing args: "
            ]
        );

        // Mismatched args
        println!("\tMismatched args");

        // cielo.exe --country us
        fail_a_from_cli(
            vec![ "cielo.exe", "--country", "us", ],
            vec![
                format_a_parse_errmsg(
                    "country",
                    "us is not a supported country"
                ).as_str()
            ]
        );

        // cielo.exe --season hotot
        fail_a_from_cli(
            vec![ "cielo.exe", "--season", "hotot", ],
            vec![
                format_a_parse_errmsg(
                    "season",
                    "The season has to be written with the first year on two digits, a caret, then the following year on 2 digits."
                ).as_str()
            ]
        );

        // cielo.exe --season 9-10
        fail_a_from_cli(
            vec![ "cielo.exe", "--season", "9-10", ],
            vec![
                format_a_parse_errmsg(
                    "season",
                    "The season has to be written with the first year on two digits, a caret, then the following year on 2 digits."
                ).as_str()
            ]
        );

        // cielo.exe --season 08-9
        fail_a_from_cli(
            vec![ "cielo.exe", "--season", "08-9", ],
            vec![
                format_a_parse_errmsg(
                    "season",
                    "The season has to be written with the first year on two digits, a caret, then the following year on 2 digits."
                ).as_str()
            ]
        );

        // cielo.exe --season 8-9
        fail_a_from_cli(
            vec![ "cielo.exe", "--season", "8-9", ],
            vec![
                format_a_parse_errmsg(
                    "season",
                    "The season has to be written with the first year on two digits, a caret, then the following year on 2 digits."
                ).as_str()
            ]
        );

        // cielo.exe --season 2018-2019
        fail_a_from_cli(
            vec![ "cielo.exe", "--season", "2018-2019", ],
            vec![
                format_a_parse_errmsg(
                    "season",
                    "The season has to be written with the first year on two digits, a caret, then the following year on 2 digits."
                ).as_str()
            ]
        );

        // cielo.exe --season 18-2019
        fail_a_from_cli(
            vec![ "cielo.exe", "--season", "18-2019", ],
            vec![
                format_a_parse_errmsg(
                    "season",
                    "The season has to be written with the first year on two digits, a caret, then the following year on 2 digits."
                ).as_str()
            ]
        );

        // cielo.exe --season 2018-19
        fail_a_from_cli(
            vec![ "cielo.exe", "--season", "2018-19", ],
            vec![
                format_a_parse_errmsg(
                    "season",
                    "The season has to be written with the first year on two digits, a caret, then the following year on 2 digits."
                ).as_str()
            ]
        );

        // cielo.exe --season 10-12
        fail_a_from_cli(
            vec![ "cielo.exe", "--season", "10-12", ],
            vec![
                format_a_parse_errmsg(
                    "season",
                    "The ending year should follow the beginning year."
                ).as_str()
            ]
        );

        // cielo.exe --season 13-12
        fail_a_from_cli(
            vec![ "cielo.exe", "--season", "13-12", ],
            vec![
                format_a_parse_errmsg(
                    "season",
                    "The ending year should follow the beginning year."
                ).as_str()
            ]
        );

        // cielo.exe --season 14-12
        fail_a_from_cli(
            vec![ "cielo.exe", "--season", "14-12", ],
            vec![
                format_a_parse_errmsg(
                    "season",
                    "The ending year should follow the beginning year."
                ).as_str()
            ]
        );

        // cielo.exe --season -
        fail_a_from_cli(
            vec![ "cielo.exe", "--season", "-", ],
            vec![
                format_a_parse_errmsg(
                    "season",
                    "The season has to be written with the first year on two digits, a caret, then the following year on 2 digits."
                ).as_str()
            ]
        );

        // cielo.exe --season 18-
        fail_a_from_cli(
            vec![ "cielo.exe", "--season", "18-", ],
            vec![
                format_a_parse_errmsg(
                    "season",
                    "The season has to be written with the first year on two digits, a caret, then the following year on 2 digits."
                ).as_str()
            ]
        );

        // cielo.exe --season -19
        fail_a_from_cli(
            vec![ "cielo.exe", "--season", "-19", ],
            vec![
                format_a_parse_errmsg(
                    "season",
                    "The season has to be written with the first year on two digits, a caret, then the following year on 2 digits."
                ).as_str()
            ]
        );

        // cielo.exe --season -18-19
        fail_a_from_cli(
            vec![ "cielo.exe", "--season", "-18-19", ],
            vec![
                format_a_parse_errmsg(
                    "season",
                    "The season has to be written with the first year on two digits, a caret, then the following year on 2 digits."
                ).as_str()
            ]
        );

        // cielo.exe --season -18--19
        fail_a_from_cli(
            vec![ "cielo.exe", "--season", "-18--19", ],
            vec![
                format_a_parse_errmsg(
                    "season",
                    "The season has to be written with the first year on two digits, a caret, then the following year on 2 digits."
                ).as_str()
            ]
        );

        // cielo.exe --season 18--19
        fail_a_from_cli(
            vec![ "cielo.exe", "--season", "18--19", ],
            vec![
                format_a_parse_errmsg(
                    "season",
                    "The season has to be written with the first year on two digits, a caret, then the following year on 2 digits."
                ).as_str()
            ]
        );

        // cielo.exe --input-format xml
        fail_a_from_cli(
            vec![ "cielo.exe", "--input-format", "xml", ],
            vec![
                format_a_parse_errmsg(
                    "input-format",
                    "xml is not accepted as an input format, unlike CSV and JSON."
                ).as_str()
            ]
        );

        // cielo.exe --input-format yamelle
        fail_a_from_cli(
            vec![ "cielo.exe", "--input-format", "yamelle", ],
            vec![
                format_a_parse_errmsg(
                    "input-format",
                    "Unknown yamelle file format"
                ).as_str()
            ]
        );

        // cielo.exe --output-format csv
        fail_a_from_cli(
            vec![ "cielo.exe", "--output-format", "csv", ],
            vec![
                format_a_parse_errmsg(
                    "output-format",
                    "csv is not accepted as an output format, unlike XML, YAML, JSON, SQLite and (humanly readable) logs."
                ).as_str()
            ]
        );

        // cielo.exe --output-format yamelle
        fail_a_from_cli(
            vec![ "cielo.exe", "--output-format", "yamelle", ],
            vec![
                format_a_parse_errmsg(
                    "output-format",
                    "Unknown yamelle file format"
                ).as_str()
            ]
        );
    }
}
