use chrono::naive::NaiveDate;
use crate::common::{Country, Season, CieloResult, ParsedValue, Match};
use crate::common::game::get_invalid_match_date;
use crate::common::team::Teams;
use crate::data::datapath;
use crate::data::file_formats::FileFormat;
use crate::data::file_formats::csv::VSR;
use crate::data::traits::{ImportValue, ValueParser};
use crate::elo::{Journey, compute_championship};
use serde_json::Value;
use std::path::PathBuf;

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Championship {
    teams: Teams,
    matches: Vec<Match>,
    start_date: NaiveDate,
    end_date: NaiveDate,
}

impl Championship {
    pub fn from_data(c: Country, s: Season, ifmt: FileFormat) -> CieloResult<Championship> {
        // Retrieving data file.
        let datafile = match datapath(c, s, ifmt) {
            Ok(p) => p,
            Err(e) => return Err(e)
        };

        if cfg!(debug_assertions) {
            println!(
                "Datas are located in {}",
                datafile.to_str().expect("Cannot see the datapath")
            );
        }

        Championship::from_datapath(&datafile, ifmt)
    }

    pub fn from_datapath(datafile: &PathBuf, ifmt: FileFormat) -> CieloResult<Championship> {
        match ifmt {
            FileFormat::Json(parser) => match parser.file_to_value(&datafile) {
                Ok(v) => Self::value_to_self(&v),
                Err(e) => Err(format!("Error while parsing JSON data for championship: {}", e))
            },
            FileFormat::Csv(parser) => match parser.file_to_value(&datafile) {
                Ok(v) => Self::value_to_self(&v),
                Err(e) => Err(format!("Error while parsing CSV data for championship: {}", e))
            },
            _ => Err("Only CSV and JSON are accepted as input formats.".to_string())
        }
    }

    pub fn matches(&self) -> &Vec<Match> {
        &self.matches
    }

    pub fn teams(&self) -> &Teams {
        &self.teams
    }

    pub fn start_date(&self) -> NaiveDate {
        self.start_date
    }

    pub fn end_date(&self) -> NaiveDate {
        self.end_date
    }

    pub fn compute_elo(&self, stdout: bool) -> CieloResult<Journey> {
        compute_championship(&self.teams, &self.matches, stdout)
    }
}

impl ImportValue<Value> for Championship {
    fn value_to_self(v: &Value) -> CieloResult<Championship> {
        if !v.is_array() {
            return Err("Championship are built from arrays.".to_string())
        }

        let m_list: Vec<Value> = v.as_array()
            .expect("Championship value is supposed to be an array.").clone();

        vec_to_self(&m_list)
    }
}

impl ImportValue<VSR> for Championship {
    fn value_to_self(v: &VSR) -> CieloResult<Championship> {
        vec_to_self(v)
    }
}

fn vec_to_self<V: ParsedValue>(m_list: &Vec<V>) -> CieloResult<Championship> where Match: ImportValue<V> {
    let mut err_msgs: Vec<String> = vec![];

    // Filling match list
    let mut matches: Vec<Match> = m_list.iter()
        .map(|mv|{
            let mv_res = Match::value_to_self(mv);

            if let Err(e) = &mv_res {
                err_msgs.push(format!("Cannot parse the match: {e}.", e = e.to_string()))
            }

            mv_res
        })
        .filter(|r| { r.is_ok() })
        .map(|rm| {
            rm.expect("Errors are supposed to be filtered")
        })
        .collect();


    // Filling Team set
    let mut teams: Teams = Teams::new();

    matches.iter().for_each(|m| {
        teams.insert(m.home_team());
        teams.insert(m.away_team());
    });

    // Building result
    if err_msgs.is_empty() && !matches.is_empty() {
        // No errors => Let's return the Championship.

        // Sort matches before
        matches.sort_by(|m1, m2| {
            m1.date().cmp(&m2.date())
        });

        let start_date: NaiveDate = matches.first().expect("There are matches in this championship.").date();
        let end_date: NaiveDate = matches.last().expect("There are matches in this championship.").date();

        Ok(Championship { teams, matches, start_date, end_date })
    }
    else if err_msgs.is_empty() && matches.is_empty() {
        // Return an empty championship
        Ok(Championship {
            teams: Teams::new(),
            matches: vec![],
            start_date: get_invalid_match_date(),
            end_date: get_invalid_match_date()
        })
    }
    else {
        // There were errors during parsing. Return them.
        Err(format!("The following things happened while parsing: {}", err_msgs.join(".\n")))
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::data::file_formats::csv::CsvParser;
    use crate::data::file_formats::json::JsonParser;
    use fs_extra::dir;
    use std::env;
    use std::path::PathBuf;
    use csv::StringRecord;

    // TODO: compute_elo tests.

    fn copy_data_4_tests() {
        let mut root_data = PathBuf::from(match env::var("CARGO_MANIFEST_DIR") {
            Ok(cwd) => cwd,
            Err(e) => panic!("Error while fetching current dir: {}", e.to_string())
        });
        root_data.push("data");

        let mut execdir_data = PathBuf::from(env::current_exe()
            .expect("Test executable should have a path."))
            .parent().expect("Test executable should have a parent")
            .to_path_buf();

        execdir_data.push("data");

        if let Err(e) = dir::remove(&execdir_data) {
            eprintln!("Error while deleting data files in target: {}", e.to_string());
        }

        let c_opt = dir::CopyOptions {
            overwrite: true,
            copy_inside: true,
            skip_exist: false,
            depth: 3,
            buffer_size: 1_000_000
        };

        match dir::copy(&root_data, &execdir_data, &c_opt) {
            Ok(_u) => println!(
                "{} copied at {}",
                root_data.to_str().expect("Cannot display root data path."),
                execdir_data.to_str().expect("Cannot display execdir data path.")
            ),
            Err(e) =>  {
                eprintln!("Error while copying data files in target: {}", e.to_string());
                eprintln!("Deleting copied files");

                match dir::remove(&execdir_data) {
                    Ok(_rr) => eprintln!("Rollback completed!"),
                    Err(er) => eprintln!("Error during rollback: {}", er.to_string())
                }
            }
        }
    }

    fn remove_data_4_tests() {
        let mut execdir_data = PathBuf::from(env::current_exe()
            .expect("Test executable should have a path."))
            .parent().expect("Test executable should have a parent")
            .to_path_buf();

        execdir_data.push("data");

        if let Err(e) = dir::remove(&execdir_data) {
            eprintln!("Error while deleting data files in target: {}", e.to_string());
        }
    }

    fn test_match_ordering(championship: &Championship) {
        assert!(championship.start_date() <= championship.end_date());

        let mut dates: Vec<NaiveDate> = championship.matches().iter().map(|m| { m.date() }).collect();
        dates.push(championship.end_date());
        dates.insert(0,championship.start_date());

        let mut dit = dates.iter().peekable();

        while let Some(d) = dit.next() {
            if let Some(nd) = dit.peek() {
                assert!(d <= nd);
            }
        }
    }

    #[test]
    fn championship_from_data() {
        println!("\n\tImporting data for tests");
        // Have to do this since tst executable is not in the same folder than the program executable.
        copy_data_4_tests();

        println!("\tWrong input format");
        let c_res = Championship::from_data(
            Country::Italy,
            Season::const_new(11, 12),
            FileFormat::Sqlite
        );
        assert!(c_res.is_err());
        assert_eq!(
            c_res.expect_err("Championship with SQLite is supposed to be wrong"),
            "Only CSV and JSON are accepted as input formats.".to_string()
        );

        println!("\tChampionship from JSON");

        println!("\t\tNominal case");
        let c_res = Championship::from_data(
            Country::France,
            Season::const_new(18, 19),
            FileFormat::Json(JsonParser {})
        );
        assert!(c_res.is_ok(), "It should be OK but got the following error instead: {}", c_res.expect_err("It is supposed to be an error."));
        let championship = c_res.expect("There should have a championship");
        assert_eq!(championship.matches().len(), 380);
        assert_eq!(championship.teams().len(), 20);
        test_match_ordering(&championship);

        println!("\t\tWrong season");
        let c_res = Championship::from_data(
            Country::France,
            Season::const_new(5, 6),
            FileFormat::Json(JsonParser {})
        );
        assert!(c_res.is_err());
        let err_msg = c_res.expect_err("Wrong season implies an error");
        assert!(
            err_msg.starts_with("Error while parsing JSON data for championship: "),
            "Error message should start with 'Error while parsing JSON data for championship: '. Got this instead: '{}'.", err_msg
        );

        println!("\tChampionship from CSV");

        println!("\t\tNominal case");
        let c_res = Championship::from_data(
            Country::France,
            Season::const_new(18, 19),
            FileFormat::Csv(CsvParser {})
        );
        assert!(c_res.is_ok());
        let championship = c_res.expect("There should have a championship");
        assert_eq!(championship.matches().len(), 380);
        assert_eq!(championship.teams().len(), 20);
        test_match_ordering(&championship);

        println!("\t\tWrong season");
        let c_res = Championship::from_data(
            Country::France,
            Season::const_new(5, 6),
            FileFormat::Csv(CsvParser {})
        );
        assert!(c_res.is_err());
        let err_msg = c_res.expect_err("Wrong season implies an error");
        assert!(
            err_msg.starts_with("Error while parsing CSV data for championship: "),
            "Error message should start with 'Error while parsing CSV data for championship: '. Got this instead: '{}'.", err_msg
        );

        println!("\tRemoving data for tests");
        remove_data_4_tests();
    }

    #[test]
    fn championship_from_datapath() {
        let furiani: NaiveDate = get_invalid_match_date();
        let mut tests_dir_root = PathBuf::from(
            env::var("CARGO_MANIFEST_DIR")
                .expect("$env:CARGO_MANIFEST_DIR should exist.")
                .as_str()
        );
        tests_dir_root.push("tests");
        tests_dir_root.push("data");

        println!("\n\tWith wrong arguments for input format");
        let c_res = Championship::from_datapath(
            &env::current_exe().expect("Should have exe path"),
            FileFormat::Sqlite
        );
        assert!(c_res.is_err());
        assert_eq!(
            c_res.expect_err("Championship with SQLite is supposed to be wrong"),
            "Only CSV and JSON are accepted as input formats.".to_string()
        );

        println!("\tChampionship from JSON");

        println!("\t\tNominal case");
        let mut df = tests_dir_root.clone();
        df.push("test_ch.json");

        let c_res = Championship::from_datapath(&df, FileFormat::Json(JsonParser {}));
        assert!(c_res.is_ok());
        let championship = c_res.expect("There should have a championship");
        assert_eq!(championship.matches().len(), 75);
        assert_eq!(championship.teams().len(), 20);
        test_match_ordering(&championship);

        println!("\t\tEmpty list of matches");
        let mut df = tests_dir_root.clone();
        df.push("empty_json_list.json");

        let c_res = Championship::from_datapath(&df, FileFormat::Json(JsonParser {}));
        assert!(c_res.is_ok());
        let championship = c_res.expect("There should have a championship");
        assert_eq!(championship.matches().len(), 0);
        assert_eq!(championship.teams().len(), 0);
        assert_eq!(championship.start_date(), furiani);
        assert_eq!(championship.end_date(), furiani);
        test_match_ordering(&championship);

        println!("\t\tWhen JSON is not a list");
        let mut df = tests_dir_root.clone();
        df.push("test_obj.json");;

        let c_res = Championship::from_datapath(&df, FileFormat::Json(JsonParser {}));
        assert!(c_res.is_err());
        let err_msg = c_res.expect_err("It is supposed to be an error.");
        let start_em = "Championship are built from arrays.";
        assert_eq!(
            err_msg, start_em.to_string(),
            "Error message should be '{sem}'. Got this instead: '{e}'.",
            sem = start_em, e = err_msg
        );

        println!("\t\tWith an error in a match");
        let mut df = tests_dir_root.clone();
        df.push("failed_json_list.json");;

        let c_res = Championship::from_datapath(&df, FileFormat::Json(JsonParser {}));
        assert!(c_res.is_err());
        let err_msg = c_res.expect_err("It is supposed to be an error.");
        let start_em = "The following things happened while parsing: ";
        assert!(
            err_msg.starts_with(start_em),
            "Error message should be '{sem}'. Got this instead: '{e}'.",
            sem = start_em, e = err_msg
        );

        println!("\t\tWith an error in the JSON structure");
        let mut df = tests_dir_root.clone();
        df.push("failing_json_list.json");;

        let c_res = Championship::from_datapath(&df, FileFormat::Json(JsonParser {}));
        assert!(c_res.is_err());
        let err_msg = c_res.expect_err("It is supposed to be an error.");
        let start_em = "Error while parsing JSON data for championship: Cannot get JSON value of data: ";
        assert!(
            err_msg.starts_with(start_em),
            "Error message should be '{sem}'. Got this instead: '{e}'.",
            sem = start_em, e = err_msg
        );

        println!("\t\tWith a not-JSON file");
        let mut df = tests_dir_root.clone();
        df.push("test_ho.csv");;

        let c_res = Championship::from_datapath(&df, FileFormat::Json(JsonParser {}));
        assert!(c_res.is_err());
        let err_msg = c_res.expect_err("It is supposed to be an error.");
        let start_em = "Error while parsing JSON data for championship: Cannot get JSON value of data: ";
        assert!(
            err_msg.starts_with(start_em),
            "Error message should be '{sem}'. Got this instead: '{e}'.",
            sem = start_em, e = err_msg
        );

        println!("\t\tWith a file which does not exist");
        let mut df = tests_dir_root.clone();
        df.push("404.jsonnotfound");;

        let c_res = Championship::from_datapath(&df, FileFormat::Json(JsonParser {}));
        assert!(c_res.is_err());
        let err_msg = c_res.expect_err("It is supposed to be an error.");
        let start_em = "Error while parsing JSON data for championship: ";
        assert!(
            err_msg.starts_with(start_em),
            "Error message should be '{sem}'. Got this instead: '{e}'.",
            sem = start_em, e = err_msg
        );

        println!("\t\tEmpty file");
        let mut df = tests_dir_root.clone();
        df.push("empty");

        let c_res = Championship::from_datapath(&df, FileFormat::Json(JsonParser {}));
        assert!(c_res.is_err());
        let err_msg = c_res.expect_err("It is supposed to be an error.");
        let start_em = "Error while parsing JSON data for championship: Cannot get JSON value of data: ";
        assert!(
            err_msg.starts_with(start_em),
            "Error message should be '{sem}'. Got this instead: '{e}'.",
            sem = start_em, e = err_msg
        );

        println!("\tChampionship from CSV");

        println!("\t\tNominal case");
        let mut df = tests_dir_root.clone();
        df.push("test_ch.csv");

        let c_res = Championship::from_datapath(&df, FileFormat::Csv(CsvParser {}));
        assert!(c_res.is_ok());
        let championship = c_res.expect("There should have a championship");
        assert_eq!(championship.matches().len(), 11);
        assert_eq!(championship.teams().len(), 20);
        test_match_ordering(&championship);

        println!("\t\tEmpty list of matches");

        println!("\t\t\tHeader-only");
        let mut df = tests_dir_root.clone();
        df.push("empty_csv.csv");

        let c_res = Championship::from_datapath(&df, FileFormat::Csv(CsvParser {}));
        assert!(c_res.is_ok());
        let championship = c_res.expect("There should have a championship");
        assert_eq!(championship.matches().len(), 0);
        assert_eq!(championship.teams().len(), 0);
        test_match_ordering(&championship);
        assert_eq!(championship.start_date(), furiani);
        assert_eq!(championship.end_date(), furiani);

        println!("\t\t\tEmpty file");
        let mut df = tests_dir_root.clone();
        df.push("empty");

        let c_res = Championship::from_datapath(&df, FileFormat::Csv(CsvParser {}));
        assert!(c_res.is_ok());
        let championship = c_res.expect("There should have a championship");
        assert_eq!(championship.matches().len(), 0);
        assert_eq!(championship.teams().len(), 0);
        test_match_ordering(&championship);
        assert_eq!(championship.start_date(), furiani);
        assert_eq!(championship.end_date(), furiani);

        println!("\t\tWith an error in a match");
        let mut df = tests_dir_root.clone();
        df.push("failed_csv.csv");

        let c_res = Championship::from_datapath(&df, FileFormat::Csv(CsvParser {}));
        assert!(c_res.is_err());
        let err_msg = c_res.expect_err("It is supposed to be an error.");
        let start_em = "The following things happened while parsing: ";
        assert!(
            err_msg.starts_with(start_em),
            "Error message should be '{sem}'. Got this instead: '{e}'.",
            sem = start_em, e = err_msg
        );

        println!("\t\tWith a not-CSV file");
        let mut df = tests_dir_root.clone();
        df.push("test_ch.json");

        let c_res = Championship::from_datapath(&df, FileFormat::Csv(CsvParser {}));
        assert!(c_res.is_err());
        let err_msg = c_res.expect_err("It is supposed to be an error.");
        let start_em = "Error while parsing CSV data for championship: Something happened while parsing CSV datas: ";
        assert!(
            err_msg.starts_with(start_em),
            "Error message should be '{sem}'. Got this instead: '{e}'.",
            sem = start_em, e = err_msg
        );

        println!("\t\tWith a file which does not exist");
        let mut df = tests_dir_root.clone();
        df.push("404.csvnotfound");

        let c_res = Championship::from_datapath(&df, FileFormat::Csv(CsvParser {}));
        assert!(c_res.is_err());
        let err_msg = c_res.expect_err("It is supposed to be an error.");
        let start_em = "Error while parsing CSV data for championship: Cannot read the CSV file located at '";
        assert!(
            err_msg.starts_with(start_em),
            "Error message should be '{sem}'. Got this instead: '{e}'.",
            sem = start_em, e = err_msg
        );
    }

    #[test]
    fn val2s_json() {
        println!("\n\tNominal case");
        let m_list: Value = serde_json::json!([
            {
                "AF": 9, "AR": 0, "AY": 3, "AwayTeam": "Lorient", "Date": "07/08/10", "FTAG": 2,
                "FTHG": 2, "HR": 0, "HTAG": 1, "HTHG": 1, "HY": 3, "HomeTeam": "Auxerre", "HF": 14
            },
            {
                "AF": 18, "AR": 1, "AY": 1, "AwayTeam": "Nancy", "Date": "07/12/10", "FTAG": 2,
                "FTHG": 1, "HF": 17, "HR": 0,  "HTAG": 1, "HTHG": 0, "HY": 3, "HomeTeam": "Lens"
            }
        ]);

        let c_res = Championship::value_to_self(&m_list);
        assert!(c_res.is_ok());
        let championship = c_res.expect("The championship should be OK");
        assert_eq!(championship.teams().len(), 4);
        assert_eq!(championship.matches().len(), 2);
        test_match_ordering(&championship);

        println!("\tEmpty list of matches");
        let m_list: Value = serde_json::json!([]);

        let c_res = Championship::value_to_self(&m_list);
        assert!(c_res.is_ok());
        let championship = c_res.expect("The championship should be OK");
        assert_eq!(championship.teams().len(), 0);
        assert_eq!(championship.matches().len(), 0);
        test_match_ordering(&championship);
        assert_eq!(championship.start_date(), get_invalid_match_date());
        assert_eq!(championship.end_date(), get_invalid_match_date());

        println!("\tWhen JSON is not a list of objects");
        let m_list: Value = serde_json::json!([2, 7, -4]);

        let c_res = Championship::value_to_self(&m_list);
        assert!(c_res.is_err());
        let err_msg = c_res.expect_err("It should be an error.");
        let start_em = "The following things happened while parsing: \
            Cannot parse the match: JSON value for a match should be a JSON object...\nCannot \
            parse the match: JSON value for a match should be a JSON object...\nCannot parse \
            the match: JSON value for a match should be a JSON object..";
        assert_eq!(
            err_msg, start_em,
            "The error message should be '{sem}'. Got this instead: '{e}'.",
            sem = start_em, e = err_msg
        );

        println!("\tWhen JSON is not a list at all");
        let m_list: Value = serde_json::json!({"streamer": "VeeDotMee", "game": "Dead Cells"});

        let c_res = Championship::value_to_self(&m_list);
        assert!(c_res.is_err());
        let err_msg = c_res.expect_err("It should be an error.");
        let start_em = "Championship are built from arrays.";
        assert_eq!(
            err_msg, start_em,
            "The error message should be '{sem}'. Got this instead: '{e}'.",
            sem = start_em, e = err_msg
        );

        println!("\tWith an error in a match");
        let m_list: Value = serde_json::json!([
            {
                "AF": "FF7", "AR": 0, "AY": 3, "AwayTeam": "Lorient", "Date": "07/08/10", "FTAG": 2,
                "FTHG": 2, "HR": 0, "HTAG": 1, "HTHG": 1, "HY": 3, "HomeTeam": "Auxerre", "HF": 14
            },
            {
                "AF": 18, "AR": 1, "AY": 1, "AwayTeam": "Nancy", "Date": "07/12/10", "FTAG": 2,
                "FTHG": 1, "HF": 17, "HR": 0,  "HTAG": 1, "HTHG": 0, "HY": 3, "HomeTeam": "Lens"
            }
        ]);

        let c_res = Championship::value_to_self(&m_list);
        assert!(c_res.is_err());
        let err_msg = c_res.expect_err("It should be an error.");
        let start_em = "The following things happened while parsing: Cannot parse the match: ";
        assert!(
            err_msg.starts_with(start_em),
            "The error message should start with '{sem}'. Got this instead: '{e}'.",
            sem = start_em, e = err_msg
        );
    }

    #[test]
    fn val2s_csv() {
        println!("\n\tNominal case");
        let m_list: VSR = vec![
            "E0,17/08/13,Arsenal,Aston Villa,1,3,A,1,1,D,A Taylor,16,9,4,4,15,18,4,3,4,5,1,0,1.44,\
                4.75,8,1.36,5,7.75,1.37,4.6,7.5,1.4,4.5,7.5,1.41,5.2,8.3,1.36,4.8,8.5,1.4,4.5,7.5,\
                1.44,4.8,7.5,33,1.44,1.4,5.2,4.68,9.7,7.87,22,1.62,1.56,2.56,2.37,,,,,,,1.\
                44,5,8.05",
            "E0,17/08/13,Liverpool,Stoke,1,0,H,1,0,H,M Atkinson,26,10,11,4,11,11,12,6,1,1,0,0,1.\
                4,5,9.5,1.4,4.33,8.25,1.4,4.4,7.3,1.44,4.2,7.5,1.41,4.88,9.35,1.36,4.6,9,1.36,4.5,\
                9.5,1.4,5,9.5,32,1.44,1.38,4.95,4.54,10,8.88,30,1.86,1.79,2.12,2.02,,,,,,,1.42,\
                4.62,10.19",
            "E0,17/08/13,Norwich,Everton,2,2,D,0,0,D,M Oliver,8,19,2,6,13,10,6,8,2,0,0,0,3.2,3.4,\
                2.4,3.1,3.25,2.3,2.9,3.3,2.3,3,3.3,2.3,3.32,3.41,2.35,3.1,3.3,2.3,3,3.4,2.3,3.12,\
                3.4,2.4,33,3.35,3.12,3.41,3.27,2.4,2.31,31,2.08,1.98,1.91,1.82,,,,,,,3.81,\
                3.27,2.21",
            "E0,17/08/13,Sunderland,Fulham,0,1,A,0,0,D,N Swarbrick,20,5,3,1,14,14,6,1,0,3,0,0,2.3,\
                3.4,3.4,2.25,3.2,3.25,2.2,3.2,3.2,2.2,3.25,3.3,2.25,3.37,3.58,2.2,3.3,3.3,2.25,3.3,\
                3.3,2.3,3.4,3.3,33,2.3,2.22,3.45,3.3,3.58,3.29,31,2.14,2.04,1.84,1.77,,,,,,,2.52,\
                3.23,3.16",
            "E0,17/08/13,Swansea,Man United,1,4,A,0,2,A,P Dowd,17,15,6,7,13,10,7,4,1,3,0,0,4.2,3.5,\
                2,4.1,3.5,1.87,4.2,3.5,1.8,4,3.5,1.9,4.1,3.52,2.03,4,3.4,1.91,4,3.6,1.9,3.8,\
                3.6,2.05,33,4.33,3.84,3.6,3.4,2.1,1.99,31,1.93,1.83,2.06,1.98,,,,,,,3.62,3.41,2.22",
            "E0,17/08/13,West Brom,Southampton,0,1,A,0,0,D,K Friend,11,7,1,2,14,24,4,8,4,0,0,0,2.4,\
                3.4,3.2,2.3,3.25,3.1,2.2,3.3,3.1,2.2,3.3,3.25,2.42,3.38,3.21,2.25,3.3,\
                3.2,2.25,3.4,3.2,2.4,3.4,3.12,33,2.43,2.31,3.45,3.33,3.3,3.07,31,2.02,1.87,\
                2.02,1.93,,,,,,,2.56,3.3,3.06",
            "E0,17/08/13,West Ham,Cardiff,2,0,H,1,0,H,H Webb,18,12,4,1,10,7,4,3,0,1,0,0,2,3.6,4,\
                1.91,3.4,4,1.9,3.45,3.8,1.9,3.4,4,1.99,3.57,4.21,1.95,3.5,3.8,1.91,3.5,4,2,3.6,\
                4,33,2,1.94,3.65,3.47,4.4,3.94,31,2,1.89,2,1.9,,,,,,,2.17,3.36,3.84",
            "E0,18/08/13,Chelsea,Hull,2,0,H,2,0,H,J Moss,22,7,5,2,7,16,5,1,0,1,0,0,1.2,7,21,1.19,\
                6.25,15,1.2,5.8,13,1.2,6.5,15,1.2,7.4,19.5,1.2,6,15,1.2,6.5,15,1.22,6.5,15,32,\
                1.22,1.19,7.7,6.68,21,15.53,22,1.59,1.53,2.63,2.45,,,,,,,1.23,6.8,16.25",
            "E0,18/08/13,Crystal Palace,Tottenham,0,1,A,0,0,D,M Clattenburg,5,17,3,2,6,9,3,7,1,0,\
                0,0,4.75,3.75,1.83,4.75,3.7,1.72,4.2,3.5,1.8,4.5,3.5,1.8,5.16,3.69,1.8,4.75,\
                3.6,1.75,4.8,3.8,1.73,4.5,3.75,1.85,33,5.2,4.66,3.95,3.61,1.85,1.77,31,1.89,\
                1.81,2.12,1.99,,,,,,,6.73,3.88,1.63",
            "E0,19/08/13,Man City,Newcastle,4,0,H,2,0,H,A Marriner,20,5,11,1,9,7,8,1,2,3,0,1,1.33,\
                5.5,11,1.3,5.25,9.75,1.3,5,9,1.28,5.5,10,1.31,5.86,11.66,1.29,5.5,10,1.3,5.25,10,\
                1.33,5.5,10.5,30,1.33,1.3,5.86,5.27,12,10.35,27,1.68,1.6,2.44,2.3,,,,\
                ,,,1.26,6.41,14.02",
            "E0,21/08/13,Chelsea,Aston Villa,2,1,H,1,1,D,K Friend,15,7,3,3,12,13,1,2,1,4,0,0,1.29,\
                5.5,10,1.28,5.25,10,1.3,5,9,1.29,5.5,9.5,1.3,5.89,11.9,1.3,5.5,9.5,1.3,5.25,\
                10,1.29,5.75,12,34,1.33,1.29,6.4,5.42,14,10.17,22,1.62,1.55,2.56,2.38,21,-1.5,1.88,\
                1.84,2.14,2.03,1.28,6.48,12.18",
            "E0,24/08/13,Aston Villa,Liverpool,0,1,A,0,1,A,M Clattenburg,17,5,3,1,9,8,8,2,3,3,0,0,\
                4,3.75,1.95,3.8,3.6,1.91,3.6,3.3,2,4,3.5,1.91,4.18,3.83,1.93,3.75,3.6,1.95,4,3.5,\
                1.91,3.9,3.8,1.95,34,4.2,3.93,3.83,3.55,2,1.91,32,1.83,1.73,2.22,2.1,18,0.5,2.01,\
                1.96,1.96,1.92,4.28,4.04,1.85",
            "E0,24/08/13,Everton,West Brom,0,0,D,0,0,D,R East,22,7,8,2,14,15,11,1,1,1,0,0,1.57,\
                4.33,6.5,1.53,4.1,6,1.55,3.9,5.6,1.57,3.75,6,1.55,4.33,7.01,1.55,4,6,1.57,4,6,\
                1.6,4.2,6,34,1.6,1.55,4.35,3.94,7.1,6.27,32,1.89,1.79,2.1,2.02,21,-1,2.05,1.92,\
                2,1.94,1.58,4.13,6.88",
            "E0,24/08/13,Fulham,Arsenal,1,3,A,0,2,A,H Webb,16,18,7,7,10,8,1,8,2,2,0,0,3.8,3.5,\
                2.1,3.6,3.5,2,3.3,3.3,2.1,3.5,3.4,2.1,3.7,3.58,2.13,3.4,3.5,2.1,3.5,3.4,2.1,3.5,\
                3.5,2.15,34,3.8,3.51,3.58,3.38,2.15,2.1,32,1.88,1.8,2.12,2,18,0.5,1.82,1.79,\
                2.15,2.09,4.78,3.6,1.88"
        ].iter().map(|r| {
            let record: Vec<&str> = r.split(",").collect();
            StringRecord::from(record)
        }).collect();
        let c_res = Championship::value_to_self(&m_list);
        assert!(c_res.is_ok());
        let championship = c_res.expect("Championship is supposed to be OK.");
        assert_eq!(championship.teams().len(), 20);
        assert_eq!(championship.matches().len(), 14);
        test_match_ordering(&championship);


        println!("\tEmpty list of matches");
        let m_list: VSR = vec![];
        let c_res = Championship::value_to_self(&m_list);
        assert!(c_res.is_ok());
        let championship = c_res.expect("Championship is supposed to be OK.");
        assert_eq!(championship.teams().len(), 0);
        assert_eq!(championship.matches().len(), 0);
        test_match_ordering(&championship);
        assert_eq!(championship.start_date(), get_invalid_match_date());
        assert_eq!(championship.end_date(), get_invalid_match_date());


        println!("\tWith an error in a match");
        let m_list: VSR = vec![
            "E0,17/08/13,Arsenal,Aston Villa,1,Jean_Claude,A,1,1,D,A Taylor,16,9,4,4,15,18,4,3,4,5,1,0,1.44,\
                4.75,8,1.36,5,7.75,1.37,4.6,7.5,1.4,4.5,7.5,1.41,5.2,8.3,1.36,4.8,8.5,1.4,4.5,7.5,\
                1.44,4.8,7.5,33,1.44,1.4,5.2,4.68,9.7,7.87,22,1.62,1.56,2.56,2.37,,,,,,,1.\
                44,5,8.05",
            "E0,17/08/13,Liverpool,Stoke,1,0,H",
            "E0,17/08/13,Norwich,Everton,2,2,D,0,0,D,M Oliver,8,19,2,6,13,10,6,8,2,0,0,0,3.2,3.4,\
                2.4,3.1,3.25,2.3,2.9,3.3,2.3,3,3.3,2.3,3.32,3.41,2.35,3.1,3.3,2.3,3,3.4,2.3,3.12,\
                3.4,2.4,33,3.35,3.12,3.41,3.27,2.4,2.31,31,2.08,1.98,1.91,1.82,,,,,,,3.81,\
                3.27,2.21",
            "E0,17/08/13,Sunderland,Fulham,0,1,A,0,0,D,N Swarbrick,20,5,3,1,14,14,6,1,0,3,0,0,2.3,\
                3.4,3.4,2.25,3.2,3.25,2.2,3.2,3.2,2.2,3.25,3.3,2.25,3.37,3.58,2.2,3.3,3.3,2.25,3.3,\
                3.3,2.3,3.4,3.3,33,2.3,2.22,3.45,3.3,3.58,3.29,31,2.14,2.04,1.84,1.77,,,,,,,2.52,\
                3.23,3.16",
            "E0,17/08/13,Swansea,Man United,1,4,A,0,2,A,P Dowd,17,15,6,7,13,10,7,4,1,3,0,0,4.2,3.5,\
                2,4.1,3.5,1.87,4.2,3.5,1.8,4,3.5,1.9,4.1,3.52,2.03,4,3.4,1.91,4,3.6,1.9,3.8,\
                3.6,2.05,33,4.33,3.84,3.6,3.4,2.1,1.99,31,1.93,1.83,2.06,1.98,,,,,,,3.62,3.41,2.22",
            "E0,17/08/13,West Brom,Southampton,0,1,A,0,0,D,K Friend,11,7,1,2,14,24,4,8,4,0,0,0,2.4,\
                3.4,3.2,2.3,3.25,3.1,2.2,3.3,3.1,2.2,3.3,3.25,2.42,3.38,3.21,2.25,3.3,\
                3.2,2.25,3.4,3.2,2.4,3.4,3.12,33,2.43,2.31,3.45,3.33,3.3,3.07,31,2.02,1.87,\
                2.02,1.93,,,,,,,2.56,3.3,3.06"
        ].iter().map(|r| {
            let record: Vec<&str> = r.split(",").collect();
            StringRecord::from(record)
        }).collect();
        let c_res = Championship::value_to_self(&m_list);
        assert!(c_res.is_err());
        let err_msg = c_res.expect_err("Error if errors in records.");
        assert!(
            err_msg.starts_with("The following things happened while parsing: "),
            "The error message should begin with 'The following things happened while parsing: '. \
                Got this instead: '{}'.", err_msg
        );
    }

    #[test]
    fn vec2s_json() {
        println!("\n\tNominal case");
        let m_list: Vec<Value> = vec![
            serde_json::json!({
                "AF": 9, "AR": 0, "AY": 3, "AwayTeam": "Lorient", "Date": "07/08/10", "FTAG": 2,
                "FTHG": 2, "HR": 0, "HTAG": 1, "HTHG": 1, "HY": 3, "HomeTeam": "Auxerre", "HF": 14
            }),
            serde_json::json!({
                "AF": 18, "AR": 1, "AY": 1, "AwayTeam": "Nancy", "Date": "07/12/10", "FTAG": 2,
                "FTHG": 1, "HF": 17, "HR": 0,  "HTAG": 1, "HTHG": 0, "HY": 3, "HomeTeam": "Lens"
            })
        ];

        let c_res = vec_to_self(&m_list);
        assert!(c_res.is_ok());
        let championship = c_res.expect("The championship should be OK");
        assert_eq!(championship.teams().len(), 4);
        assert_eq!(championship.matches().len(), 2);
        test_match_ordering(&championship);

        println!("\tEmpty list of matches");
        let m_list: Vec<Value> = vec![];

        let c_res = vec_to_self(&m_list);
        assert!(c_res.is_ok());
        let championship = c_res.expect("The championship should be OK");
        assert_eq!(championship.teams().len(), 0);
        assert_eq!(championship.matches().len(), 0);
        test_match_ordering(&championship);
        assert_eq!(championship.start_date(), get_invalid_match_date());
        assert_eq!(championship.end_date(), get_invalid_match_date());

        println!("\tWhen JSON is not a list of objects");
        let m_list: Vec<Value> = vec![
            serde_json::json!(2),
            serde_json::json!(7),
            serde_json::json!(-4),
        ];

        let c_res = vec_to_self(&m_list);
        assert!(c_res.is_err());
        let err_msg = c_res.expect_err("It should be an error.");
        let start_em = "The following things happened while parsing: \
            Cannot parse the match: JSON value for a match should be a JSON object...\nCannot \
            parse the match: JSON value for a match should be a JSON object...\nCannot parse \
            the match: JSON value for a match should be a JSON object..";
        assert_eq!(
            err_msg, start_em,
            "The error message should be '{sem}'. Got this instead: '{e}'.",
            sem = start_em, e = err_msg
        );

        println!("\tWith an error in a match");
        let m_list: Vec<Value> = vec![
            serde_json::json!({
                "AF": "FF7", "AR": 0, "AY": 3, "AwayTeam": "Lorient", "Date": "07/08/10", "FTAG": 2,
                "FTHG": 2, "HR": 0, "HTAG": 1, "HTHG": 1, "HY": 3, "HomeTeam": "Auxerre", "HF": 14
            }),
            serde_json::json!({
                "AF": 18, "AR": 1, "AY": 1, "AwayTeam": "Nancy", "Date": "07/12/10", "FTAG": 2,
                "FTHG": 1, "HF": 17, "HR": 0,  "HTAG": 1, "HTHG": 0, "HY": 3, "HomeTeam": "Lens"
            })
        ];

        let c_res = vec_to_self(&m_list);
        assert!(c_res.is_err());
        let err_msg = c_res.expect_err("It should be an error.");
        let start_em = "The following things happened while parsing: Cannot parse the match: ";
        assert!(
            err_msg.starts_with(start_em),
            "The error message should start with '{sem}'. Got this instead: '{e}'.",
            sem = start_em, e = err_msg
        );
    }

    #[test]
    fn vec2s_csv() {println!("\n\tNominal case");
        let m_list: VSR = vec![
            "E0,17/08/13,Arsenal,Aston Villa,1,3,A,1,1,D,A Taylor,16,9,4,4,15,18,4,3,4,5,1,0,1.44,\
                4.75,8,1.36,5,7.75,1.37,4.6,7.5,1.4,4.5,7.5,1.41,5.2,8.3,1.36,4.8,8.5,1.4,4.5,7.5,\
                1.44,4.8,7.5,33,1.44,1.4,5.2,4.68,9.7,7.87,22,1.62,1.56,2.56,2.37,,,,,,,1.\
                44,5,8.05",
            "E0,17/08/13,Liverpool,Stoke,1,0,H,1,0,H,M Atkinson,26,10,11,4,11,11,12,6,1,1,0,0,1.\
                4,5,9.5,1.4,4.33,8.25,1.4,4.4,7.3,1.44,4.2,7.5,1.41,4.88,9.35,1.36,4.6,9,1.36,4.5,\
                9.5,1.4,5,9.5,32,1.44,1.38,4.95,4.54,10,8.88,30,1.86,1.79,2.12,2.02,,,,,,,1.42,\
                4.62,10.19",
            "E0,17/08/13,Norwich,Everton,2,2,D,0,0,D,M Oliver,8,19,2,6,13,10,6,8,2,0,0,0,3.2,3.4,\
                2.4,3.1,3.25,2.3,2.9,3.3,2.3,3,3.3,2.3,3.32,3.41,2.35,3.1,3.3,2.3,3,3.4,2.3,3.12,\
                3.4,2.4,33,3.35,3.12,3.41,3.27,2.4,2.31,31,2.08,1.98,1.91,1.82,,,,,,,3.81,\
                3.27,2.21",
            "E0,17/08/13,Sunderland,Fulham,0,1,A,0,0,D,N Swarbrick,20,5,3,1,14,14,6,1,0,3,0,0,2.3,\
                3.4,3.4,2.25,3.2,3.25,2.2,3.2,3.2,2.2,3.25,3.3,2.25,3.37,3.58,2.2,3.3,3.3,2.25,3.3,\
                3.3,2.3,3.4,3.3,33,2.3,2.22,3.45,3.3,3.58,3.29,31,2.14,2.04,1.84,1.77,,,,,,,2.52,\
                3.23,3.16",
            "E0,17/08/13,Swansea,Man United,1,4,A,0,2,A,P Dowd,17,15,6,7,13,10,7,4,1,3,0,0,4.2,3.5,\
                2,4.1,3.5,1.87,4.2,3.5,1.8,4,3.5,1.9,4.1,3.52,2.03,4,3.4,1.91,4,3.6,1.9,3.8,\
                3.6,2.05,33,4.33,3.84,3.6,3.4,2.1,1.99,31,1.93,1.83,2.06,1.98,,,,,,,3.62,3.41,2.22",
            "E0,17/08/13,West Brom,Southampton,0,1,A,0,0,D,K Friend,11,7,1,2,14,24,4,8,4,0,0,0,2.4,\
                3.4,3.2,2.3,3.25,3.1,2.2,3.3,3.1,2.2,3.3,3.25,2.42,3.38,3.21,2.25,3.3,\
                3.2,2.25,3.4,3.2,2.4,3.4,3.12,33,2.43,2.31,3.45,3.33,3.3,3.07,31,2.02,1.87,\
                2.02,1.93,,,,,,,2.56,3.3,3.06",
            "E0,17/08/13,West Ham,Cardiff,2,0,H,1,0,H,H Webb,18,12,4,1,10,7,4,3,0,1,0,0,2,3.6,4,\
                1.91,3.4,4,1.9,3.45,3.8,1.9,3.4,4,1.99,3.57,4.21,1.95,3.5,3.8,1.91,3.5,4,2,3.6,\
                4,33,2,1.94,3.65,3.47,4.4,3.94,31,2,1.89,2,1.9,,,,,,,2.17,3.36,3.84",
            "E0,18/08/13,Chelsea,Hull,2,0,H,2,0,H,J Moss,22,7,5,2,7,16,5,1,0,1,0,0,1.2,7,21,1.19,\
                6.25,15,1.2,5.8,13,1.2,6.5,15,1.2,7.4,19.5,1.2,6,15,1.2,6.5,15,1.22,6.5,15,32,\
                1.22,1.19,7.7,6.68,21,15.53,22,1.59,1.53,2.63,2.45,,,,,,,1.23,6.8,16.25",
            "E0,18/08/13,Crystal Palace,Tottenham,0,1,A,0,0,D,M Clattenburg,5,17,3,2,6,9,3,7,1,0,\
                0,0,4.75,3.75,1.83,4.75,3.7,1.72,4.2,3.5,1.8,4.5,3.5,1.8,5.16,3.69,1.8,4.75,\
                3.6,1.75,4.8,3.8,1.73,4.5,3.75,1.85,33,5.2,4.66,3.95,3.61,1.85,1.77,31,1.89,\
                1.81,2.12,1.99,,,,,,,6.73,3.88,1.63",
            "E0,19/08/13,Man City,Newcastle,4,0,H,2,0,H,A Marriner,20,5,11,1,9,7,8,1,2,3,0,1,1.33,\
                5.5,11,1.3,5.25,9.75,1.3,5,9,1.28,5.5,10,1.31,5.86,11.66,1.29,5.5,10,1.3,5.25,10,\
                1.33,5.5,10.5,30,1.33,1.3,5.86,5.27,12,10.35,27,1.68,1.6,2.44,2.3,,,,\
                ,,,1.26,6.41,14.02",
            "E0,21/08/13,Chelsea,Aston Villa,2,1,H,1,1,D,K Friend,15,7,3,3,12,13,1,2,1,4,0,0,1.29,\
                5.5,10,1.28,5.25,10,1.3,5,9,1.29,5.5,9.5,1.3,5.89,11.9,1.3,5.5,9.5,1.3,5.25,\
                10,1.29,5.75,12,34,1.33,1.29,6.4,5.42,14,10.17,22,1.62,1.55,2.56,2.38,21,-1.5,1.88,\
                1.84,2.14,2.03,1.28,6.48,12.18",
            "E0,24/08/13,Aston Villa,Liverpool,0,1,A,0,1,A,M Clattenburg,17,5,3,1,9,8,8,2,3,3,0,0,\
                4,3.75,1.95,3.8,3.6,1.91,3.6,3.3,2,4,3.5,1.91,4.18,3.83,1.93,3.75,3.6,1.95,4,3.5,\
                1.91,3.9,3.8,1.95,34,4.2,3.93,3.83,3.55,2,1.91,32,1.83,1.73,2.22,2.1,18,0.5,2.01,\
                1.96,1.96,1.92,4.28,4.04,1.85",
            "E0,24/08/13,Everton,West Brom,0,0,D,0,0,D,R East,22,7,8,2,14,15,11,1,1,1,0,0,1.57,\
                4.33,6.5,1.53,4.1,6,1.55,3.9,5.6,1.57,3.75,6,1.55,4.33,7.01,1.55,4,6,1.57,4,6,\
                1.6,4.2,6,34,1.6,1.55,4.35,3.94,7.1,6.27,32,1.89,1.79,2.1,2.02,21,-1,2.05,1.92,\
                2,1.94,1.58,4.13,6.88",
            "E0,24/08/13,Fulham,Arsenal,1,3,A,0,2,A,H Webb,16,18,7,7,10,8,1,8,2,2,0,0,3.8,3.5,\
                2.1,3.6,3.5,2,3.3,3.3,2.1,3.5,3.4,2.1,3.7,3.58,2.13,3.4,3.5,2.1,3.5,3.4,2.1,3.5,\
                3.5,2.15,34,3.8,3.51,3.58,3.38,2.15,2.1,32,1.88,1.8,2.12,2,18,0.5,1.82,1.79,\
                2.15,2.09,4.78,3.6,1.88"
        ].iter().map(|r| {
            let record: Vec<&str> = r.split(",").collect();
            StringRecord::from(record)
        }).collect();
        let c_res = vec_to_self(&m_list);
        assert!(c_res.is_ok());
        let championship = c_res.expect("Championship is supposed to be OK.");
        assert_eq!(championship.teams().len(), 20);
        assert_eq!(championship.matches().len(), 14);
        test_match_ordering(&championship);

        println!("\tEmpty list of matches");
        let m_list: VSR = vec![];
        let c_res = vec_to_self(&m_list);
        assert!(c_res.is_ok());
        let championship = c_res.expect("Championship is supposed to be OK.");
        assert_eq!(championship.teams().len(), 0);
        assert_eq!(championship.matches().len(), 0);
        test_match_ordering(&championship);
        assert_eq!(championship.start_date(), get_invalid_match_date());
        assert_eq!(championship.end_date(), get_invalid_match_date());

        println!("\tWith an error in a match");
        let m_list: VSR = vec![
            "E0,17/08/13,Arsenal,Aston Villa,1,Jean_Claude,A,1,1,D,A Taylor,16,9,4,4,15,18,4,3,4,5,1,0,1.44,\
                4.75,8,1.36,5,7.75,1.37,4.6,7.5,1.4,4.5,7.5,1.41,5.2,8.3,1.36,4.8,8.5,1.4,4.5,7.5,\
                1.44,4.8,7.5,33,1.44,1.4,5.2,4.68,9.7,7.87,22,1.62,1.56,2.56,2.37,,,,,,,1.\
                44,5,8.05",
            "E0,17/08/13,Liverpool,Stoke,1,0,H",
            "E0,17/08/13,Norwich,Everton,2,2,D,0,0,D,M Oliver,8,19,2,6,13,10,6,8,2,0,0,0,3.2,3.4,\
                2.4,3.1,3.25,2.3,2.9,3.3,2.3,3,3.3,2.3,3.32,3.41,2.35,3.1,3.3,2.3,3,3.4,2.3,3.12,\
                3.4,2.4,33,3.35,3.12,3.41,3.27,2.4,2.31,31,2.08,1.98,1.91,1.82,,,,,,,3.81,\
                3.27,2.21",
            "E0,17/08/13,Sunderland,Fulham,0,1,A,0,0,D,N Swarbrick,20,5,3,1,14,14,6,1,0,3,0,0,2.3,\
                3.4,3.4,2.25,3.2,3.25,2.2,3.2,3.2,2.2,3.25,3.3,2.25,3.37,3.58,2.2,3.3,3.3,2.25,3.3,\
                3.3,2.3,3.4,3.3,33,2.3,2.22,3.45,3.3,3.58,3.29,31,2.14,2.04,1.84,1.77,,,,,,,2.52,\
                3.23,3.16",
            "E0,17/08/13,Swansea,Man United,1,4,A,0,2,A,P Dowd,17,15,6,7,13,10,7,4,1,3,0,0,4.2,3.5,\
                2,4.1,3.5,1.87,4.2,3.5,1.8,4,3.5,1.9,4.1,3.52,2.03,4,3.4,1.91,4,3.6,1.9,3.8,\
                3.6,2.05,33,4.33,3.84,3.6,3.4,2.1,1.99,31,1.93,1.83,2.06,1.98,,,,,,,3.62,3.41,2.22",
            "E0,17/08/13,West Brom,Southampton,0,1,A,0,0,D,K Friend,11,7,1,2,14,24,4,8,4,0,0,0,2.4,\
                3.4,3.2,2.3,3.25,3.1,2.2,3.3,3.1,2.2,3.3,3.25,2.42,3.38,3.21,2.25,3.3,\
                3.2,2.25,3.4,3.2,2.4,3.4,3.12,33,2.43,2.31,3.45,3.33,3.3,3.07,31,2.02,1.87,\
                2.02,1.93,,,,,,,2.56,3.3,3.06"
        ].iter().map(|r| {
            let record: Vec<&str> = r.split(",").collect();
            StringRecord::from(record)
        }).collect();
        let c_res = vec_to_self(&m_list);
        assert!(c_res.is_err());
        let err_msg = c_res.expect_err("Error if errors in records.");
        assert!(
            err_msg.starts_with("The following things happened while parsing: "),
            "The error message should begin with 'The following things happened while parsing: '. \
                Got this instead: '{}'.", err_msg
        );
    }
}
