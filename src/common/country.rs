use std::convert::TryFrom;
use std::fmt;

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum Country { England, Spain, Germany, Italy, France }

impl TryFrom<&String> for Country {
    type Error = String;

    fn try_from(country: &String) -> Result<Self, Self::Error> {
        match country.as_str() {
            "en" => Ok(Country::England),
            "es" => Ok(Country::Spain),
            "de" => Ok(Country::Germany),
            "it" => Ok(Country::Italy),
            "fr" => Ok(Country::France),
            _ => Err(format!("{} is not a supported country", country))
        }
    }
}

impl fmt::Display for Country {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        write!(f, "{}", match self {
            Country::England => "england",
            Country::Spain => "spain",
            Country::Germany => "germany",
            Country::Italy => "italy",
            Country::France => "france",
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn test_a_country(c_str: &str, expected_c: Country) {
        let c_str = c_str.to_string();
        let c_res = Country::try_from(&c_str);
        assert!(c_res.is_ok(), format!("{} is supposed to be a valid country string", c_str));
        assert_eq!(c_res.unwrap(), expected_c);
    }

    #[test]
    fn try_from_country() {
        // Valid country codes
        println!("\n\tValid country codes");

        test_a_country("en", Country::England);
        test_a_country("es", Country::Spain);
        test_a_country("de", Country::Germany);
        test_a_country("it", Country::Italy);
        test_a_country("fr", Country::France);

        // Invalid country code
        println!("\tInvalid country code");

        let c_str = "foobar".to_string();
        let c_res = Country::try_from(&c_str);
        assert!(c_res.is_err());
        assert_eq!(c_res.unwrap_err(), "foobar is not a supported country".to_string());
    }

    #[test]
    fn to_string() {
        println!();
        assert_eq!(Country::England.to_string(), "england");
        assert_eq!(Country::Spain.to_string(), "spain");
        assert_eq!(Country::Germany.to_string(), "germany");
        assert_eq!(Country::Italy.to_string(), "italy");
        assert_eq!(Country::France.to_string(), "france");
    }
}
