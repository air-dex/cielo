use chrono::naive::NaiveDate;
use crate::common::{CieloResult, Team};
use crate::data::traits::ImportValue;
use csv::StringRecord;
use serde_json::{Map, Value, Number};
use std::fmt::{Display, Formatter, Error};

#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct Match {
    home: TeamMatch,
    away: TeamMatch,
    date: NaiveDate,
}

impl Match {
    pub fn date(&self) -> NaiveDate {
        self.date
    }

    pub fn home_team(&self) -> Team {
        self.home.team.clone()
    }

    pub fn home_team_name(&self) -> &str {
        self.home.team_name()
    }

    pub fn set_home_team(&mut self, team: Team) {
        self.home.set_team(team)
    }

    pub fn away_team(&self) -> Team {
        self.away.team.clone()
    }

    pub fn away_team_name(&self) -> &str {
        self.away.team_name()
    }

    pub fn set_away_team(&mut self, team: Team) {
        self.away.set_team(team)
    }

    pub fn home(&self) -> &TeamMatch {
        &self.home
    }

    pub fn away(&self) -> &TeamMatch {
        &self.away
    }

    pub fn title(&self) -> String {
        format!("{ht} - {at}", ht = self.home_team_name(), at = self.away_team_name())
    }

    pub fn score(&self, with_teams: bool) -> String {
        if with_teams {
            format!(
                "{ht} {hg} - {ag} {at}",
                ht = self.home_team_name(), hg = self.home().ft_goals(),
                at = self.away_team_name(), ag = self.away().ft_goals(),
            )
        }
        else {
            format!("{hg} - {ag}", hg = self.home().ft_goals(), ag = self.away().ft_goals())
        }
    }

    pub fn match_win(&self) -> MatchWin {
        let hg = self.home().ft_goals();
        let ag = self.away().ft_goals();

        if hg > ag { MatchWin::HomeWin }
        else if hg == ag { MatchWin::Draw }
        else { MatchWin::AwayWin }
    }

    pub fn goal_difference(&self) -> u16 {
        let hg = self.home().ft_goals();
        let ag = self.away().ft_goals();

        if hg > ag { hg - ag }
        else if hg == ag { 0 }
        else { ag - hg }
    }
}

pub fn are_matches_sorted(matches: &Vec<Match>) -> bool {
    let dates: Vec<NaiveDate> = matches.iter().map(|m| { m.date() }).collect();
    let mut dit = dates.iter().peekable();

    while let Some(d) = dit.next() {
        if let Some(nd) = dit.peek() {
            if *nd < d {
                return false
            }
        }
    }

    true
}

#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct TeamMatch {
    team: Team,
    goals: u16,
    ht_goals: u16,
    yellow_cards: u16,
    red_cards: u16,
    fouls: u16
}

impl TeamMatch {
    pub fn new(team: Team) -> Self {
        Self {
            team,
            goals: 0,
            ht_goals: 0,
            yellow_cards: 0,
            red_cards: 0,
            fouls: 0
        }
    }

    pub fn team_name(&self) -> &str {
        self.team.name()
    }

    pub fn set_team(&mut self, team: Team) {
        self.team = team;
    }

    pub fn ft_goals(&self) -> u16 {
        self.goals
    }

    pub fn ht_goals(&self) -> u16 {
        self.ht_goals
    }

    pub fn yellow_cards(&self) -> u16 {
        self.yellow_cards
    }

    pub fn red_cards(&self) -> u16 {
        self.red_cards
    }

    pub fn fouls(&self) -> u16 {
        self.fouls
    }
}

impl ImportValue<Value> for Match {
    fn value_to_self(v: &Value) -> CieloResult<Self> {
        if !v.is_object() {
            return Err("JSON value for a match should be a JSON object.".to_string())
        }

        let obj = v.as_object()
            .expect("JSON value for a match is supposed to be an object");

        const DATE: &'static str = "Date";

        let date = match obj.get(DATE) {
            Some(dv) => if let Value::String(d) = dv {
                match build_match_date(d) {
                    Ok(md) => md,
                    Err(e) => return Err(e)
                }
            } else {
                return Err(wrong_type_err(DATE, "string"))
            },
            None => return Err(no_field_err(DATE, "the match date"))
        };

        const HOME_TEAM: &'static str = "HomeTeam";

        let home_team = match obj.get(HOME_TEAM) {
            Some(htv) => if let Value::String(s) = htv {
                s.clone()
            } else {
                return Err(wrong_type_err(HOME_TEAM, "string"))
            },
            None => return Err(no_field_err(HOME_TEAM, "the home team"))
        };

        const AWAY_TEAM: &'static str = "AwayTeam";

        let away_team = match obj.get(AWAY_TEAM) {
            Some(atv) => if let Value::String(s) = atv {
                s.clone()
            } else {
                return Err(wrong_type_err(AWAY_TEAM, "string"))
            },
            None => return Err(no_field_err(AWAY_TEAM, "the away team"))
        };

        let home_goals = match get_json_number(obj, "FTHG", "full time home goals") {
            Ok(r) => r,
            Err(e) => return Err(e)
        };

        let away_goals = match get_json_number(obj, "FTAG", "full time away goals") {
            Ok(r) => r,
            Err(e) => return Err(e)
        };

        let ht_home_goals = match get_json_number(obj, "HTHG", "half time home goals") {
            Ok(r) => r,
            Err(e) => return Err(e)
        };

        let ht_away_goals = match get_json_number(obj, "HTAG", "half time away goals") {
            Ok(r) => r,
            Err(e) => return Err(e)
        };

        if ht_home_goals > home_goals {
            return Err("There should be more goals at the end of the match than at its half-time for the home team.".to_string())
        }

        if ht_away_goals > away_goals {
            return Err("There should be more goals at the end of the match than at its half-time for the away team.".to_string())
        }

        let home_fouls = match get_json_number(obj, "HF", "fouls committed by the home team") {
            Ok(r) => r,
            Err(e) => return Err(e)
        };

        let away_fouls = match get_json_number(obj, "AF", "fouls committed by the away team") {
            Ok(r) => r,
            Err(e) => return Err(e)
        };

        let home_yellow = match get_json_number(obj, "HY", "yellow cards received by the home team") {
            Ok(r) => r,
            Err(e) => return Err(e)
        };

        let away_yellow = match get_json_number(obj, "AY", "yellow cards received by the away team") {
            Ok(r) => r,
            Err(e) => return Err(e)
        };

        let home_red = match get_json_number(obj, "HR", "red cards received by the home team") {
            Ok(r) => r,
            Err(e) => return Err(e)
        };

        let away_red = match get_json_number(obj, "AR", "red cards received by the away team") {
            Ok(r) => r,
            Err(e) => return Err(e)
        };

        Ok(Match {
            date,
            home: TeamMatch {
                team: Team::new(home_team),
                goals: home_goals,
                ht_goals: ht_home_goals,
                yellow_cards: home_yellow,
                red_cards: home_red,
                fouls: home_fouls
            },
            away: TeamMatch {
                team: Team::new(away_team),
                goals: away_goals,
                ht_goals: ht_away_goals,
                yellow_cards: away_yellow,
                red_cards: away_red,
                fouls: away_fouls
            }
        })
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum MatchWin {
    HomeWin,
    Draw,
    AwayWin
}

impl MatchWin {
    pub fn get_char(&self) -> char {
        match self {
            MatchWin::HomeWin => '1',
            MatchWin::Draw => 'N',
            MatchWin::AwayWin => '2'
        }
    }
}

impl Display for MatchWin {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        write!(f, "{}", self.get_char())
    }
}

pub fn get_invalid_match_date() -> NaiveDate {
    NaiveDate::from_ymd_opt(1992, 5, 5).expect("No matches on May, 5th.")
}

#[inline]
fn get_json_number(obj: &Map<String, Value>, field: &str, desc: &str) -> CieloResult<u16> {
    match obj.get(field) {
        Some(afv) => if let Value::Number(n) = afv {
            match n.as_u64() {
                Some(i) => Ok(i as u16),
                None => Err(unsigned_value_err(n, field))
            }
        } else {
            Err(wrong_type_err(field, "number (integer)"))
        },
        None => Err(no_field_err(field, desc))
    }
}

#[inline]
fn no_field_err(field: &str, desc: &str) -> String {
    format!("No '{}' field for {}.", field, desc)
}

#[inline]
fn wrong_type_err(field: &str, typ: &str) -> String {
    format!("The '{}' field is supposed to be a {}.", field, typ)
}

#[inline]
fn unsigned_value_err(value: &Number, field: &str) -> String {
    format!("Value '{}' for field {} is supposed to be positive.", value.to_string(), field)
}

impl ImportValue<StringRecord> for Match {
    fn value_to_self(record: &StringRecord) -> CieloResult<Self> {
        let mut it = record.iter();
        let premature_csv_end: CieloResult<Match> = Err(PREMATURE_MSG_END.to_string());

        // 1st column: don't care.
        if let None = it.next() {
            return premature_csv_end.clone()
        }

        // 2nd column: date.
        let date = match it.next() {
            Some(r) => match build_match_date(r) {
                Ok(md) => md,
                Err(e) => return Err(e)
            },
            None => return premature_csv_end.clone()
        };

        // 3rd column: home team.
        let home_team: String = match it.next() {
            Some(s) => s.to_string(),
            None => return premature_csv_end.clone()
        };

        // 4th column: away team.
        let away_team: String = match it.next() {
            Some(s) => s.to_string(),
            None => return premature_csv_end.clone()
        };

        // 5th column: full time home goals.
        let home_goals = match get_csv_number(&it.next()) {
            Ok(n) => n,
            Err(e) => return Err(e)
        };

        // 6th column: full time away goals.
        let away_goals = match get_csv_number(&it.next()) {
            Ok(n) => n,
            Err(e) => return Err(e)
        };

        // 7th column: don't care.
        if let None = it.next() {
            return premature_csv_end.clone()
        }

        // 8th column: half time home goals.
        let ht_home_goals = match get_csv_number(&it.next()) {
            Ok(n) => n,
            Err(e) => return Err(e)
        };

        // 9th column: away time home goals.
        let ht_away_goals = match get_csv_number(&it.next()) {
            Ok(n) => n,
            Err(e) => return Err(e)
        };

        if ht_home_goals > home_goals {
            return Err("There should be more goals at the end of the match than at its half-time for the home team.".to_string())
        }

        if ht_away_goals > away_goals {
            return Err("There should be more goals at the end of the match than at its half-time for the away team.".to_string())
        }

        // 10th from 14th column (unrolled): don't care.
        if let None = it.next() {
            return premature_csv_end.clone()
        }
        if let None = it.next() {
            return premature_csv_end.clone()
        }
        if let None = it.next() {
            return premature_csv_end.clone()
        }
        if let None = it.next() {
            return premature_csv_end.clone()
        }
        if let None = it.next() {
            return premature_csv_end.clone()
        }

        // 15th column: home fouls.
        let home_fouls = match get_csv_number(&it.next()) {
            Ok(n) => n,
            Err(e) => return Err(e)
        };

        // 16th column: away fouls.
        let away_fouls = match get_csv_number(&it.next()) {
            Ok(n) => n,
            Err(e) => return Err(e)
        };

        // 17th and 18th column: don't care.
        if let None = it.next() {
            return premature_csv_end.clone()
        }

        if let None = it.next() {
            return premature_csv_end.clone()
        }

        // 19th column: home yellow cards.
        let home_yellow = match get_csv_number(&it.next()) {
            Ok(n) => n,
            Err(e) => return Err(e)
        };

        // 20th column: away yellow cards.
        let away_yellow = match get_csv_number(&it.next()) {
            Ok(n) => n,
            Err(e) => return Err(e)
        };

        // 21th column: home red cards.
        let home_red = match get_csv_number(&it.next()) {
            Ok(n) => n,
            Err(e) => return Err(e)
        };

        // 22th column: away red cards.
        let away_red = match get_csv_number(&it.next()) {
            Ok(n) => n,
            Err(e) => return Err(e)
        };

        // Rest of the record: don't care.
        it.for_each(drop);

        // Building match
        Ok(Match {
            date,
            home: TeamMatch {
                team: Team::new(home_team),
                goals: home_goals,
                ht_goals: ht_home_goals,
                yellow_cards: home_yellow,
                red_cards: home_red,
                fouls: home_fouls
            },
            away: TeamMatch {
                team: Team::new(away_team),
                goals: away_goals,
                ht_goals: ht_away_goals,
                yellow_cards: away_yellow,
                red_cards: away_red,
                fouls: away_fouls
            }
        })
    }
}

static PREMATURE_MSG_END: &'static str = "Record should not end there.";

fn build_match_date(d: &str) -> CieloResult<NaiveDate> {
    let mut res: CieloResult<NaiveDate> = build_match_date_internals(d, "%d/%m/%y");

    if res.is_err() {
        res = build_match_date_internals(d, "%Y-%m-%d");
    }

    if res.is_err() {
        res = build_match_date_internals(d, "%d/%m/%Y");
    }

    res
}

fn build_match_date_internals(d: &str, pattern: &str) -> CieloResult<NaiveDate> {
    match NaiveDate::parse_from_str(d, pattern) {
        Ok(md) => Ok(md),
        Err(e) => Err(format!(
            "Error while parsing the date ('{d}'): {e}",
            d = d, e = e.to_string()
        ))
    }
}

fn get_csv_number(it_next: &Option<&str>) -> CieloResult<u16> {
    match it_next {
        Some(record) => match u16::from_str_radix(record, 10) {
            Ok(n) => Ok(n),
            Err(e) => Err(format!(
                "Cannot parse '{s}' into a positive integer: {e}",
                s = record,
                e = e.to_string()
            ))
        },
        None => return Err(PREMATURE_MSG_END.to_string())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    // TODO: additionnal tests for mew code: MatchWin

    #[test]
    #[ignore]
    fn test_match_sorting() {
        unimplemented!()
    }

    #[test]
    #[ignore]
    fn test_match_title() {
        unimplemented!()
    }

    #[test]
    #[ignore]
    fn test_match_score() {
        unimplemented!()
    }

    #[test]
    fn test_err_strings() {
        let n = Number::from(42_u64);
        assert_eq!(no_field_err("country", "old men"), "No 'country' field for old men.");
        assert_eq!(wrong_type_err("country", "old men"), "The 'country' field is supposed to be a old men.");
        assert_eq!(
            unsigned_value_err(&n, "geek"),
            "Value '42' for field geek is supposed to be positive."
        );
    }

    #[test]
    fn test_a_date() {
        let june_14th_2019: NaiveDate = NaiveDate::from_ymd(2019, 6, 14);

        println!("\n\tNominal case");
        let choix: CieloResult<NaiveDate> = build_match_date("14/6/2019");
        assert!(choix.is_ok());
        assert_eq!(choix.expect("Parsing date is supposed to be OK"), june_14th_2019);
        let choix: CieloResult<NaiveDate> = build_match_date("2019-06-14");
        assert!(choix.is_ok());
        assert_eq!(choix.expect("Parsing date is supposed to be OK"), june_14th_2019);
        let choix: CieloResult<NaiveDate> = build_match_date("14/6/19");
        assert!(choix.is_ok());
        assert_eq!(choix.expect("Parsing date is supposed to be OK"), june_14th_2019);

        println!("\tDo not respect the date pattern");
        let choix: CieloResult<NaiveDate> = build_match_date("20190614");
        assert!(choix.is_err());
        assert!(choix.expect_err("Parsed date is supposed to be wrong.").starts_with("Error while parsing the date ('20190614'): "));
    }

    #[test]
    fn test_json_numbers() {
        let obj: Value = serde_json::json!({
            "answer": 42,
            "neg": -666,
            "pi": 3.14,
            "cevennes": "sept"
        });

        let obj: &Map<String, Value> = obj.as_object().expect("The JSON entity used for this test should be an object.");

        println!("\n\tNominal case");

        let num_res = get_json_number(&obj, "answer", "the geek famous");
        assert!(num_res.is_ok());
        assert_eq!(num_res.expect("The extracted number should be ok"), 42);

        println!("\tNo field for this name");

        let num_res = get_json_number(&obj, "cadence", "Zelda pulse");
        assert!(num_res.is_err());
        assert_eq!(num_res.expect_err("No number if no field"), "No 'cadence' field for Zelda pulse.".to_string());

        println!("\tNumber field with a string");

        let num_res = get_json_number(&obj, "cevennes", "homophony");
        assert!(num_res.is_err());
        assert_eq!(num_res.expect_err("No number if string field"), "The 'cevennes' field is supposed to be a number (integer).".to_string());

        println!("\tFloat number for this field.");

        let num_res = get_json_number(&obj, "pi", "the circle number");
        assert!(num_res.is_err());
        assert_eq!(num_res.expect_err("No number if string field"), "Value '3.14' for field pi is supposed to be positive.".to_string());

        println!("\tNegative integer for this field.");

        let num_res = get_json_number(&obj, "neg", "no hell");
        assert!(num_res.is_err());
        assert_eq!(num_res.expect_err("No number if string field"), "Value '-666' for field neg is supposed to be positive.".to_string());
    }

    #[inline]
    fn test_a_json_v2s_field(title: &str, field: &str, desc: &str, match_val: u16, exp_val: u16, nomval: &Value) {
        println!("\t{title} test", title = title);

        println!("\t\tNominal case");
        assert_eq!(match_val, exp_val);

        println!("\t\tNo field for this name");

        let mut value = nomval.clone();
        let value_ref: &mut Map<String, Value> = value.as_object_mut().expect("The value is supposed to be an object");
        value_ref.remove(field);

        let m_res = Match::value_to_self(&value);
        assert!(m_res.is_err());
        let exp_errmsg = format!("No '{f}' field for {d}.", f = field, d = desc);
        assert_eq!(m_res.expect_err("Parsing should turn wrong"), exp_errmsg);

        println!("\t\tNumber field with a string");

        let mut value = nomval.clone();
        let value_ref: &mut Map<String, Value> = value.as_object_mut().expect("The value is supposed to be an object");
        value_ref.insert(field.to_string(), Value::from("quarante-deux"));

        let m_res = Match::value_to_self(&value);
        assert!(m_res.is_err());
        let exp_errmsg = format!("The '{f}' field is supposed to be a number (integer).", f = field);
        assert_eq!(m_res.expect_err("Parsing should turn wrong"), exp_errmsg);

        println!("\t\tFloat number for this field.");

        let mut value = nomval.clone();
        let value_ref: &mut Map<String, Value> = value.as_object_mut().expect("The value is supposed to be an object");
        value_ref.insert(field.to_string(), Value::from(3.14));

        let m_res = Match::value_to_self(&value);
        assert!(m_res.is_err());
        let exp_errmsg = format!("Value '3.14' for field {f} is supposed to be positive.", f = field);
        assert_eq!(m_res.expect_err("Parsing should turn wrong"), exp_errmsg);

        println!("\t\tNegative integer for this field.");

        let mut value = nomval.clone();
        let value_ref: &mut Map<String, Value> = value.as_object_mut().expect("The value is supposed to be an object");
        value_ref.insert(field.to_string(), Value::from(-10));

        let m_res = Match::value_to_self(&value);
        assert!(m_res.is_err());
        let exp_errmsg = format!("Value '-10' for field {f} is supposed to be positive.", f = field);
        assert_eq!(m_res.expect_err("Parsing should turn wrong"), exp_errmsg);
    }

    #[test]
    fn json_v2s() {
        println!("\n\tPartial nominal case");
        let nomval: Value = serde_json::json!({
            "AC": 1, "AF": 20, "AR": 0, "AS": 5, "AST": 1, "AY": 3,
            "AwayTeam": "Toulouse", "Date": "10/08/2018", "FTAG": 0, "FTHG": 4,
            "FTR": "H", "HC": 5, "HF": 7, "HR": 0, "HS": 23, "HST": 10, "HTAG": 0,
            "HTHG": 1, "HY": 1, "HomeTeam": "Marseille"
        });

        let m_res_nomval = Match::value_to_self(&nomval);
        assert!(m_res_nomval.is_ok(), "Nominal case should provide a match instead of an error");
        let m_nomval = m_res_nomval.expect("Parsing should be ok");
        let m_nomref = &m_nomval;

        println!("\tJSON entity is not an object");

        let value = serde_json::json!([true]);
        let m_res = Match::value_to_self(&value);
        assert!(m_res.is_err());
        assert_eq!(
            m_res.expect_err("Parsing a list should be an error."),
            "JSON value for a match should be a JSON object.".to_string()
        );

        println!("\tDate test");

        println!("\t\tNominal case");

        assert_eq!(m_nomref.date(), NaiveDate::from_ymd(2018, 8, 10));

        println!("\t\tNo field for this name");

        let mut value = nomval.clone();
        let value_ref: &mut Map<String, Value> = value.as_object_mut().expect("The value is supposed to be an object");
        value_ref.remove(&"Date".to_string());

        let m_res = Match::value_to_self(&value);
        assert!(m_res.is_err());
        assert_eq!(
            m_res.expect_err("Parsing should turn wrong"),
            "No 'Date' field for the match date.".to_string()
        );

        println!("\t\tField is not a string");

        let mut value = nomval.clone();
        let value_ref: &mut Map<String, Value> = value.as_object_mut().expect("The value is supposed to be an object");
        value_ref.insert("Date".to_string(), Value::from(42));

        let m_res = Match::value_to_self(&value);
        assert!(m_res.is_err());
        assert_eq!(
            m_res.expect_err("Parsing should turn wrong"),
            "The 'Date' field is supposed to be a string.".to_string()
        );

        println!("\t\tDo not respect the date pattern");

        let mut value = nomval.clone();
        let value_ref: &mut Map<String, Value> = value.as_object_mut().expect("The value is supposed to be an object");
        value_ref.insert("Date".to_string(), Value::from("20111102"));

        let m_res = Match::value_to_self(&value);
        assert!(m_res.is_err());
        let err_msg = m_res.expect_err("Parsing should turn wrong");
        assert!(
            err_msg.starts_with("Error while parsing the date ('20111102'): "),
            "Error message should start with 'Error while parsing the date ('20111102'): '. Got this instead: {}", err_msg
        );

        println!("\tHome team test");

        println!("\t\tNominal case");
        assert_eq!(m_nomref.home_team(), Team::new("Marseille".to_string()));

        println!("\t\tNo field for this name");

        let mut value = nomval.clone();
        let value_ref: &mut Map<String, Value> = value.as_object_mut().expect("The value is supposed to be an object");
        value_ref.remove(&"HomeTeam".to_string());

        let m_res = Match::value_to_self(&value);
        assert!(m_res.is_err());
        assert_eq!(
            m_res.expect_err("Parsing should turn wrong"),
            "No 'HomeTeam' field for the home team.".to_string()
        );

        println!("\t\tField is not a string");

        let mut value = nomval.clone();
        let value_ref: &mut Map<String, Value> = value.as_object_mut().expect("The value is supposed to be an object");
        value_ref.insert("HomeTeam".to_string(), Value::from(42));

        let m_res = Match::value_to_self(&value);
        assert!(m_res.is_err());
        assert_eq!(
            m_res.expect_err("Parsing should turn wrong"),
            "The 'HomeTeam' field is supposed to be a string.".to_string()
        );

        println!("\tAway team test");

        println!("\t\tNominal case");
        assert_eq!(m_nomref.away_team(), Team::new("Toulouse".to_string()));

        println!("\t\tNo field for this name");

        let mut value = nomval.clone();
        let value_ref: &mut Map<String, Value> = value.as_object_mut().expect("The value is supposed to be an object");
        value_ref.remove(&"AwayTeam".to_string());

        let m_res = Match::value_to_self(&value);
        assert!(m_res.is_err());
        assert_eq!(
            m_res.expect_err("Parsing should turn wrong"),
            "No 'AwayTeam' field for the away team.".to_string()
        );

        println!("\t\tField is not a string");

        let mut value = nomval.clone();
        let value_ref: &mut Map<String, Value> = value.as_object_mut().expect("The value is supposed to be an object");
        value_ref.insert("AwayTeam".to_string(), Value::from(42));

        let m_res = Match::value_to_self(&value);
        assert!(m_res.is_err());
        assert_eq!(
            m_res.expect_err("Parsing should turn wrong"),
            "The 'AwayTeam' field is supposed to be a string.".to_string()
        );

        // Numeric fields

        test_a_json_v2s_field(
            "Full time home goals",
            "FTHG", "full time home goals",
            m_nomref.home().ft_goals(), 4,
            &nomval
        );

        test_a_json_v2s_field(
            "Full time away goals",
            "FTAG", "full time away goals",
            m_nomref.away().ft_goals(), 0,
            &nomval
        );

        test_a_json_v2s_field(
            "Half time home goals",
            "HTHG", "half time home goals",
            m_nomref.home().ht_goals(), 1,
            &nomval
        );

        test_a_json_v2s_field(
            "Half time away goals",
            "HTAG", "half time away goals",
            m_nomref.away().ht_goals(), 0,
            &nomval
        );

        test_a_json_v2s_field(
            "Home fouls",
            "HF", "fouls committed by the home team",
            m_nomref.home().fouls(), 7,
            &nomval
        );

        test_a_json_v2s_field(
            "Away fouls",
            "AF", "fouls committed by the away team",
            m_nomref.away().fouls(), 20,
            &nomval
        );

        test_a_json_v2s_field(
            "Yellow home cards",
            "HY", "yellow cards received by the home team",
            m_nomref.home().yellow_cards(), 1,
            &nomval
        );

        test_a_json_v2s_field(
            "Yellow away cards",
            "AY", "yellow cards received by the away team",
            m_nomref.away().yellow_cards(), 3,
            &nomval
        );

        test_a_json_v2s_field(
            "Red home cards",
            "HR", "red cards received by the home team",
            m_nomref.home().red_cards(), 0,
            &nomval
        );

        test_a_json_v2s_field(
            "Red away cards",
            "AR", "red cards received by the away team",
            m_nomref.away().red_cards(), 0,
            &nomval
        );

        println!("\tMore goals at the end of the match");

        println!("\t\tHome team");

        println!("\t\t\tNominal case");
        assert!(m_nomref.home().ft_goals() >= m_nomref.home().ht_goals());

        println!("\t\t\tWith a wrong value");
        let mut value = nomval.clone();
        let value_ref: &mut Map<String, Value> = value.as_object_mut().expect("The value is supposed to be an object");
        value_ref.insert("HTHG".to_string(), Value::from(5));

        let m_res = Match::value_to_self(&value);
        assert!(m_res.is_err());
        assert_eq!(
            m_res.expect_err("The match is supposed to be an error."),
            "There should be more goals at the end of the match than at its half-time for the home team.".to_string()
        );

        println!("\t\tAway team");

        println!("\t\t\tNominal case");
        assert!(m_nomref.away().ft_goals() >= m_nomref.away().ht_goals());

        println!("\t\t\tWith a wrong value");
        let mut value = nomval.clone();
        let value_ref: &mut Map<String, Value> = value.as_object_mut().expect("The value is supposed to be an object");
        value_ref.insert("HTAG".to_string(), Value::from(2));

        let m_res = Match::value_to_self(&value);
        assert!(m_res.is_err());
        assert_eq!(
            m_res.expect_err("The match is supposed to be an error."),
            "There should be more goals at the end of the match than at its half-time for the away team.".to_string()
        );
    }

    #[test]
    fn test_csv_numbers() {
        println!("\n\tNominal case");
        let p_res = get_csv_number(&Some("25"));
        assert!(p_res.is_ok());
        assert_eq!(p_res.expect("The parsing is OK"), 25);

        println!("\tParse error");

        println!("\t\tNegative");
        let p_res = get_csv_number(&Some("-25"));
        assert!(p_res.is_err());
        assert!(p_res.expect_err("The parsing turned wrong").starts_with("Cannot parse '-25' into a positive integer: "));

        println!("\t\tToo big");
        let p_res = get_csv_number(&Some("1234567890"));
        assert!(p_res.is_err());
        assert!(p_res.expect_err("The parsing turned wrong").starts_with("Cannot parse '1234567890' into a positive integer: "));

        println!("\t\tFloat");
        let p_res = get_csv_number(&Some("3.14"));
        assert!(p_res.is_err());
        assert!(p_res.expect_err("The parsing turned wrong").starts_with("Cannot parse '3.14' into a positive integer: "));

        println!("\tPremature end");
        let p_res = get_csv_number(&None);
        assert!(p_res.is_err());
        assert_eq!(p_res.expect_err("The parsing turned wrong"), "Record should not end there.".to_string());
    }

    fn csv_premature_end(legend: &str, vec_rec: &Vec<&str>) {
        println!("\t{}", legend);
        let sr = StringRecord::from(vec_rec.clone());
        let m_res = Match::value_to_self(&sr);
        assert!(m_res.is_err());
        assert_eq!(m_res.expect_err("Premature end is an error"), "Record should not end there.".to_string());
    }

    #[inline]
    // rec_col: column number in the record
    fn test_a_csv_v2s_field(legend: &str, rec_col: usize, nom_vecrec: &Vec<&str>, match_value: u16, exp_value: u16) {
        println!("\t{}", legend);

        println!("\t\tNominal case");
        assert_eq!(match_value, exp_value);

        println!("\t\tParse error");

        println!("\t\t\tNegative");
        let mut vecrec: Vec<&str> = nom_vecrec.clone();
        vecrec[rec_col-1] = "-25";
        let sr: StringRecord = StringRecord::from(vecrec);
        let m_res = Match::value_to_self(&sr);
        assert!(m_res.is_err());
        assert!(m_res.expect_err("The parsing turned wrong").starts_with("Cannot parse '-25' into a positive integer: "));

        println!("\t\t\tToo big");
        let mut vecrec: Vec<&str> = nom_vecrec.clone();
        vecrec[rec_col-1] = "1234567890";
        let sr: StringRecord = StringRecord::from(vecrec);
        let m_res = Match::value_to_self(&sr);
        assert!(m_res.is_err());
        assert!(m_res.expect_err("The parsing turned wrong").starts_with("Cannot parse '1234567890' into a positive integer: "));

        println!("\t\t\tFloat");
        let mut vecrec: Vec<&str> = nom_vecrec.clone();
        vecrec[rec_col-1] = "3.14";
        let sr: StringRecord = StringRecord::from(vecrec);
        let m_res = Match::value_to_self(&sr);
        assert!(m_res.is_err());
        assert!(m_res.expect_err("The parsing turned wrong").starts_with("Cannot parse '3.14' into a positive integer: "));

        csv_premature_end("\tPremature end", &nom_vecrec[..rec_col-1].to_vec());
    }

    #[test]
    fn csv_v2s() {
        println!("\n\tPartial nominal case");
        let nomvec: Vec<&str> = "D1,\
            21/08/10,FC Koln,Kaiserslautern,1,3,\
            A,\
            1,0,\
            H,10,17,4,6,\
            10,25,\
            5,6,\
            1,2,1,0,\
            2.1,3.4,3.4,2.15,3.3,3.2,2.15,3.3,3.25,2.1,3.3,3.1,2.1,3.4,3.4,2.1,3.25,3.2,2.2,3.2,\
            3.3,2.2,3.2,3.4,2.25,3.12,3.25,2.1,3.3,3.3,33,2.27,2.16,3.45,3.29,3.56,3.3,29,2.16,\
            2.04,1.8,1.73,21,0,1.63,1.54,2.52,2.39".split(',').collect();
        let nomval = StringRecord::from(nomvec.clone());
        let m_res_nomval = Match::value_to_self(&nomval);
        assert!(m_res_nomval.is_ok());
        let m_nomval: Match = m_res_nomval.expect("Nominal match is supposed to be OK.");
        let m_nomref: &Match = &m_nomval;

        csv_premature_end("Empty record (and 'division premature end' too)", &Vec::<&str>::new());

        println!("\tDate test");

        println!("\t\tNominal case");
        assert_eq!(m_nomref.date(), NaiveDate::from_ymd(2010, 8, 21));

        println!("\t\tDo not respect the date pattern");
        let mut vecrec: Vec<&str> = nomvec[0..2].to_vec();
        vecrec[1] = "111216";
        let m_res = Match::value_to_self(&StringRecord::from(vecrec));
        assert!(m_res.is_err());
        let err_msg = m_res.expect_err("There should be an error in the date pattern.");
        assert!(
            err_msg.starts_with("Error while parsing the date ('111216'): "),
            "The error message should start with 'Error while parsing the date ('111216'): '. Got this instead: '{}'.", err_msg
        );

        csv_premature_end("\tPremature end", &nomvec[0..1].to_vec());

        println!("\tHome team test");

        println!("\t\tNominal case");
        assert_eq!(m_nomref.home_team(), Team::new("FC Koln".to_string()));
        csv_premature_end("\tPremature end", &nomvec[0..2].to_vec());

        println!("\tAway team test");

        println!("\t\tNominal case");
        assert_eq!(m_nomref.away_team(), Team::new("Kaiserslautern".to_string()));

        csv_premature_end("\tPremature end", &nomvec[0..3].to_vec());

        test_a_csv_v2s_field(
            "Full time home goals",
            5, &nomvec,
            m_nomref.home().ft_goals(), 1
        );

        test_a_csv_v2s_field(
            "Full time away goals",
            6, &nomvec,
            m_nomref.away().ft_goals(), 3
        );

        csv_premature_end("No seventh column", &nomvec[0..6].to_vec());

        test_a_csv_v2s_field(
            "Half time home goals",
            8, &nomvec,
            m_nomref.home().ht_goals(), 1
        );

        test_a_csv_v2s_field(
            "Half time away goals",
            9, &nomvec,
            m_nomref.away().ht_goals(), 0
        );

        csv_premature_end("No tenth column", &nomvec[0..9].to_vec());
        csv_premature_end("No eleventh column", &nomvec[0..10].to_vec());
        csv_premature_end("No twelfth column", &nomvec[0..11].to_vec());
        csv_premature_end("No thirteenth column", &nomvec[0..12].to_vec());
        csv_premature_end("No fourteenth column", &nomvec[0..13].to_vec());

        test_a_csv_v2s_field(
            "Home fouls",
            15, &nomvec,
            m_nomref.home().fouls(), 10
        );

        test_a_csv_v2s_field(
            "Away fouls",
            16, &nomvec,
            m_nomref.away().fouls(), 25
        );

        csv_premature_end("No seventeenth column", &nomvec[0..16].to_vec());
        csv_premature_end("No eighteenth column", &nomvec[0..17].to_vec());

        test_a_csv_v2s_field(
            "Home yellow cards",
            19, &nomvec,
            m_nomref.home().yellow_cards(), 1
        );

        test_a_csv_v2s_field(
            "Away yellow cards",
            20, &nomvec,
            m_nomref.away().yellow_cards(), 2
        );

        test_a_csv_v2s_field(
            "Home red cards",
            21, &nomvec,
            m_nomref.home().red_cards(), 1
        );

        test_a_csv_v2s_field(
            "Away red cards",
            22, &nomvec,
            m_nomref.away().red_cards(), 0
        );

        println!("\tMore goals at the end of the match");

        println!("\t\tHome team");

        println!("\t\t\tNominal case");
        assert!(m_nomref.home().ft_goals() >= m_nomref.home().ht_goals());

        println!("\t\t\tWith a wrong value");
        let mut vecrec = nomvec.clone();
        vecrec[7] = "5";
        let m_res = Match::value_to_self(&StringRecord::from(vecrec));
        assert!(m_res.is_err());
        assert_eq!(
            m_res.expect_err("The match is supposed to be an error."),
            "There should be more goals at the end of the match than at its half-time for the home team.".to_string()
        );

        println!("\t\tAway team");

        println!("\t\t\tNominal case");
        assert!(m_nomref.away().ft_goals() >= m_nomref.away().ht_goals());

        println!("\t\t\tWith a wrong value");
        let mut vecrec = nomvec.clone();
        vecrec[8] = "4";
        let m_res = Match::value_to_self(&StringRecord::from(vecrec));
        assert!(m_res.is_err());
        assert_eq!(
            m_res.expect_err("The match is supposed to be an error."),
            "There should be more goals at the end of the match than at its half-time for the away team.".to_string()
        );
    }
}
