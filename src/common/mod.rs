pub mod team;
pub use team::Team;

pub mod game;
pub use game::Match;

mod championship;
pub use championship::Championship;

mod country;
pub use country::Country;

mod season;
pub use season::Season;

pub type CieloResult<T> = Result<T, String>;

pub trait ParsedValue {}
