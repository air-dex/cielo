use std::convert::TryFrom;
use std::str::FromStr;
use regex::Regex;

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct Season {
    begin_year: u8,
    end_year: u8
}

impl Season {
    pub const fn const_new(begin_year: u8, end_year: u8) -> Self {
        Self {begin_year, end_year}
    }

    pub fn begin_year(&self) -> u8 {
        self.begin_year
    }

    pub fn end_year(&self) -> u8 {
        self.end_year
    }
}

impl TryFrom<&String> for Season {
    type Error = String;

    fn try_from(season: &String) -> Result<Self, Self::Error> {
        let regex = match Regex::new(SEASON_PATTERN) {
            Ok(re) => re,
            Err(e) => return Err(format!("Cannot compile season regex {}: {}.", SEASON_PATTERN, e.to_string()))
        };

        match regex.captures(season.as_ref()) {
            Some(captures) => {
                // Beginning year
                let begin_year: u8 = match captures.name("b") {
                    Some(m) => {
                        let y = m.as_str();
                        match u8::from_str(y) {
                            Ok(nb) => nb,
                            Err(e) => return Err(format!("Cannot parse '{}' beginning year: {}", y, e.to_string()))
                        }
                    },
                    None => return Err("Cannot find the season's begin year.".to_string())
                };

                // Following year
                let end_year: u8 = match captures.name("e") {
                    Some(m) => {
                        let y = m.as_str();
                        match u8::from_str(y) {
                            Ok(nb) => nb,
                            Err(e) => return Err(format!("Cannot parse '{}' ending year: {}", y, e.to_string()))
                        }
                    },
                    None => return Err("Cannot find the season's ending year.".to_string())
                };

                if end_year == begin_year + 1 {
                    Ok(Season {begin_year, end_year})
                }
                else {
                    Err("The ending year should follow the beginning year.".to_string())
                }
            },
            None => Err("The season has to be written with the first year on two digits, a caret, then the following year on 2 digits.".to_string())
        }
    }
}

// ^(?P<b>\d{2})-(?P<e>\d{2})$
static SEASON_PATTERN: &'static str = r"^(?P<b>\d{2})-(?P<e>\d{2})$";

#[cfg(test)]
mod tests {
    use super::*;

    fn test_a_parse(season: &str, expected_begin_year: u8, expected_end_year: u8) {
        let s_str = season.to_string();
        let s_res = Season::try_from(&s_str);
        assert!(s_res.is_ok());
        assert_eq!(s_res.unwrap(), Season::const_new(expected_begin_year, expected_end_year));
    }

    fn fail_a_parse(season: &str, err_msg: &str) {
        let s_str = season.to_string();
        let s_res = Season::try_from(&s_str);
        assert!(s_res.is_err());
        assert_eq!(s_res.unwrap_err(), err_msg.to_string());
    }

    #[test]
    fn season_parse() {
        // Nominal case
        println!("\n\tNominal case");

        test_a_parse("18-19", 18, 19);

        // Years beginning with a zero
        println!("\tYears beginning with a zero");

        test_a_parse("09-10", 9, 10);
        test_a_parse("04-05", 4, 5);
        test_a_parse("00-01", 0, 1);
        fail_a_parse(
            "9-10",
            "The season has to be written with the first year on two digits, a caret, then the following year on 2 digits."
        );
        fail_a_parse(
            "08-9",
            "The season has to be written with the first year on two digits, a caret, then the following year on 2 digits."
        );
        fail_a_parse(
            "8-9",
            "The season has to be written with the first year on two digits, a caret, then the following year on 2 digits."
        );

        // Years with 4 digits
        println!("\tYears with 4 digits");

        fail_a_parse(
            "2018-2019",
            "The season has to be written with the first year on two digits, a caret, then the following year on 2 digits."
        );
        fail_a_parse(
            "18-2019",
            "The season has to be written with the first year on two digits, a caret, then the following year on 2 digits."
        );
        fail_a_parse(
            "2018-19",
            "The season has to be written with the first year on two digits, a caret, then the following year on 2 digits."
        );

        // Not following year
        println!("\tNot following year");

        fail_a_parse(
            "10-12",
            "The ending year should follow the beginning year."
        );
        fail_a_parse(
            "13-12",
            "The ending year should follow the beginning year."
        );
        fail_a_parse(
            "14-12",
            "The ending year should follow the beginning year."
        );

        // Missing years
        println!("\tMissing years");

        fail_a_parse(
            "-",
            "The season has to be written with the first year on two digits, a caret, then the following year on 2 digits."
        );
        fail_a_parse(
            "18-",
            "The season has to be written with the first year on two digits, a caret, then the following year on 2 digits."
        );
        fail_a_parse(
            "-19",
            "The season has to be written with the first year on two digits, a caret, then the following year on 2 digits."
        );

        // Negative years
        println!("\tNegative years");

        fail_a_parse(
            "-18-19",
            "The season has to be written with the first year on two digits, a caret, then the following year on 2 digits."
        );
        fail_a_parse(
            "-18--19",
            "The season has to be written with the first year on two digits, a caret, then the following year on 2 digits."
        );
        fail_a_parse(
            "18--19",
            "The season has to be written with the first year on two digits, a caret, then the following year on 2 digits."
        );

        // Other values
        println!("\tOther values");

        fail_a_parse(
            "hotot",
            "The season has to be written with the first year on two digits, a caret, then the following year on 2 digits."
        );
    }
}
