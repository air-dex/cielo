use std::env::current_exe;
use std::path::PathBuf;
use crate::common::{Country, Season, CieloResult};
use crate::data::file_formats::FileFormat;

#[inline]
pub fn current_exedir() -> CieloResult<PathBuf> {
    match current_exe() {
        Ok(p) => match p.parent() { // Removing the program name
            Some(pp) => Ok(pp.to_path_buf()),
            None => return Err("Cannot get the current exe directory.".to_string())
        },
        Err(e) => return Err(format!("Cannot get the current exe file name: {}", e.to_string()))
    }
}

fn rel_datapath(country: Country, season: Season, ifmt: FileFormat) -> PathBuf {
    let mut rdp: PathBuf = PathBuf::new();

    rdp.push("data");
    rdp.push(country.to_string());
    rdp.push(build_data_filename(season, ifmt));

    rdp
}

pub fn datapath(country: Country, season: Season, ifmt: FileFormat) -> CieloResult<PathBuf> {
    let mut wd: PathBuf = current_exedir()?;

    wd.push(rel_datapath(country, season, ifmt));

    Ok(wd)
}

fn build_data_filename(season: Season, ifmt: FileFormat) -> String {
    format!(
        "season-{b:02}{e:02}_{ext}.{ext}",
        b = season.begin_year(),
        e= season.end_year(),
        ext= ifmt.to_string()
    )
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::data::file_formats::csv::CsvParser;
    use crate::data::file_formats::json::JsonParser;

    #[test]
    fn data_filename() {
        println!("\n\tNominal case");

        let s = Season::const_new(15, 16);
        assert_eq!(build_data_filename(s, FileFormat::Csv(CsvParser {})), "season-1516_csv.csv");
        assert_eq!(build_data_filename(s, FileFormat::Json(JsonParser {})), "season-1516_json.json");

        println!("\tSeasons with leading zeros");

        let s = Season::const_new(9, 10);
        assert_eq!(build_data_filename(s, FileFormat::Csv(CsvParser {})), "season-0910_csv.csv");
        assert_eq!(build_data_filename(s, FileFormat::Json(JsonParser {})), "season-0910_json.json");

        let s = Season::const_new(4, 5);
        assert_eq!(build_data_filename(s, FileFormat::Csv(CsvParser {})), "season-0405_csv.csv");
        assert_eq!(build_data_filename(s, FileFormat::Json(JsonParser {})), "season-0405_json.json");

        let s = Season::const_new(0, 1);
        assert_eq!(build_data_filename(s, FileFormat::Csv(CsvParser {})), "season-0001_csv.csv");
        assert_eq!(build_data_filename(s, FileFormat::Json(JsonParser {})), "season-0001_json.json");
    }

    #[inline]
    fn test_a_datapath(country: Country, byear: u8, eyear: u8, ifmt: FileFormat, exp_country: &str, exp_filename: &str) {
        let season: Season = Season::const_new(byear, eyear);
        let dp_res = rel_datapath(country, season, ifmt);

        let mut exp_path = PathBuf::new();
        exp_path.push("data");
        exp_path.push(exp_country);
        exp_path.push(exp_filename);

        assert_eq!(
            dp_res, exp_path,
            "The datapath for {c:?}, {s:?} and {i:?} should be {ep}",
            c = country, s= season, i= ifmt, ep= exp_path.to_str().expect("The expected path should be displayed.")
        );
    }

    #[test]
    fn datapath_test() {
        println!("\n\tNominal case");

        test_a_datapath(Country::Italy, 15, 16, FileFormat::Csv(CsvParser {}),
                        "italy", "season-1516_csv.csv");
        test_a_datapath(Country::Spain, 12, 13, FileFormat::Json(JsonParser {}),
                        "spain", "season-1213_json.json");

        println!("\tSeasons with leading zeros");

        test_a_datapath(Country::Italy, 9, 10, FileFormat::Csv(CsvParser {}),
                        "italy", "season-0910_csv.csv");
        test_a_datapath(Country::Spain, 9, 10, FileFormat::Json(JsonParser {}),
                        "spain", "season-0910_json.json");

        test_a_datapath(Country::Italy, 2, 3, FileFormat::Csv(CsvParser {}),
                        "italy", "season-0203_csv.csv");
        test_a_datapath(Country::Germany, 2, 3, FileFormat::Json(JsonParser {}),
                        "germany", "season-0203_json.json");

        test_a_datapath(Country::France, 0, 1, FileFormat::Csv(CsvParser {}),
                        "france", "season-0001_csv.csv");
        test_a_datapath(Country::England, 0, 1, FileFormat::Json(JsonParser {}),
                        "england", "season-0001_json.json");
    }
}
