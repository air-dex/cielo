use std::path::PathBuf;
use std::io::{Read, Write};
use csv::{Reader, Writer, StringRecord};
use crate::common::CieloResult;
use crate::common::ParsedValue;
use crate::data::traits::ValueParser;

pub type VSR = Vec<StringRecord>;

impl ParsedValue for VSR {}
impl ParsedValue for StringRecord {}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct CsvParser;

impl CsvParser {
    fn write_csv<T: Write>(&self, w: &mut Writer<T>, records: &VSR) -> CieloResult<()> {
        let mut err_msgs: Vec<String> = records.iter()
            .map(|sr| {
                match w.write_record(sr) {
                    Ok(_wok) => "".to_string(),
                    Err(e) => format!("Cannot write CSV data: {}", e.to_string())
                }
            })
            .filter(|err_msg| {
                !err_msg.is_empty()
            })
            .collect();

        if let Err(e) = w.flush() {
            err_msgs.push(format!("Cannot flush CSV: {}", e.to_string()));
        }

        if err_msgs.is_empty() {
            Ok(())
        }
        else {
            Err(format!("Something happened during CSV writing: {}", err_msgs.join(" ")))
        }
    }

    fn fetch_csv_records<T: Read>(&self, r: &mut Reader<T>) -> CieloResult<VSR> {
        let mut records: VSR = vec![];
        let mut err_msgs: Vec<String> = vec![];

        r.records().for_each(|rec_res| {
            match rec_res {
                Ok(rec) => records.push(rec),
                Err(e) => err_msgs.push(e.to_string())
            }
        });

        if err_msgs.is_empty() {
            Ok(records)
        }
        else {
            Err(format!("Something happened while parsing CSV datas: {}", err_msgs.join(" ")))
        }
    }
}

impl ValueParser<VSR> for CsvParser {
    fn value_to_file(&self, v: &VSR, dp: &PathBuf) -> CieloResult<()> {
        match Writer::from_path(dp) {
            Ok(mut w) => self.write_csv(&mut w, v),
            Err(e) => Err(format!(
                "Cannot write CSV datas in '{dp}': {e}",
                dp = dp.to_str().expect("The file path for CSV should be displayed."),
                e = e.to_string()
            ))
        }
    }

    fn value_to_string(&self, v: &VSR) -> CieloResult<String> {
        let mut w = Writer::from_writer(vec![]);

        if let Err(e) = self.write_csv(&mut w, v) {
            return Err(e)
        }

        match w.into_inner() {
            Ok(utf8_str) => match String::from_utf8(utf8_str) {
                Ok(s) => Ok(s),
                Err(e) => Err(format!("Error while putting CSV data into a string: {}", e.to_string()))
            },
            Err(e) => Err(format!("Error while putting CSV data into a string: {}", e.to_string()))
        }
    }

    fn file_to_value(&self, path: &PathBuf) -> CieloResult<VSR> {
        match Reader::from_path(path) {
            Ok(mut r) => self.fetch_csv_records(&mut r),
            Err(e) => Err(format!(
                "Cannot read the CSV file located at '{dp}': {e}",
                dp = path.to_str().expect("The file path should be readable"),
                e = e.to_string()
            ))
        }
    }

    fn string_to_value(&self, s: &String) -> CieloResult<VSR> {
        let mut r = Reader::from_reader(s.as_bytes());
        self.fetch_csv_records(&mut r)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::env;
    use std::fs::{File, read_to_string, remove_file};

    fn create_writer(filename: &str) -> (Writer<File>, PathBuf) {
        let mut tmp = env::temp_dir();
        tmp.push(filename);

        match Writer::from_path(&tmp) {
            Ok(w) => (w, tmp),
            Err(e) => panic!(
                "Cannot create a CSV Writer for {tmp}: {e}.",
                tmp = tmp.to_str().expect("The data path should be readable."),
                e = e.to_string()
            )
        }
    }

    static CSV_PARSER: CsvParser = CsvParser{};

    #[test]
    fn write_records() {
        println!("\n\tNominal case");

        let (mut wnc, fp) = create_writer("nc.csv");
        let vsr: VSR = vec![StringRecord::from(vec!["a", "1", "toto"])];

        let p_res = CSV_PARSER.write_csv(&mut wnc, &vsr);
        assert!(p_res.is_ok());
        let s = match read_to_string(&fp) {
            Ok(fc) => fc,
            Err(e) => panic!("File content should be readable but it's not: {}", e.to_string())
        };
        assert_eq!(s, "a,1,toto\n");

        if let Err(e) = remove_file(fp) {
            eprintln!("The file was not removed: {}", e.to_string());
        }

        println!("\tEmpty VSR");
        let (mut wnc, fp) = create_writer("empty_vsr.csv");
        let vsr: VSR = vec![StringRecord::from(Vec::<String>::new())];

        let p_res = CSV_PARSER.write_csv(&mut wnc, &vsr);
        assert!(p_res.is_ok());
        let s = match read_to_string(&fp) {
            Ok(fc) => fc,
            Err(e) => panic!("File content should be readable but it's not: {}", e.to_string())
        };
        assert_eq!(s, "\"\"\n");

        if let Err(e) = remove_file(fp) {
            eprintln!("The file was not removed: {}", e.to_string());
        }
    }

    #[test]
    fn fetch_records() {
        let root: PathBuf = match env::var("CARGO_MANIFEST_DIR") {
            Ok(f) => PathBuf::from(f),
            Err(e) => panic!("Cannot get the package root: {}", e.to_string())
        };

        println!("\n\tNominal case");
        println!("\t\tFetching...");
        let mut ncpath = root.clone();
        ncpath.push("tests");
        ncpath.push("data");
        ncpath.push("test.csv");

        let mut reader = match Reader::from_path(&ncpath) {
            Ok(r) => r,
            Err(e) => panic!("A CSV reader cannot be created: {}", e.to_string())
        };
        let p_res = CSV_PARSER.fetch_csv_records(&mut reader);
        assert!(p_res.is_ok());
        let vsr = p_res.expect("Parsing CSV should be OK.");
        assert_eq!(vsr.len(), 3);

        println!("\t\tVerifying 1st record");
        let r = &vsr[0];
        let mut it = r.iter();

        match it.next() {
            Some(s) => assert_eq!(s, "VeeDotMee"),
            None => panic!("There should be a 'VeeDotMee' string there")
        }

        match it.next() {
            Some(s) => assert_eq!(s, "Dead Cells"),
            None => panic!("There should be a 'Dead Cells' string there")
        }

        if let Some(s) = it.next() {
            panic!("The record should be over but the following string was found there: '{}'", s);
        }

        println!("\t\tVerifying 2nd record");
        let r = &vsr[1];
        let mut it = r.iter();

        match it.next() {
            Some(s) => assert_eq!(s, "PewPew82"),
            None => panic!("There should be a 'PewPew82' string there")
        }

        match it.next() {
            Some(s) => assert_eq!(s, "Path of Exile"),
            None => panic!("There should be a 'Path of Exile' string there")
        }

        if let Some(s) = it.next() {
            panic!("The record should be over but the following string was found there: '{}'", s);
        }

        println!("\t\tVerifying 3rd record");
        let r = &vsr[2];
        let mut it = r.iter();

        match it.next() {
            Some(s) => assert_eq!(s, "At0mium"),
            None => panic!("There should be a 'At0mium' string there")
        }

        match it.next() {
            Some(s) => assert_eq!(s, "Death Stranding"),
            None => panic!("There should be a 'Death Stranding' string there")
        }

        if let Some(s) = it.next() {
            panic!("The record should be over but the following string was found there: '{}'", s);
        }

        println!("\tHeader-only CSV file");
        println!("\t\tFetching...");
        let mut ncpath = root.clone();
        ncpath.push("tests");
        ncpath.push("data");
        ncpath.push("test_ho.csv");

        let mut reader = match Reader::from_path(&ncpath) {
            Ok(r) => r,
            Err(e) => panic!("A CSV reader cannot be created: {}", e.to_string())
        };
        let p_res = CSV_PARSER.fetch_csv_records(&mut reader);
        assert!(p_res.is_ok());
        let vsr = p_res.expect("Parsing CSV should be OK.");
        assert!(vsr.is_empty());

        println!("\tEmpty file");
        println!("\t\tFetching...");
        let mut ncpath = root.clone();
        ncpath.push("tests");
        ncpath.push("data");
        ncpath.push("empty");

        let mut reader = match Reader::from_path(&ncpath) {
            Ok(r) => r,
            Err(e) => panic!("A CSV reader cannot be created: {}", e.to_string())
        };
        let p_res = CSV_PARSER.fetch_csv_records(&mut reader);
        assert!(p_res.is_ok());
        let vsr = p_res.expect("Parsing CSV should be OK.");
        assert!(vsr.is_empty());

        println!("\tNot a CSV file");
        let mut ncpath = root.clone();
        ncpath.push("src");
        ncpath.push("main.rs");

        let mut reader = match Reader::from_path(&ncpath) {
            Ok(r) => r,
            Err(e) => panic!("A CSV reader cannot be created: {}", e.to_string())
        };
        let p_res = CSV_PARSER.fetch_csv_records(&mut reader);
        assert!(p_res.is_err());
        let err_msg = p_res.expect_err("There should be an error here.");
        assert!(err_msg.contains("Something happened while parsing CSV datas: "));
    }

    #[test]
    fn v2f() {
        println!("\n\tNominal case");

        let (mut _wnc, fp) = create_writer("nc.csv");
        let vsr: VSR = vec![StringRecord::from(vec!["a", "1", "toto"])];

        let p_res = CSV_PARSER.value_to_file(&vsr, &fp);
        assert!(p_res.is_ok());
        let s = match read_to_string(&fp) {
            Ok(fc) => fc,
            Err(e) => panic!("File content should be readable but it's not: {}", e.to_string())
        };
        assert_eq!(s, "a,1,toto\n");

        if let Err(e) = remove_file(fp) {
            eprintln!("The file was not removed: {}", e.to_string());
        }

        println!("\tEmpty VSR");
        let (mut _wnc, fp) = create_writer("empty_vsr.csv");
        let vsr: VSR = vec![StringRecord::from(Vec::<String>::new())];

        let p_res = CSV_PARSER.value_to_file(&vsr, &fp);
        assert!(p_res.is_ok());
        let s = match read_to_string(&fp) {
            Ok(fc) => fc,
            Err(e) => panic!("File content should be readable but it's not: {}", e.to_string())
        };
        assert_eq!(s, "\"\"\n");

        if let Err(e) = remove_file(fp) {
            eprintln!("The file was not removed: {}", e.to_string());
        }
    }

    #[test]
    fn f2v() {
        let root: PathBuf = match env::var("CARGO_MANIFEST_DIR") {
            Ok(f) => PathBuf::from(f),
            Err(e) => panic!("Cannot get the package root: {}", e.to_string())
        };

        println!("\n\tNominal case");
        println!("\t\tFetching...");
        let mut ncpath = root.clone();
        ncpath.push("tests");
        ncpath.push("data");
        ncpath.push("test.csv");

        let p_res = CSV_PARSER.file_to_value(&ncpath);
        assert!(p_res.is_ok());
        let vsr = p_res.expect("Parsing CSV should be OK.");
        assert_eq!(vsr.len(), 3);

        println!("\t\tVerifying 1st record");
        let r = &vsr[0];
        let mut it = r.iter();

        match it.next() {
            Some(s) => assert_eq!(s, "VeeDotMee"),
            None => panic!("There should be a 'VeeDotMee' string there")
        }

        match it.next() {
            Some(s) => assert_eq!(s, "Dead Cells"),
            None => panic!("There should be a 'Dead Cells' string there")
        }

        if let Some(s) = it.next() {
            panic!("The record should be over but the following string was found there: '{}'", s);
        }

        println!("\t\tVerifying 2nd record");
        let r = &vsr[1];
        let mut it = r.iter();

        match it.next() {
            Some(s) => assert_eq!(s, "PewPew82"),
            None => panic!("There should be a 'PewPew82' string there")
        }

        match it.next() {
            Some(s) => assert_eq!(s, "Path of Exile"),
            None => panic!("There should be a 'Path of Exile' string there")
        }

        if let Some(s) = it.next() {
            panic!("The record should be over but the following string was found there: '{}'", s);
        }

        println!("\t\tVerifying 3rd record");
        let r = &vsr[2];
        let mut it = r.iter();

        match it.next() {
            Some(s) => assert_eq!(s, "At0mium"),
            None => panic!("There should be a 'At0mium' string there")
        }

        match it.next() {
            Some(s) => assert_eq!(s, "Death Stranding"),
            None => panic!("There should be a 'Death Stranding' string there")
        }

        if let Some(s) = it.next() {
            panic!("The record should be over but the following string was found there: '{}'", s);
        }

        println!("\tHeader-only CSV file");
        println!("\t\tFetching...");
        let mut ncpath = root.clone();
        ncpath.push("tests");
        ncpath.push("data");
        ncpath.push("test_ho.csv");

        let p_res = CSV_PARSER.file_to_value(&ncpath);
        assert!(p_res.is_ok());
        let vsr = p_res.expect("Parsing CSV should be OK.");
        assert!(vsr.is_empty());

        println!("\tEmpty file");
        println!("\t\tFetching...");
        let mut ncpath = root.clone();
        ncpath.push("tests");
        ncpath.push("data");
        ncpath.push("empty");

        let p_res = CSV_PARSER.file_to_value(&ncpath);
        assert!(p_res.is_ok());
        let vsr = p_res.expect("Parsing CSV should be OK.");
        assert!(vsr.is_empty());

        println!("\tNot a CSV file");
        let mut ncpath = root.clone();
        ncpath.push("src");
        ncpath.push("main.rs");

        let p_res = CSV_PARSER.file_to_value(&ncpath);
        assert!(p_res.is_err());
        let err_msg = p_res.expect_err("There should be an error here.");
        assert!(err_msg.contains("Something happened while parsing CSV datas: "));

        println!("\tUnknown file");
        let mut ncpath = root.clone();
        ncpath.push("tests");
        ncpath.push("data");
        ncpath.push("404.csvnotfound");

        let p_res = CSV_PARSER.file_to_value(&ncpath);
        assert!(p_res.is_err());
        let err_msg: String = p_res.expect_err("There should be an error here.");
        let exp_em = format!(
            "Cannot read the CSV file located at '{dp}': ",
            dp = ncpath.to_str().expect("The file path should be readable")
        );
        assert!(err_msg.starts_with(&exp_em));
    }

    #[test]
    fn v2s() {
        println!("\n\tNominal case");

        let vsr: VSR = vec![StringRecord::from(vec!["a", "1", "toto"])];
        let p_res = CSV_PARSER.value_to_string(&vsr);
        assert!(p_res.is_ok());
        let s = p_res.expect("The parsing should be OK!");
        assert_eq!(s, "a,1,toto\n");

        println!("\tEmpty VSR");

        let vsr: VSR = vec![StringRecord::from(Vec::<String>::new())];
        let p_res = CSV_PARSER.value_to_string(&vsr);
        assert!(p_res.is_ok());
        let s = p_res.expect("The parsing should be OK!");
        assert_eq!(s, "\"\"\n");
    }

    #[test]
    fn s2v() {
        println!("\n\tNominal case");

        println!("\t\tFetching...");
        let csv = String::from("Streamer,Game\nVeeDotMee,Dead Cells\nPewPew82,Path of Exile\nAt0mium,Death Stranding\n");
        let p_res = CSV_PARSER.string_to_value(&csv);
        assert!(p_res.is_ok());
        let vsr = p_res.expect("Parsing CSV should be OK.");
        assert_eq!(vsr.len(), 3);

        println!("\t\tVerifying 1st record");
        let r = &vsr[0];
        let mut it = r.iter();

        match it.next() {
            Some(s) => assert_eq!(s, "VeeDotMee"),
            None => panic!("There should be a 'VeeDotMee' string there")
        }

        match it.next() {
            Some(s) => assert_eq!(s, "Dead Cells"),
            None => panic!("There should be a 'Dead Cells' string there")
        }

        if let Some(s) = it.next() {
            panic!("The record should be over but the following string was found there: '{}'", s);
        }

        println!("\t\tVerifying 2nd record");
        let r = &vsr[1];
        let mut it = r.iter();

        match it.next() {
            Some(s) => assert_eq!(s, "PewPew82"),
            None => panic!("There should be a 'PewPew82' string there")
        }

        match it.next() {
            Some(s) => assert_eq!(s, "Path of Exile"),
            None => panic!("There should be a 'Path of Exile' string there")
        }

        if let Some(s) = it.next() {
            panic!("The record should be over but the following string was found there: '{}'", s);
        }

        println!("\t\tVerifying 3rd record");
        let r = &vsr[2];
        let mut it = r.iter();

        match it.next() {
            Some(s) => assert_eq!(s, "At0mium"),
            None => panic!("There should be a 'At0mium' string there")
        }

        match it.next() {
            Some(s) => assert_eq!(s, "Death Stranding"),
            None => panic!("There should be a 'Death Stranding' string there")
        }

        if let Some(s) = it.next() {
            panic!("The record should be over but the following string was found there: '{}'", s);
        }

        println!("\tHeader-only CSV file");

        println!("\t\tFetching...");
        let csv = "Streamer,Game".to_string();
        let p_res = CSV_PARSER.string_to_value(&csv);
        assert!(p_res.is_ok());
        let vsr = p_res.expect("Parsing CSV should be OK.");
        assert!(vsr.is_empty());

        println!("\tEmpty file");

        println!("\t\tFetching...");
        let csv = "".to_string();
        let p_res = CSV_PARSER.string_to_value(&csv);
        assert!(p_res.is_ok());
        let vsr = p_res.expect("Parsing CSV should be OK.");
        assert!(vsr.is_empty());

        println!("\tNot a CSV file");
        let csv = String::from("fn main() -> () {\n\tprintln!(\"Hello, world!\");\n}\n");

        let p_res = CSV_PARSER.string_to_value(&csv);
        assert!(p_res.is_err());
        let err_msg = p_res.expect_err("There should be an error here.");
        assert!(err_msg.starts_with("Something happened while parsing CSV datas: "));
    }
}
