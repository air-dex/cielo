use std::convert::TryFrom;
use std::fmt;
use crate::data::file_formats::csv::CsvParser;
use crate::data::file_formats::json::JsonParser;

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum FileFormat {
    Json(JsonParser),
    Csv(CsvParser),
    Xml, Yaml, Human, Sqlite
}

impl TryFrom<&String> for FileFormat {
    type Error = String;

    fn try_from(format: &String) -> Result<Self, Self::Error> {
        match format.as_str() {
            "csv" => Ok(FileFormat::Csv(CsvParser {})),
            "json" => Ok(FileFormat::Json(JsonParser {})),
            "xml" => Ok(FileFormat::Xml),
            "yaml" => Ok(FileFormat::Yaml),
            "human" | "log" => Ok(FileFormat::Human),
            "sqlite" => Ok(FileFormat::Sqlite),
            _ => Err(format!("Unknown {} file format", format))
        }
    }
}

impl fmt::Display for FileFormat {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        write!(f, "{}", match self {
            FileFormat::Csv(_) => "csv",
            FileFormat::Json(_) => "json",
            FileFormat::Xml => "xml",
            FileFormat::Yaml => "yaml",
            FileFormat::Human => "log",
            FileFormat::Sqlite => "sqlite",
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn test_a_file_format(ff_str: &str, expected_ff: FileFormat) {
        let ff_str = ff_str.to_string();
        let ff = FileFormat::try_from(&ff_str);
        assert!(ff.is_ok(), format!("{} is supposed to be a valid file format string", ff_str));
        assert_eq!(ff.unwrap(), expected_ff);
    }

    #[test]
    fn format_from() {
        // Valid strings
        println!("\n\tValid strings");

        test_a_file_format("csv", FileFormat::Csv(CsvParser {}));
        test_a_file_format("json", FileFormat::Json(JsonParser {}));
        test_a_file_format("xml", FileFormat::Xml);
        test_a_file_format("yaml", FileFormat::Yaml);
        test_a_file_format("human", FileFormat::Human);
        test_a_file_format("log", FileFormat::Human);
        test_a_file_format("sqlite", FileFormat::Sqlite);

        // Invalid string
        println!("\tInvalid string");

        let ff_str = "foobar".to_string();
        let ff = FileFormat::try_from(&ff_str);
        assert!(ff.is_err());
        assert_eq!(ff.unwrap_err(), "Unknown foobar file format".to_string());
    }

    #[test]
    fn to_string() {
        println!();
        assert_eq!(FileFormat::Csv(CsvParser {}).to_string(), "csv");
        assert_eq!(FileFormat::Json(JsonParser {}).to_string(), "json");
        assert_eq!(FileFormat::Xml.to_string(), "xml");
        assert_eq!(FileFormat::Yaml.to_string(), "yaml");
        assert_eq!(FileFormat::Human.to_string(), "log");
        assert_ne!(FileFormat::Human.to_string(), "human");
        assert_eq!(FileFormat::Sqlite.to_string(), "sqlite");
    }
}
