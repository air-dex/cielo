use crate::common::CieloResult;
use crate::common::ParsedValue;
use crate::data::traits::ValueParser;
use serde_json::Value;

impl ParsedValue for Value {}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct JsonParser;

impl ValueParser<Value> for JsonParser {
    fn value_to_string(&self, v: &Value) -> CieloResult<String> {
        Ok(v.to_string())
    }

    fn string_to_value(&self, s: &String) -> CieloResult<Value> {
        match serde_json::from_str(s.as_str()) {
            Ok(v) => Ok(v),
            Err(e) => Err(format!(
                "Cannot get JSON value of data: {e}. Data: '{d}'",
                e = e.to_string(), d = s
            )),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::env;
    use std::fs::{read_to_string, remove_file};
    use std::path::PathBuf;

    static JSON_PARSER: JsonParser = JsonParser {};

    fn test_it_obj(itnext: &Option<&Value>, exp: &Vec<(&str, &str)>) {
        match itnext {
            Some(v) => test_obj(v, exp),
            None => panic!("Iterator is not supposed to be over now.")
        }
    }

    fn test_obj(v: &Value, exp: &Vec<(&str, &str)>) {
        assert!(v.is_object());
        let o = v.as_object().expect("The JSON object is supposed to be an array");
        assert_eq!(o.len(), exp.len());

        exp.iter().for_each(|&(ek, ev) | {
            assert!(o.contains_key(ek));

            match o.get(ek) {
                Some(v) => {
                    assert!(v.is_string());
                    assert_eq!(v.as_str().expect("The field is supposed to be a string"), ev);
                },
                None => panic!("The object {o:?} is supposed tyo have a '{k}' field.", o = o, k = ek)
            }
        });
    }

    #[test]
    fn v2f() {
        println!("\n\tNominal case");

        println!("\t\tObject");
        let obj = serde_json::json!({
            "game": "Dead Cells",
            "studio": "Motion Twin"
        });

        let mut path: PathBuf = env::temp_dir();
        path.push("nco.json");

        let p_res = JSON_PARSER.value_to_file(&obj, &path);
        assert!(p_res.is_ok());

        match read_to_string(&path) {
            Ok(s) => assert_eq!(s, "{\"game\":\"Dead Cells\",\"studio\":\"Motion Twin\"}".to_string()),
            Err(e) => panic!("Cannot read result: {}", e.to_string())
        }

        if let Err(e) = remove_file(&path) {
            eprintln!(
                "Cannot remove test file '{dp}': {e}",
                dp = path.to_str().expect("File path should be readable"),
                e = e.to_string()
            )
        }

        println!("\t\tList");
        let list = serde_json::json!([
            {"streamer": "VeeDotMee", "game": "Dead Cells"},
            {"streamer": "PewPew82", "game": "Path of Exile"},
            {"streamer": "At0mium", "game": "Death Stranding"}
        ]);

        let mut path: PathBuf = env::temp_dir();
        path.push("ncl.json");

        let p_res = JSON_PARSER.value_to_file(&list, &path);
        assert!(p_res.is_ok());

        match read_to_string(&path) {
            Ok(s) => assert_eq!(s,"[{\"game\":\"Dead Cells\",\"streamer\":\"VeeDotMee\"},{\"game\":\"Path of Exile\",\"streamer\":\"PewPew82\"},{\"game\":\"Death Stranding\",\"streamer\":\"At0mium\"}]".to_string()),
            Err(e) => panic!("Cannot read result: {}", e.to_string())
        }

        if let Err(e) = remove_file(&path) {
            eprintln!(
                "Cannot remove test file '{dp}': {e}",
                dp = path.to_str().expect("File path should be readable"),
                e = e.to_string()
            )
        }
    }

    #[test]
    fn f2v() {
        let mut root = match env::var("CARGO_MANIFEST_DIR"){
            Ok(s) => PathBuf::from(s),
            Err(e) => panic!("Cannot get project root: {e}", e = e.to_string())
        };
        root.push("tests");
        root.push("data");

        println!("\n\tNominal case");

        println!("\t\tList");

        let mut path = root.clone();
        path.push("test_list.json");

        let p_res = JSON_PARSER.file_to_value(&path);
        assert!(p_res.is_ok());

        let jsonval:Value = p_res.expect("Result should be OK");
        assert!(jsonval.is_array());
        let jsonval: &Vec<Value> = jsonval.as_array().expect("The JSON value is supposed to be an array");
        assert_eq!(jsonval.len(), 3);

        let mut it = jsonval.iter();

        test_it_obj(&it.next(), &vec![("streamer", "VeeDotMee"), ("game", "Dead Cells")]);
        test_it_obj(&it.next(), &vec![("streamer", "PewPew82"), ("game", "Path of Exile")]);
        test_it_obj(&it.next(), &vec![("streamer", "At0mium"), ("game", "Death Stranding")]);

        if let Some(v) = it.next() {
            panic!("The iterator is supposed to be over now. Got this instead: {:?}", v)
        }

        println!("\t\tObject");

        let mut path = root.clone();
        path.push("test_obj.json");

        let p_res = JSON_PARSER.file_to_value(&path);
        assert!(p_res.is_ok());

        let jsonval: Value = p_res.expect("Result should be OK");
        test_obj(&jsonval, &vec![("streamer", "VeeDotMee"), ("game", "Dead Cells")]);

        println!("\tEmpty file");

        let mut path = root.clone();
        path.push("empty");

        let p_res = JSON_PARSER.file_to_value(&path);
        assert!(p_res.is_err());
        let err = p_res.expect_err("The result is supposed to be an error");
        let exp_err = String::from("Cannot get JSON value of data: ");
        assert!(
            err.starts_with(&exp_err),
            "The message error should starts with '{ee}'. Got this instead: {err}", ee = exp_err, err = err
        );

        println!("\tNot JSON");

        let mut path = root.clone();
        path.push("test_ho.csv");

        let p_res = JSON_PARSER.file_to_value(&path);
        assert!(p_res.is_err());
        let err = p_res.expect_err("The result is supposed to be an error");
        let exp_err = String::from("Cannot get JSON value of data: ");
        assert!(
            err.starts_with(&exp_err),
            "The message error should starts with '{ee}'. Got this instead: {err}", ee = exp_err, err = err
        );
    }

    #[test]
    fn v2s() {
        println!("\n\tNominal case");

        println!("\t\tObject");
        let obj = serde_json::json!({
            "game": "Dead Cells",
            "studio": "Motion Twin"
        });

        let p_res = JSON_PARSER.value_to_string(&obj);
        assert!(p_res.is_ok());
        assert_eq!(
            p_res.expect("Parsing is supposed to be OK"),
            "{\"game\":\"Dead Cells\",\"studio\":\"Motion Twin\"}".to_string()
        );

        println!("\t\tList");
        let list = serde_json::json!([
            {"streamer": "VeeDotMee", "game": "Dead Cells"},
            {"streamer": "PewPew82", "game": "Path of Exile"},
            {"streamer": "At0mium", "game": "Death Stranding"}
        ]);

        let p_res = JSON_PARSER.value_to_string(&list);
        assert!(p_res.is_ok());
        assert_eq!(
            p_res.expect("Parsing is supposed to be OK"),
            "[{\"game\":\"Dead Cells\",\"streamer\":\"VeeDotMee\"},{\"game\":\"Path of Exile\",\"streamer\":\"PewPew82\"},{\"game\":\"Death Stranding\",\"streamer\":\"At0mium\"}]".to_string()
        );
    }

    #[test]
    fn s2v() {
        println!("\n\tNominal case");

        println!("\t\tList");

        let p_res = JSON_PARSER.string_to_value(&"[{\"streamer\":\"VeeDotMee\",\"game\":\"Dead Cells\"}, {\"streamer\":\"PewPew82\",\"game\":\"Path of Exile\"},{\"streamer\": \"At0mium\", \"game\": \"Death Stranding\"}]".to_string());
        assert!(p_res.is_ok());

        let jsonval:Value = p_res.expect("Result should be OK");
        assert!(jsonval.is_array());
        let jsonval: &Vec<Value> = jsonval.as_array().expect("The JSON value is supposed to be an array");
        assert_eq!(jsonval.len(), 3);

        let mut it = jsonval.iter();

        test_it_obj(&it.next(), &vec![("streamer", "VeeDotMee"), ("game", "Dead Cells")]);
        test_it_obj(&it.next(), &vec![("streamer", "PewPew82"), ("game", "Path of Exile")]);
        test_it_obj(&it.next(), &vec![("streamer", "At0mium"), ("game", "Death Stranding")]);

        if let Some(v) = it.next() {
            panic!("The iterator is supposed to be over now. Got this instead: {:?}", v)
        }

        println!("\t\tObject");

        let p_res = JSON_PARSER.string_to_value(&"{\"streamer\": \"VeeDotMee\", \"game\":\"Dead Cells\"}".to_string());
        assert!(p_res.is_ok());

        let jsonval: Value = p_res.expect("Result should be OK");
        test_obj(&jsonval, &vec![("streamer", "VeeDotMee"), ("game", "Dead Cells")]);

        println!("\tEmpty file");

        let p_res = JSON_PARSER.string_to_value(&"".to_string());
        assert!(p_res.is_err());
        let err = p_res.expect_err("The result is supposed to be an error");
        let exp_err = String::from("Cannot get JSON value of data: ");
        assert!(
            err.starts_with(&exp_err),
            "The message error should starts with '{ee}'. Got this instead: {err}", ee = exp_err, err = err
        );

        println!("\tNot JSON");

        let p_res = JSON_PARSER.string_to_value(&"Streamer,Game".to_string());
        assert!(p_res.is_err());
        let err = p_res.expect_err("The result is supposed to be an error");
        let exp_err = String::from("Cannot get JSON value of data: ");
        assert!(
            err.starts_with(&exp_err),
            "The message error should starts with '{ee}'. Got this instead: {err}", ee = exp_err, err = err
        );
    }
}
