use crate::common::{CieloResult, ParsedValue};
use std::fs::{read_to_string, write};
use std::marker::Sized;
use std::path::PathBuf;

pub trait ValueParser<V: ParsedValue> {
    fn value_to_file(&self, v: &V, dp: &PathBuf) -> CieloResult<()> {
        match self.value_to_string(v) {
            Ok(s) => match write(dp, &s) {
                Ok(ok) => Ok(ok),
                Err(e) => Err(format!(
                    "Cannot write entity data into '{dp}': {e}\nData: {d}",
                    dp = dp.to_str().expect("The data path should be convertible into a String."),
                    e = e.to_string(),
                    d = s
                ))
            },
            Err(e) => Err(format!(
                "Cannot export data into '{dp}': {e}",
                dp = dp.to_str().expect("The data path should be convertible into a String."),
                e = e.to_string()
            ))
        }
    }

    fn value_to_string(&self, v: &V) -> CieloResult<String>;

    fn file_to_value(&self, path: &PathBuf) -> CieloResult<V>{
        match read_to_string(path) {
            Ok(s) => self.string_to_value(&s),
            Err(e) => return Err(format!(
                "Cannot import data from '{dp}': {e}",
                dp = path.to_str().expect("The data path should be convertible into a String."),
                e = e.to_string()
            ))
        }
    }

    fn string_to_value(&self, s: &String) -> CieloResult<V>;
}

pub trait ImportValue<V: ParsedValue>: Sized {
    fn value_to_self(v: &V) -> CieloResult<Self>;
}

pub trait ExportValue<V: Sized> {
    fn self_to_value(&self) -> CieloResult<V>;
}
