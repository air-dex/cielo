use chrono::NaiveDate;
use crate::common::{Match, CieloResult};
use crate::common::game::{are_matches_sorted, MatchWin};
use crate::common::team::Teams;
use crate::elo::{MatchDay, Journey, Ranking, Rank, MatchConclusion, PointsF};
use std::collections::{HashMap, HashSet};

pub fn compute_championship(teams: &Teams, matches: &Vec<Match>, stdout: bool) -> CieloResult<Journey> {
    // Matches are supposed to be sorted by date.
    if !are_matches_sorted(matches) {
        return Err("The ELO computer only runs on sorted matches. Sort them and try again later.".to_string());
    }

    // Before the first match: everybody has got the same rank and no played matches.
    let mut res: Journey = HashMap::new();
    let mut current_ranking: Ranking = Ranking::from(teams);

    let mut it = matches.iter().peekable();

    while let Some(next_match) = it.peek() {
        // Picking matches of the day.
        let mut motd: HashSet<Match> = HashSet::new();

        motd.insert((*next_match).clone());
        let next_date = next_match.date();
        it.next();

        while let Some(future_match) = it.peek() {
            if future_match.date() == next_date {
                motd.insert((*future_match).clone());
                it.next();
            }
            else {
                break;
            }
        }

        // Computing ELO for matches of the day.
        match compute_day_matches(next_date, motd, &mut current_ranking) {
            Ok(md) => {
                if stdout { md.tell_day_matches(); }
                res.insert(next_date, md);
            },
            Err(e) => return Err(format!(
                "The ELO computer cannot compute matches that were played on {d}: {e}",
                d = next_date.to_string(), e = e
            ))
        };
    }

    Ok(res)
}

pub fn compute_day_matches(day: NaiveDate, matches: HashSet<Match>, ranking: &mut Ranking) -> CieloResult<MatchDay> {
    let mut verdicts: HashSet<MatchConclusion> = HashSet::new();
    let mut err_msgs: Vec<String> = vec![];

    matches.iter().for_each(|m| {
        match compute_match(m, ranking) {
            Ok(mc) => { verdicts.insert(mc); },
            Err(e) => err_msgs.push(format!(
                "Cannot compute ELO for {m}: {e}",
                m = m.title(), e = e
            ))
        }
    });

    // Updating ranking
    ranking.sort_ranks();
    ranking.set_date(day);

    if err_msgs.is_empty() {
        // No errors -> returning MatchDay.
        Ok(MatchDay::new(day, verdicts, ranking.clone()))
    }
    else {
        // Returning errors instead.
        Err(format!(
            "Something happened while computing ELO for {d} matches: {e}",
            d = day.to_string(), e = err_msgs.join(".\n")
        ))
    }
}

pub fn compute_match(m: &Match, ranking: &mut Ranking) -> CieloResult<MatchConclusion> {
    let home_rank: &Rank = match ranking.rank(m.home_team()) {
        Some(r) => r,
        None => return Err(format!(
            "Cannot find the ranking for the home team ({}).", m.home_team_name()
        ))
    };

    let away_rank: &Rank = match ranking.rank(m.away_team()) {
        Some(r) => r,
        None => return Err(format!(
            "Cannot find the ranking for the away team ({}).", m.away_team_name()
        ))
    };

    let res: CieloResult<MatchConclusion> = compute_points(m, home_rank, away_rank);

    if let Some(mc) = res.iter().next() {
        if let Err(e) = ranking.update(&mc) {
            return Err(format!("Cannot update ranking: {e}", e = e))
        }
    }

    res
}

pub fn compute_points(m: &Match, home_rank: &Rank, away_rank: &Rank) -> CieloResult<MatchConclusion> {
    let home_pts: PointsF = home_rank.pts();
    let away_pts: PointsF = away_rank.pts();

    // Difference in ratings
    let (drh, dra) = (home_pts - away_pts + HOME_MALUS, away_pts - home_pts);

    // Win expectancy
    let (weh, wea) = (win_expectancy(drh), win_expectancy(dra));

    // Effective win
    let (wh, wa) = match m.match_win() {
        MatchWin::HomeWin => (W_WIN , W_LOSS),
        MatchWin::Draw    => (W_DRAW, W_DRAW),
        MatchWin::AwayWin => (W_LOSS, W_WIN ),
    };

    let ka: f64 = adjusted_weight(m);

    // Compute earnings
    let delta_pts = |w: f64, we: f64| -> f64 { pts_diff(K, ka, w, we) };
    let (home_earnings, away_earnings) = (delta_pts(wh, weh), delta_pts(wa, wea));

    // Error messages for hunting NaNs (but not Ducuing)
    let mut err_msgs: Vec<String> = vec![];
    let score = m.score(true);

    if home_earnings.is_nan() {
        err_msgs.push(format!(
            "Earnings for {h} in {s} are NaN.",
            h = m.home_team_name(), s = score
        ))
    }

    if away_earnings.is_nan() {
        err_msgs.push(format!(
            "Earnings for {a} in {s} are NaN.",
            a = m.away_team_name(), s = score
        ))
    }

    // Returning results.
    if err_msgs.is_empty() {
        Ok(MatchConclusion::new(m.clone(), home_earnings, away_earnings))
    }
    else {
        Err(err_msgs.join(" "))
    }

}

fn win_expectancy(dr: f64) -> f64 {
    1.0 / ( 10.0_f64.powf(-dr/400.0) + 1.0 )
}

// Additional points for the home team in points difference.
pub static HOME_MALUS: f64 = 100.0;

// Effective win coefficients
pub static W_WIN : f64 = 1.0;
pub static W_DRAW: f64 = 0.5;
pub static W_LOSS: f64 = 0.0;

// Tournament weight. Set to a fixed value because everybody plays the same competition everytime.
pub static K: f64 = 10.0;

pub fn adjusted_weight(m: &Match) -> f64 {
    // Goal difference
    let gd: u16 = m.goal_difference();

    match gd {
        0 => 0.0,
        1 => 0.0,
        2 => 0.5,
        3 => 0.75,
        _ => (gd + 3) as f64 / 8.0      // 0.75 + ((gd as f64 - 3.0) / 8.0) on eloratings.net
    }
}

pub fn pts_diff(k: f64, ka: f64, w: f64, we: f64) -> f64 {
    k *(1.0 + ka) * (w - we)
}

// TODO: tests
