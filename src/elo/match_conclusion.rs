use crate::common::Match;
use crate::elo::PointsF;
use std::hash::{Hash, Hasher};

#[derive(Debug, Clone)]
pub struct MatchConclusion {
    contest: Match,
    home_earnings: PointsF,
    away_earnings: PointsF
}

impl MatchConclusion {
    pub fn new(contest: Match, home_earnings: PointsF, away_earnings: PointsF) -> Self {
        Self {contest, home_earnings, away_earnings}
    }

    pub fn contest(&self) -> &Match {
        &self.contest
    }

    pub fn home_earnings(&self) -> PointsF {
        self.home_earnings
    }

    pub fn away_earnings(&self) -> PointsF {
        self.away_earnings
    }

    pub fn earnings(&self) -> (PointsF, PointsF) {
        (self.home_earnings, self.away_earnings)
    }

    pub fn match_sumup_lines(&self) -> Vec<String> {
        vec![
            self.contest.score(true),
            format!(
                "\t{ht} {verb} {he:.2} pts.",
                ht = self.contest.home_team_name(),
                verb = if self.home_earnings > 0.0 { "earns" } else { "loses" },
                he = self.home_earnings
            ),
            format!(
                "\t{at} {verb} {ae:.2} pts.",
                at = self.contest.away_team_name(),
                verb = if self.away_earnings > 0.0 { "earns" } else { "loses" },
                ae = self.away_earnings
            ),
        ]
    }
}

impl PartialEq for MatchConclusion {
    fn eq(&self, other: &MatchConclusion) -> bool {
        self.contest == other.contest
            && self.home_earnings.eq(&other.home_earnings)
            && self.away_earnings.eq(&other.away_earnings)
    }
}

impl Eq for MatchConclusion {}

impl Hash for MatchConclusion {
    fn hash<H: Hasher>(&self, state: &mut H) {
        let dstring = format!(
            "Cielo#{m:?}#{he}#{ae}#Cielo",
            m = self.contest, he = self.home_earnings, ae = self.away_earnings
        );
        dstring.hash(state)
    }
}

// TODO: tests
