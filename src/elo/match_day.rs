use chrono::NaiveDate;
use std::collections::{HashSet, HashMap};
use crate::elo::{Ranking, MatchConclusion};

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct MatchDay {
    date: NaiveDate,
    results: HashSet<MatchConclusion>,
    ranking: Ranking
}

impl MatchDay {
    pub fn new(date: NaiveDate, results: HashSet<MatchConclusion>, ranking: Ranking) -> Self {
        Self { date, results, ranking }
    }

    pub fn day_lines(&self) -> Vec<String> {
        let mut res= Vec::with_capacity(
            2
                + 3*self.results.len().max(1)
                + self.ranking.ranking_len()
        );

        res.push(format!("{} matches:", self.date));

        if self.results.is_empty(){
            res.push("\tNone".to_string());
        }
        else {
            self.results.iter().for_each(|mc| {
                mc.match_sumup_lines().iter().for_each(|l| {
                    res.push(format!("\t{}", l));
                })
            });
        }

        let r_lines = self.ranking.ranking_lines();
        let mut rit = r_lines.iter();

        res.push(rit.next().expect("Ranking lines have a header.").clone());

        rit.for_each(|l| {
            res.push(format!("\t{}", l))
        });

        res
    }

    pub fn tell_day_matches(&self) {
        self.day_lines().iter().for_each(|l| { println!("{}", l); });
    }
}

pub type Journey = HashMap<NaiveDate, MatchDay>;
