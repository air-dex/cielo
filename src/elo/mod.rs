mod rank;
pub use rank::Rank;

mod ranking;
pub use ranking::Ranking;

mod match_conclusion;
pub use match_conclusion::MatchConclusion;

mod match_day;
pub use match_day::{Journey, MatchDay};

pub mod computer;
pub use computer::compute_championship;

pub type Points = i16;
pub type PointsF = f64;
