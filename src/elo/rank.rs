use crate::common::Team;
use crate::common::game::MatchWin;
use crate::elo::{Points, PointsF, MatchConclusion};
use std::cmp::Ordering;
use std::fmt::{Display, Formatter, Error};

pub static RED_CARD_POINTS: u16 = 3;
pub static YELLOW_CARD_POINTS: u16 = 1;
pub static FOULS_POINTS: u16 = 0;

#[derive(Debug, Clone)]
pub struct Rank {
    rank: usize,
    team: Team,
    pts: PointsF,
    win: u16,
    draw: u16,
    loss: u16,
    goals_for: u16,
    goals_against: u16,
    yellow_cards: u16,
    red_cards: u16,
    fouls: u16
}

impl Rank {
    pub fn new(team: Team) -> Self {
        Self {
            rank: 0,
            team,
            pts: 0.0,
            win: 0,
            draw: 0,
            loss: 0,
            goals_for: 0,
            goals_against: 0,
            yellow_cards: 0,
            red_cards: 0,
            fouls: 0
        }
    }

    pub fn rank(&self) -> usize {
        self.rank
    }

    pub fn set_rank(&mut self, rank: usize) {
        self.rank = rank
    }

    pub fn team(&self) -> &Team {
        &self.team
    }

    pub fn played(&self) -> u16 {
        self.win + self.draw + self.loss
    }

    pub fn pts(&self) -> PointsF {
        self.pts
    }

    pub fn update_pts(&mut self, new_pts: f64) {
        self.pts += new_pts;
    }

    pub fn rounded_pts(&self) -> Points {
        self.pts.round() as Points
    }

    pub fn win(&self) -> u16 {
        self.win
    }

    pub fn draw(&self) -> u16 {
        self.draw
    }

    pub fn loss(&self) -> u16 {
        self.loss
    }

    pub fn goals_for(&self) -> u16 {
        self.goals_for
    }

    pub fn goals_against(&self) -> u16 {
        self.goals_against
    }

    pub fn goal_difference(&self) -> i16 {
        self.goals_for as i16 - self.goals_against as i16
    }

    pub fn yellow_cards(&self) -> u16 {
        self.yellow_cards
    }

    pub fn red_cards(&self) -> u16 {
        self.red_cards
    }

    pub fn fouls(&self) -> u16 {
        self.fouls
    }

    pub fn update_stats(&mut self, mc: &MatchConclusion) {
        let m = mc.contest();

        let (rtm, otmg, is_home) = if self.team == m.home_team() {
            (m.home(), m.away().ft_goals(), true)
        } else {
            (m.away(), m.home().ft_goals(), false)
        };

        self.pts += if is_home { mc.home_earnings() } else { mc.away_earnings() };

        self.goals_for += rtm.ft_goals();
        self.goals_against += otmg;

        self.fouls += rtm.fouls();
        self.yellow_cards += rtm.yellow_cards();
        self.red_cards += rtm.red_cards();

        match m.match_win() {
            MatchWin::HomeWin => if is_home { self.win += 1 } else { self.loss += 1 },
            MatchWin::Draw => self.draw += 1,
            MatchWin::AwayWin => if is_home { self.loss += 1 } else { self.win += 1 }
        }
    }

    pub fn disciplinary_points(&self) -> u16 {
        FOULS_POINTS * self.fouls
            + YELLOW_CARD_POINTS * self.yellow_cards
            + RED_CARD_POINTS * self.red_cards
    }

    fn cmp_rank(&self, other: &Rank) -> Ordering {
        // 1°) More points
        let c_pt: Ordering = self.pts.partial_cmp(&other.pts).expect("No NaN in Cielo.");

        if c_pt != Ordering::Equal {
            return c_pt
        }

        // 2°) More goals difference
        let c_pt: Ordering = self.goal_difference().cmp(&other.goal_difference());

        if c_pt != Ordering::Equal {
            return c_pt
        }

        // 3°) More goals for
        let c_pt: Ordering = self.goals_for.cmp(&other.goals_for);

        if c_pt != Ordering::Equal {
            return c_pt
        }

        // 4°) Less goals against
        let c_pt: Ordering = self.goals_against.cmp(&other.goals_against);

        if c_pt != Ordering::Equal {
            return c_pt.reverse()
        }

        // 5°) More victories
        let c_pt: Ordering = self.win.cmp(&other.win);

        if c_pt != Ordering::Equal {
            return c_pt
        }

        // 6°) More draws
        let c_pt: Ordering = self.draw.cmp(&other.draw);

        if c_pt != Ordering::Equal {
            return c_pt
        }

        // 7°) Less defeats
        let c_pt: Ordering = self.loss.cmp(&other.loss);

        if c_pt != Ordering::Equal {
            return c_pt.reverse()
        }

        // 8°) Less disciplinary points
        let c_pt: Ordering = self.disciplinary_points().cmp(&other.disciplinary_points());

        if c_pt != Ordering::Equal {
            return c_pt.reverse()
        }

        // 9°) Less red cards
        let c_pt: Ordering = self.red_cards.cmp(&other.red_cards);

        if c_pt != Ordering::Equal {
            return c_pt.reverse()
        }

        // 10°) Less yellow cards
        let c_pt: Ordering = self.yellow_cards.cmp(&other.yellow_cards);

        if c_pt != Ordering::Equal {
            return c_pt.reverse()
        }

        // 11°) Less fouls
        let c_pt: Ordering = self.fouls.cmp(&other.fouls);

        if c_pt != Ordering::Equal {
            return c_pt.reverse()
        }

        // Cannot decide between => same rank
        Ordering::Equal
    }

    pub fn rank_line(&self) -> String {
        format!(
            "{p}°) {n}\t {pts:.2} pts\t{pm} p {w} w {d} d {l} l\t{gf} gf {ga} ga {gd} gd\t{y} yc {r} rc {f} f",
            p = self.rank, n = self.team.name(), pts = self.pts,
            pm = self.played(), w = self.win, d = self.draw, l = self.loss,
            gf = self.goals_for, ga = self.goals_against, gd = self.goal_difference(),
            y = self.yellow_cards, r = self.red_cards, f = self.fouls
        )
    }
}

impl PartialOrd for Rank {
    fn partial_cmp(&self, other: &Rank) -> Option<Ordering> {
        Some(self.cmp_rank(other))
    }
}

impl Ord for Rank {
    fn cmp(&self, other: &Self) -> Ordering {
        self.cmp_rank(other)
    }
}

impl PartialEq for Rank {
    fn eq(&self, other: &Rank) -> bool {
        self.cmp_rank(other) == Ordering::Equal
    }
}

impl Eq for Rank {}

impl Display for Rank {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        write!(f, "{}", self.rank_line())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    #[ignore]
    fn goal_diff() {
        unimplemented!("Less for");
        unimplemented!("Less against");
        unimplemented!("Equal");

        unimplemented!()
    }

    #[test]
    #[ignore]
    fn disciplinary() {
        assert_eq!(FOULS_POINTS, 0);
        assert_eq!(YELLOW_CARD_POINTS, 1);
        assert_eq!(RED_CARD_POINTS, 3);

        unimplemented!()
    }

    #[test]
    #[ignore]
    fn compare_ranks() {
        // For each comparison point:

        unimplemented!("Less");
        unimplemented!("Greater");
        unimplemented!("Equal");

        unimplemented!()
    }

    #[test]
    #[ignore]
    fn rank_eq() {
        // For each comparison point:

        unimplemented!("Equal");
        unimplemented!("!=");

        unimplemented!()
    }

    #[test]
    #[ignore]
    fn rank_cmp() {
        // For each comparison point:

        unimplemented!("Less");
        unimplemented!("Greater");
        unimplemented!("Equal");

        unimplemented!()
    }

    #[test]
    #[ignore]
    fn rank_partial_cmp() {
        // For each comparison point:

        unimplemented!("Less");
        unimplemented!("Greater");
        unimplemented!("Equal");

        unimplemented!()
    }
}
