use chrono::naive::NaiveDate;
use crate::common::{Team, CieloResult};
use crate::common::game::get_invalid_match_date;
use crate::common::team::Teams;
use crate::elo::{Rank, MatchConclusion};
use std::convert::From;
use std::fmt::{Display, Error, Formatter};

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Ranking {
    ranks: Vec<Rank>,
    date: NaiveDate
}

impl Ranking {
    pub fn new(ranks: &Vec<Rank>, date: NaiveDate) -> Self {
        Self { ranks: ranks.clone(), date }
    }

    pub fn ranks(&self) -> &Vec<Rank> {
        &self.ranks
    }

    pub fn date(&self) -> NaiveDate {
        self.date
    }

    pub fn set_date(&mut self, date: NaiveDate) {
        self.date = date;
    }

    pub fn rank_mut(&mut self, team: Team) -> Option<&mut Rank> {
        let mut it = self.ranks.iter_mut();

        while let Some(r) = it.next() {
            if *r.team() == team {
                return Some(r)
            }
        }

        None
    }

    pub fn rank(&self, team: Team) -> Option<&Rank> {
        let mut it = self.ranks.iter();

        while let Some(r) = it.next() {
            if *r.team() == team {
                return Some(r)
            }
        }

        None
    }

    pub fn update(&mut self, mc: &MatchConclusion) -> CieloResult<()> {
        // Updating home team
        match self.rank_mut(mc.contest().home_team()) {
            Some(rank) => rank.update_stats(mc),
            None => return Err(format!("Cannot find the home team in rankings in order to update it."))
        }

        // Updating away team
        match self.rank_mut(mc.contest().away_team()) {
            Some(rank) => rank.update_stats(mc),
            None => return Err(format!("Cannot find the away team in rankings in order to update it."))
        }

        Ok(())
    }

    pub fn sort_ranks(&mut self) {
        self.ranks.sort();
        self.ranks.reverse();

        (0..self.ranks.len()).for_each(|i| {
            if i > 0 && self.ranks[i] == self.ranks[i-1] {
                let prev_rank = self.ranks[i-1].rank();
                self.ranks[i].set_rank(prev_rank);
            }
            else {
                self.ranks[i].set_rank(i + 1);
            }
        });
    }

    pub fn ranking_len(&self) -> usize {
        self.ranks().len()
    }

    pub fn ranking_lines(&self) -> Vec<String> {
        let mut res = Vec::with_capacity(self.ranks.len() + 1);

        res.push(format!("Ranking as of {}:", self.date));

        self.ranks.iter().for_each(|r| {
            res.push(r.rank_line());
        });

        res
    }
}

impl From<&Teams> for Ranking {
    fn from(teams: &Teams) -> Self {
        Self {
            ranks: init_rank(teams),
            date: get_invalid_match_date()
        }
    }
}

impl Display for Ranking {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        write!(f, "{}", self.ranking_lines().join("\n"))
    }
}

fn init_rank(teams: &Teams) -> Vec<Rank> {
    let mut ranks: Vec<Rank> = teams.iter().map(|t: &Team| -> Rank {
        Rank::new(t.clone())}
    ).collect();
    ranks.sort();

    ranks
}

// TODO: tests
