mod args_parsing;
pub use args_parsing::CieloArgs;

pub mod common;
pub mod data;
pub mod elo;

use crate::common::Championship;
use crate::elo::Journey;

pub fn run(args: CieloArgs) -> common::CieloResult<()> {
    if cfg!(debug_assertions) {
        println!("cielo::run({:?})", args);
    }

    println!("Loading data");

    let championship: Championship = match Championship::from_data(
        args.country(),
        args.season(),
        args.input_format()
    ) {
        Ok(ch) => ch,
        Err(e) => return Err(e)
    };

    println!("Compute ELO ranks");

    let ch_journey: Journey = match championship.compute_elo(args.stdout()) {
        Ok(j) => j,
        Err(e) => return Err(e)
    };

    unimplemented!("Export results");
}
