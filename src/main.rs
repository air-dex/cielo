use std::env;
use cielo::CieloArgs;

fn main() {
    let args: Vec<String> = env::args().collect();

    match CieloArgs::from_cli(&args) {
        Ok(c_args) => {
            if let Err(err_msg) = cielo::run(c_args) {
                panic!("Error while running Cielo: {}", err_msg)
            }
        },
        Err(err_msg) => panic!("Error while parsing Cielo's CLI arguments: {}", err_msg)
    }
}
